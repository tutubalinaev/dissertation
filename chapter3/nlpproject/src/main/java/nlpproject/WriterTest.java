package nlpproject;


import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.resource.ExternalResourceDescription;
import org.junit.Assert;
import org.junit.Test;

import com.textocat.textokit.commons.util.PipelineDescriptorUtils;
import com.textocat.textokit.depparser.mst.MSTWriter;
import com.textocat.textokit.morph.dictionary.MorphDictionaryAPIFactory;
import com.textocat.textokit.postagger.PosTaggerAPI;
import com.textocat.textokit.segmentation.SentenceSplitterAPI;
import com.textocat.textokit.tokenizer.TokenizerAPI;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

public class WriterTest {
	
    public static void testOnNotEmptyFile() throws UIMAException, IOException {
    	String inputFilePath="/Users/telena/Documents/JAVA_Development/MaltParser/mstparser/350docs.tab";
        File inputFile = new File(inputFilePath);
        File outputFile = new File(inputFilePath+"-edited.tab");
        //
        CollectionReaderDescription colReaderDesc = MSTTrainedFileReader.createDescription(inputFile);
        AnalysisEngineDescription writerDesc = MSTWriter.createDescription(outputFile);
       
        Object[] configurationData = {
        		PosTaggerAPI.PARAM_REUSE_EXISTING_WORD_ANNOTATIONS, "true"}; 
        		
        AnalysisEngineDescription posTaggerAE = createEngineDescription(
        		PosTaggerAPI.AE_POSTAGGER,
        		configurationData);  
        
        AnalysisEngineDescription aeDesc = createEngineDescription(
                //createEngineDescription(TokenizerAPI.AE_TOKENIZER),
                //createEngineDescription(SentenceSplitterAPI.AE_SENTENCE_SPLITTER),
                //createEngineDescription(PosTaggerAPI.AE_POSTAGGER),
                posTaggerAE,
        		createEngineDescription(DoubWordReducer.class)
        );
        ExternalResourceDescription morphDictDesc =
                MorphDictionaryAPIFactory.getMorphDictionaryAPI().getResourceDescriptionForCachedInstance();
        morphDictDesc.setName(PosTaggerAPI.MORPH_DICTIONARY_RESOURCE_NAME);
        PipelineDescriptorUtils.getResourceManagerConfiguration(aeDesc)
                .addExternalResource(morphDictDesc);

        SimplePipeline.runPipeline(colReaderDesc, aeDesc, writerDesc);

    }
    public static void main(String[] args) throws UIMAException, IOException {
    	testOnNotEmptyFile();
    }
}
