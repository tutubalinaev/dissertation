package nlpproject;

import static org.apache.uima.fit.util.JCasUtil.select;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import com.google.common.collect.Lists;
import com.textocat.textokit.postagger.MorphCasUtils;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

import com.textocat.textokit.depparser.Dependency;
import com.textocat.textokit.morph.fs.Word;

/**
 * @author Elena Tutubalina
 */
public class DoubWordReducer extends JCasAnnotator_ImplBase {
    @Override
    public void process(JCas jCas) throws AnalysisEngineProcessException {    	
    	Type tt=jCas.getTypeSystem().getType("com.textocat.textokit.morph.fs.Word");
        LinkedList<AnnotationFS> annoToRemove = Lists.newLinkedList(jCas.getCas().getAnnotationIndex(tt));
        for (AnnotationFS anno : annoToRemove) {
        	String posTag = MorphCasUtils.getFirstPosTag((Word)anno);   
        	if (posTag==null || !posTag.contains("]")){
        		findOriginalWord(jCas,(Word)anno);
        		jCas.getCas().removeFsFromIndexes(anno);
        	}
        }
    }
    
    public void findOriginalWord(JCas jCas, Word ann){
    	for(Word w : JCasUtil.select(jCas, Word.class)) {    		 
            if (w.getBegin()==ann.getBegin() && w.getEnd()==ann.getEnd()){
            	//System.out.println(MorphCasUtils.getFirstPosTag(ann));
            	w.setWordforms(ann.getWordforms());
            }
        }  
    }
        
    public String getWordInfo(Word w){
    	if (w == null)
    		return "--";
    	String src = w.getCoveredText();
        String lemma = MorphCasUtils.getFirstLemma(w);
        String posTag = MorphCasUtils.getFirstPosTag(w);
        return String.format("%s/%s/%s", src, lemma, posTag);
    
    }
}
