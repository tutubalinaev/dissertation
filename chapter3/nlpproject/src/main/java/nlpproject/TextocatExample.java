package nlpproject;

import com.google.common.collect.Maps;
import com.textocat.textokit.morph.dictionary.MorphDictionaryAPIFactory;
import com.textocat.textokit.morph.lemmatizer.LemmatizerAPI;
import com.textocat.textokit.postagger.DictionaryComplianceChecker;
import com.textocat.textokit.postagger.PosTaggerAPI;
import com.textocat.textokit.segmentation.SentenceSplitterAPI;
import com.textocat.textokit.tokenizer.TokenizerAPI;
import com.textocat.textokit.commons.util.PipelineDescriptorUtils;
import com.textocat.textokit.depparser.mst.MSTParsingAnnotator;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.factory.ExternalResourceFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.factory.TypeSystemDescriptionFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.metadata.MetaDataObject;
import org.apache.uima.resource.metadata.TypeSystemDescription;







import org.apache.uima.ruta.engine.RutaEngine;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngine;

/**
 * @author Rinat Gareev
 */
public class TextocatExample {
    public static void main(String[] args) throws UIMAException, IOException {
   	
    	AnalysisEngineDescription parserDesc = MSTParsingAnnotator.createDescription(
    				new URL("file:/Users/telena/Documents/JAVA_Development/MaltParser/mstparser/dep.model"));
	    	    	   	
        TypeSystemDescription tsDesc = TypeSystemDescriptionFactory
                .createTypeSystemDescription(
                        "com.textocat.textokit.commons.Commons-TypeSystem",
                        "com.textocat.textokit.depparser.dependency-ts",
                        TokenizerAPI.TYPESYSTEM_TOKENIZER,
                        SentenceSplitterAPI.TYPESYSTEM_SENTENCES,
                        PosTaggerAPI.TYPESYSTEM_POSTAGGER,
                        LemmatizerAPI.TYPESYSTEM_LEMMATIZER
                		);
        //
        
        Object[] configurationData = {
        		PosTaggerAPI.DEFAULT_REUSE_EXISTING_WORD_ANNOTATIONS, "true",
        		PosTaggerAPI.PARAM_REUSE_EXISTING_WORD_ANNOTATIONS, "true"}; 
        		
        AnalysisEngineDescription posTaggerAE = createEngineDescription(
        		PosTaggerAPI.AE_POSTAGGER,
        		configurationData);
 	    	
    	AnalysisEngineDescription aeDesc = createEngineDescription(
                createEngineDescription(TokenizerAPI.AE_TOKENIZER),
                createEngineDescription(SentenceSplitterAPI.AE_SENTENCE_SPLITTER),
                posTaggerAE,
                //createEngineDescription(LemmatizerAPI.AE_LEMMATIZER),
               createEngineDescription(parserDesc),
               createEngineDescription(WordPosLemmaWriter.class)
        );
        
      
        
        Map<String, MetaDataObject> aeDescriptions = Maps.newLinkedHashMap();
		aeDescriptions.put("tokenizer", TokenizerAPI.getAEImport());
		//
		aeDescriptions.put("sentenceSplitter", SentenceSplitterAPI.getAEImport());
        
        ExternalResourceDescription morphDictDesc =
                MorphDictionaryAPIFactory.getMorphDictionaryAPI().getResourceDescriptionForCachedInstance();
        morphDictDesc.setName(PosTaggerAPI.MORPH_DICTIONARY_RESOURCE_NAME);
        PipelineDescriptorUtils.getResourceManagerConfiguration(aeDesc)
                .addExternalResource(morphDictDesc);
        
        /*ExternalResourceDescription morphDictDesc = MorphDictionaryAPIFactory.getMorphDictionaryAPI()
				.getResourceDescriptionForCachedInstance();
        System.out.println(morphDictDesc);
		ExternalResourceFactory.bindResource(aeDesc,
				DictionaryComplianceChecker.RESOURCE_DICTIONARY, morphDictDesc);*/
        


        //SimplePipeline.runPipeline(readerDesc, aeDesc, writerDesc);
        
        AnalysisEngine mainPipeline = createEngine(aeDesc);
    	JCas jcas = JCasFactory.createJCas(tsDesc); 
        
    	jcas.setDocumentText("без в принципе проблем");
    	//jcas.setDocumentText("Позже сподобились на пиццу, сделана не плохо,тоненькая с острой колбаской, в процессе еды острота особенно ощущалась");
    	//jcas.setDocumentText("Автомобиль, несмотря на китайскую сборку, не плохой, но есть небольшая проблема со стеклами.");
        jcas.setDocumentLanguage("ru");
        mainPipeline.process(jcas);
    }
}