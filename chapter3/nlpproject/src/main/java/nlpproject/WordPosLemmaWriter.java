package nlpproject;

import static com.textocat.textokit.commons.cas.AnnotationUtils.toPrettyString;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectCovered;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

import com.google.common.collect.Lists;
import com.textocat.textokit.postagger.MorphCasUtils;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;

import com.textocat.textokit.commons.cas.FSUtils;
import com.textocat.textokit.depparser.Dependency;
import com.textocat.textokit.morph.fs.Word;
import com.textocat.textokit.morph.fs.Wordform;
import com.textocat.textokit.morph.fs.SimplyWord;

/**
 * @author Rinat Gareev
 */
public class WordPosLemmaWriter extends JCasAnnotator_ImplBase {
    @Override
    public void process(JCas jCas) throws AnalysisEngineProcessException {    	
    	for(SimplyWord w : JCasUtil.select(jCas, SimplyWord.class)) { 
    		//if (w.getPosTag()!=null && w.getPosTag().contains("VERB"))
    			//System.out.print(String.format("%s\n", w.getLemma()));
    		System.out.printf("  %-16s %-10s %-10s %-10s %n", 
                    w.getCoveredText(), 
                    w.getBegin(),
                    w.getEnd(),
                    w.getLemma());
       } 
    	//for(SimplyWord w : JCasUtil.select(jCas, SimplyWord.class)) { 
    	//	System.out.println(w);
       //} 
    	
      for (Word token : JCasUtil.select(jCas, Word.class)) {
            System.out.printf("  %-16s %-10s %-10s %-10s %-10s %n", 
                    token.getCoveredText(), 
                    token.getBegin(),
                    token.getEnd(),
                    MorphCasUtils.getFirstPosTag(token),
                    MorphCasUtils.getFirstPosTag(token));
        }
        
    	
    	/*for(Word w : JCasUtil.select(jCas, Word.class)) {    		 
             System.out.print(String.format("++%s\n", getWordInfo(w)));
        }  
    	Collection<Word> objs=select(jCas, Word.class);
    	  */  	
        System.out.printf("-- Dependency relations --%n");
        System.out.printf("  %-16s %-10s %-10s %n", "TOKEN", "DEPREL", "DEP", "GOV");
        for (Dependency dep : JCasUtil.select(jCas, Dependency.class)) {
            System.out.printf("  %-16s %-10s %-10s %n", 
                    dep.getLabel(), 
                    getWordInfo(dep.getDependent()), 
                    getWordInfo(dep.getHead()));
        }
        
        // mark the end of a document
        //System.out.println("\n");
    }
        
    
    public String getWordInfo(Word w){
    	if (w == null)
    		return "--";
    	String src = w.getCoveredText();

    	FSArray wfs = w.getWordforms();
    	//for (int i=0; i< wfs.size(); i++)
    		//System.out.println(((Wordform) wfs.get(i)).getLemma());
        //Set<String> lemma = MorphCasUtils.getOnlyWordform(w);
    	String lemma="/";
        String posTag = MorphCasUtils.getFirstPosTag(w);
        return String.format("%s/%s/%s/%s/%s", src, lemma, posTag, w.getBegin(), w.getEnd());
    
    }
}
