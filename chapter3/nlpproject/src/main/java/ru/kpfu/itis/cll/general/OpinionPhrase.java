

/* First created by JCasGen Mon Nov 09 16:33:50 MSK 2015 */
package ru.kpfu.itis.cll.general;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** Type defined in ru.kpfu.itis.cll.general
 * Updated by JCasGen Mon Nov 09 19:41:58 MSK 2015
 * XML source: /Users/telena/Dropbox/ProblemExtraction/project2/src/main/resources/ru/kpfu/itis/cll/main/BasicEnProblemExtractorEngine.xml
 * @generated */
public class OpinionPhrase extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(OpinionPhrase.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected OpinionPhrase() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public OpinionPhrase(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public OpinionPhrase(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public OpinionPhrase(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: oindicator

  /** getter for oindicator - gets Feature: An indicator for problem phrases
   * @generated
   * @return value of the feature 
   */
  public Annotation getOindicator() {
    if (OpinionPhrase_Type.featOkTst && ((OpinionPhrase_Type)jcasType).casFeat_oindicator == null)
      jcasType.jcas.throwFeatMissing("oindicator", "ru.kpfu.itis.cll.general.OpinionPhrase");
    return (Annotation)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((OpinionPhrase_Type)jcasType).casFeatCode_oindicator)));}
    
  /** setter for oindicator - sets Feature: An indicator for problem phrases 
   * @generated
   * @param v value to set into the feature 
   */
  public void setOindicator(Annotation v) {
    if (OpinionPhrase_Type.featOkTst && ((OpinionPhrase_Type)jcasType).casFeat_oindicator == null)
      jcasType.jcas.throwFeatMissing("oindicator", "ru.kpfu.itis.cll.general.OpinionPhrase");
    jcasType.ll_cas.ll_setRefValue(addr, ((OpinionPhrase_Type)jcasType).casFeatCode_oindicator, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: otarget

  /** getter for otarget - gets Feature: A opinion target for the phrase
   * @generated
   * @return value of the feature 
   */
  public OpinionTarget getOtarget() {
    if (OpinionPhrase_Type.featOkTst && ((OpinionPhrase_Type)jcasType).casFeat_otarget == null)
      jcasType.jcas.throwFeatMissing("otarget", "ru.kpfu.itis.cll.general.OpinionPhrase");
    return (OpinionTarget)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((OpinionPhrase_Type)jcasType).casFeatCode_otarget)));}
    
  /** setter for otarget - sets Feature: A opinion target for the phrase 
   * @generated
   * @param v value to set into the feature 
   */
  public void setOtarget(OpinionTarget v) {
    if (OpinionPhrase_Type.featOkTst && ((OpinionPhrase_Type)jcasType).casFeat_otarget == null)
      jcasType.jcas.throwFeatMissing("otarget", "ru.kpfu.itis.cll.general.OpinionPhrase");
    jcasType.ll_cas.ll_setRefValue(addr, ((OpinionPhrase_Type)jcasType).casFeatCode_otarget, jcasType.ll_cas.ll_getFSRef(v));}    
  }

    