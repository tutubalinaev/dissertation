package ru.kpfu.itis.cll.annotators;

import static org.apache.uima.fit.util.JCasUtil.select;



import java.util.Collection;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.jcas.JCas;

import ru.kpfu.itis.cll.general.OpinionPhrase;
import ru.kpfu.itis.cll.general.OpinionTarget;

public class NonDomainTargetsReducer extends JCasAnnotator_ImplBase {
	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		//for (AnnotationFS sentence : select(aJCas, Sentence.class)){
		//	if (sentence.getCoveredText().length()>3) {
		Collection<OpinionPhrase> objs=select(aJCas, OpinionPhrase.class);
				for (OpinionPhrase phrase:objs){					
					OpinionTarget target=phrase.getOtarget();					
					if (target.getDomainInd()==false){
						//System.out.println(phrase);
						getLogger().info("NonDomainTargetsReducer: removing: " +phrase.getOindicator().getCoveredText()+";"+target.getCoveredText());						
						aJCas.removeFsFromIndexes(target);
						aJCas.removeFsFromIndexes(phrase.getOindicator());
						//aJCas.removeFsFromIndexes(phrase);
					}
					else {
						getLogger().info("NonDomainTargetsReducer: saving: " +phrase.getOindicator().getCoveredText()+";"+target.getCoveredText());						
					}
				}
			//}
		//}
	}
}
