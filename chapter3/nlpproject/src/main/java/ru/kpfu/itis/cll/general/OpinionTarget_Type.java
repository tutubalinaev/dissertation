
/* First created by JCasGen Sun Nov 01 17:20:27 MSK 2015 */
package ru.kpfu.itis.cll.general;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** Type defined in ru.kpfu.itis.cll.general
 * Updated by JCasGen Mon Nov 09 19:41:58 MSK 2015
 * @generated */
public class OpinionTarget_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (OpinionTarget_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = OpinionTarget_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new OpinionTarget(addr, OpinionTarget_Type.this);
  			   OpinionTarget_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new OpinionTarget(addr, OpinionTarget_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = OpinionTarget.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.kpfu.itis.cll.general.OpinionTarget");



  /** @generated */
  final Feature casFeat_pos;
  /** @generated */
  final int     casFeatCode_pos;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public String getPos(int addr) {
        if (featOkTst && casFeat_pos == null)
      jcas.throwFeatMissing("pos", "ru.kpfu.itis.cll.general.OpinionTarget");
    return ll_cas.ll_getStringValue(addr, casFeatCode_pos);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setPos(int addr, String v) {
        if (featOkTst && casFeat_pos == null)
      jcas.throwFeatMissing("pos", "ru.kpfu.itis.cll.general.OpinionTarget");
    ll_cas.ll_setStringValue(addr, casFeatCode_pos, v);}
    
  
 
  /** @generated */
  final Feature casFeat_domainInd;
  /** @generated */
  final int     casFeatCode_domainInd;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public boolean getDomainInd(int addr) {
        if (featOkTst && casFeat_domainInd == null)
      jcas.throwFeatMissing("domainInd", "ru.kpfu.itis.cll.general.OpinionTarget");
    return ll_cas.ll_getBooleanValue(addr, casFeatCode_domainInd);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setDomainInd(int addr, boolean v) {
        if (featOkTst && casFeat_domainInd == null)
      jcas.throwFeatMissing("domainInd", "ru.kpfu.itis.cll.general.OpinionTarget");
    ll_cas.ll_setBooleanValue(addr, casFeatCode_domainInd, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public OpinionTarget_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_pos = jcas.getRequiredFeatureDE(casType, "pos", "uima.cas.String", featOkTst);
    casFeatCode_pos  = (null == casFeat_pos) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_pos).getCode();

 
    casFeat_domainInd = jcas.getRequiredFeatureDE(casType, "domainInd", "uima.cas.Boolean", featOkTst);
    casFeatCode_domainInd  = (null == casFeat_domainInd) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_domainInd).getCode();

  }
}



    