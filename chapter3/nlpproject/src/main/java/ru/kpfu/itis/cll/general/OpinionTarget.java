

/* First created by JCasGen Sun Nov 01 17:20:27 MSK 2015 */
package ru.kpfu.itis.cll.general;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** Type defined in ru.kpfu.itis.cll.general
 * Updated by JCasGen Mon Nov 09 19:41:58 MSK 2015
 * XML source: /Users/telena/Dropbox/ProblemExtraction/project2/src/main/resources/ru/kpfu/itis/cll/main/BasicEnProblemExtractorEngine.xml
 * @generated */
public class OpinionTarget extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(OpinionTarget.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated
   * @return index of the type  
   */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected OpinionTarget() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated
   * @param addr low level Feature Structure reference
   * @param type the type of this Feature Structure 
   */
  public OpinionTarget(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated
   * @param jcas JCas to which this Feature Structure belongs 
   */
  public OpinionTarget(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated
   * @param jcas JCas to which this Feature Structure belongs
   * @param begin offset to the begin spot in the SofA
   * @param end offset to the end spot in the SofA 
  */  
  public OpinionTarget(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** 
   * <!-- begin-user-doc -->
   * Write your own initialization here
   * <!-- end-user-doc -->
   *
   * @generated modifiable 
   */
  private void readObject() {/*default - does nothing empty block */}
     
  //*--------------*
  //* Feature: pos

  /** getter for pos - gets Feature: pos
   * @generated
   * @return value of the feature 
   */
  public String getPos() {
    if (OpinionTarget_Type.featOkTst && ((OpinionTarget_Type)jcasType).casFeat_pos == null)
      jcasType.jcas.throwFeatMissing("pos", "ru.kpfu.itis.cll.general.OpinionTarget");
    return jcasType.ll_cas.ll_getStringValue(addr, ((OpinionTarget_Type)jcasType).casFeatCode_pos);}
    
  /** setter for pos - sets Feature: pos 
   * @generated
   * @param v value to set into the feature 
   */
  public void setPos(String v) {
    if (OpinionTarget_Type.featOkTst && ((OpinionTarget_Type)jcasType).casFeat_pos == null)
      jcasType.jcas.throwFeatMissing("pos", "ru.kpfu.itis.cll.general.OpinionTarget");
    jcasType.ll_cas.ll_setStringValue(addr, ((OpinionTarget_Type)jcasType).casFeatCode_pos, v);}    
   
    
  //*--------------*
  //* Feature: domainInd

  /** getter for domainInd - gets Feature: if target is domain-specific, domainInd = true; otherwise, domainInd = false
   * @generated
   * @return value of the feature 
   */
  public boolean getDomainInd() {
    if (OpinionTarget_Type.featOkTst && ((OpinionTarget_Type)jcasType).casFeat_domainInd == null)
      jcasType.jcas.throwFeatMissing("domainInd", "ru.kpfu.itis.cll.general.OpinionTarget");
    return jcasType.ll_cas.ll_getBooleanValue(addr, ((OpinionTarget_Type)jcasType).casFeatCode_domainInd);}
    
  /** setter for domainInd - sets Feature: if target is domain-specific, domainInd = true; otherwise, domainInd = false 
   * @generated
   * @param v value to set into the feature 
   */
  public void setDomainInd(boolean v) {
    if (OpinionTarget_Type.featOkTst && ((OpinionTarget_Type)jcasType).casFeat_domainInd == null)
      jcasType.jcas.throwFeatMissing("domainInd", "ru.kpfu.itis.cll.general.OpinionTarget");
    jcasType.ll_cas.ll_setBooleanValue(addr, ((OpinionTarget_Type)jcasType).casFeatCode_domainInd, v);}    
  }

    