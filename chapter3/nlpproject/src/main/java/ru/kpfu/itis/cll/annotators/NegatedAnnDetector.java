package ru.kpfu.itis.cll.annotators;

import static org.apache.uima.fit.util.CasUtil.select;
import static org.apache.uima.fit.util.CasUtil.selectCovered;
import static org.apache.uima.fit.util.JCasUtil.selectCovered;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.resource.ResourceInitializationException;

import ru.kpfu.itis.cll.general.NegAction;
import ru.kpfu.itis.cll.general.NegProblem;
import ru.kpfu.itis.cll.general.NegativeWord;
import ru.kpfu.itis.cll.general.PositiveWord;
import ru.kpfu.itis.cll.general.ProblemTrigger;

import com.textocat.textokit.depparser.Dependency;
import com.textocat.textokit.morph.fs.Word;
import com.textocat.textokit.postagger.MorphCasUtils;

public class NegatedAnnDetector extends CasAnnotator_ImplBase {
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_SENTENCE_ANNOTATION_TYPE = "SentenceAnnotation";
	@ConfigurationParameter(name = PN_SENTENCE_ANNOTATION_TYPE, mandatory = false, defaultValue = "com.textocat.textokit.segmentation.fstype.Sentence")
	private String sentenceAnnotationType;
	private Type sentenceType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_TOKEN_ANNOTATION_TYPE = "WordAnnotation";
	@ConfigurationParameter(name = PN_TOKEN_ANNOTATION_TYPE, mandatory = false, defaultValue = "com.textocat.textokit.morph.fs.Word")
	private String tokenAnnotationType;
	private Type tokenType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_ACTION_WORD_ANNOTATION_TYPE = "ActionTriggerAnnotation";
	@ConfigurationParameter(name = PN_ACTION_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.ActionTrigger")
	private String actionTriggerAnnotationType;
	private Type actionTriggerType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_NEG_ACTION_WORD_ANNOTATION_TYPE = "NegActionTriggerAnnotation";
	@ConfigurationParameter(name = PN_ACTION_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.NegAction")
	private String negActionAnnotationType;
	private Type negActionTriggerType = null;
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_PROBLEM_WORD_ANNOTATION_TYPE = "ProblemTriggerAnnotation";
	@ConfigurationParameter(name = PN_PROBLEM_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.ProblemTrigger")
	private String ProblemTriggerAnnotationType;
	private Type problemWordType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_NOT_PROBLEM_WORD_ANNOTATION_TYPE = "NotProblemWordAnnotation";
	@ConfigurationParameter(name = PN_NOT_PROBLEM_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.NotProblemWord")
	private String NotProblemWordAnnotationType;
	private Type notProblemWordType = null;	

	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String NEGATION_WORD_ANNOTATION_TYPE = "NegationWordAnnotation";
	@ConfigurationParameter(name = NEGATION_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.Negation")
	private String NegationAnnotationType;
	private Type negationWordType = null;
	// -------------------------------------------------------------------------------------------------------------------
	public static final String NEGATION_FOR_PROBLEM_ANNOTATION_TYPE = "NegativeWordAnnotation";
	@ConfigurationParameter(name = NEGATION_FOR_PROBLEM_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.NegationForProblem")
	private String NegationForProblemAnnotationType;
	private Type negationForProblemType = null;		
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_POSITIVE_WORD_ANNOTATION_TYPE = "PositiveWordAnnotation";
	@ConfigurationParameter(name = PN_POSITIVE_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.PositiveWord")
	private String PositiveWordAnnotationType;
	private Type positiveWordType = null;
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_NEGATIVE_WORD_ANNOTATION_TYPE = "NegativeWordAnnotation";
	@ConfigurationParameter(name = PN_NEGATIVE_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.NegativeWord")
	private String NegativeWordAnnotationType;
	private Type negativeWordType = null;	
	
	
	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		super.initialize(aContext);
	}
	
	@Override
	public void process(CAS aCAS) throws AnalysisEngineProcessException {	
		for (AnnotationFS sentence : select(aCAS, sentenceType)){
			if (sentence.getCoveredText().length()>3) {
				System.out.println(sentence.getCoveredText());
				try {
					extractConnections(aCAS, sentence);
				} catch (CASException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				printAllAnnotationsOfSentence(sentence);
			}	
		}
	}
	
	public void extractConnections(CAS aCAS, AnnotationFS sentence) throws CASException{
		List<Dependency> ordeps=getDependencies(sentence);
		List deps = new ArrayList(ordeps);
		
		List<AnnotationFS> problNegations = getNegationForProblemFromSentence(sentence);		
		extractWordsWithDependencies(aCAS, sentence, problNegations, true);
		
		List<AnnotationFS> comNegations = getNegationWordFromSentence(sentence);
		extractWordsWithDependencies(aCAS, sentence, comNegations, false);
		
	}
	
	public void extractWordsWithDependencies(CAS aCAS, AnnotationFS sentence,  List<AnnotationFS> negations, boolean isProblem) throws CASException{
		List<Dependency> ordeps=getDependencies(sentence);
		List deps = new ArrayList(ordeps);
		
		for (AnnotationFS neg:negations){
			//System.out.println(deps.size());
			List<Word> words = getMatchedWordsFromDependency(deps, neg, false);			
			//filter them 
			ListIterator<Word> wordsit = words.listIterator();
			while (wordsit.hasNext()){
				Word w = wordsit.next();
				String pos = MorphCasUtils.getFirstPosTag(w);
				if (pos!=null && (pos.contains("PRCL") || pos.contains("PRED")))
					wordsit.remove();
				if (isProblem && (pos==null || pos.contains("ADJF") 
						|| pos.contains("ADVB") 
						|| pos.contains("CONJ")
						|| pos.contains("VERB")
						|| pos.contains("INFN"))){
					if (!doesIndicatorFind(aCAS,w,sentence,isProblem)){
						boolean chechHead = (pos!=null && (pos.contains("VERB")||pos.contains("INFN")))? false:true;
						List<Word> newwords = getMatchedWordsFromDependency(deps, w, chechHead);
						for (Word ww:newwords){
							//String pos1 = MorphCasUtils.getFirstPosTag(ww);
							///if (pos1==null || !pos1.equals(pos))
							wordsit.add(ww);
						}
					}
				}
			}
			for (Word w:words){
				System.out.println("--" + w.getCoveredText());
				if (isProblem)
					annotateProblemDependency(aCAS, w, sentence, neg);	
				else
					annotateNegatedDependency(aCAS, w, sentence, neg);
			}
			//System.out.println(deps.size());
		}
	}

	public boolean doesIndicatorFind(CAS aCAS, Word word,AnnotationFS sentence, boolean isProblem) {
		List<AnnotationFS> indicators = null;
		if (isProblem)
			indicators = getProblemTriggersFromSentence(sentence);
		else
			indicators = getActionsTriggerFromNegAction(sentence);
		
		for (AnnotationFS ann:indicators){
			//System.out.println(word.getCoveredText() + " " + ann.getCoveredText());
			if (ann.getBegin()==word.getBegin() && ann.getEnd()==word.getEnd())
				return true;
		}
		return false;
	}
	
	
	public List<Word> getMatchedWordsFromDependency(List<Dependency> deps, AnnotationFS word, boolean checkHead){
		List<Word> res = new ArrayList<Word>();
		Iterator<Dependency> depit = deps.iterator();
		while (depit.hasNext()){
			boolean shouldDelete=false;
			Dependency d = depit.next();
			Word ann = d.getHead();
			if (checkHead)
			if (ann!=null && ann.getBegin()==word.getBegin() && ann.getEnd()==word.getEnd())
				if (d.getDependent()!=null) {
					shouldDelete=true;
					res.add(d.getDependent());
				}
			ann = d.getDependent();
			if (ann!=null && ann.getBegin()==word.getBegin() && ann.getEnd()==word.getEnd())
				if (d.getHead()!=null) {
					shouldDelete=true;
					res.add(d.getHead());
			}
			if (shouldDelete) depit.remove();				
		}
		return res;
	}

	
	public boolean checkDeps(Word word, AnnotationFS negation){
		String posTag = MorphCasUtils.getFirstPosTag(word);
		if (negation.getCoveredText().equals("без") && posTag!=null)
			return !posTag.contains("ADJF");
		if (posTag.contains("VERB") && !posTag.contains("past")) //устраните VERB&plur&perf
			return false;
		return true;
	}
	
	public void annotateProblemDependency(CAS aCAS, Word word,AnnotationFS sentence, AnnotationFS negation) throws CASException{
		if (word == null)
			return;
		List<AnnotationFS> problems = getProblemTriggersFromSentence(sentence);
		for (AnnotationFS ann:problems){
			System.out.println(word.getCoveredText() + " " + ann.getCoveredText());
			if (ann.getBegin()==word.getBegin()
					&& ann.getEnd()==word.getEnd()
					&& checkDeps(word,negation)){
				int begin=Math.min(word.getBegin(),negation.getBegin());
				int end=Math.max(word.getEnd(),negation.getEnd());
				NegProblem obj = new NegProblem(aCAS.getJCas(), begin, end);
				aCAS.addFsToIndexes(obj);
				System.out.println("NEW1: " + obj.getCoveredText());
			}				
		}
		List<AnnotationFS> nothappproblems = getNotHappProblemTriggersFromSentence(sentence);
		for (AnnotationFS ann:nothappproblems){
			if (ann.getBegin()==word.getBegin() 
					&& ann.getEnd()==word.getEnd()
					&& checkDeps(word,negation)){
				int begin=Math.min(word.getBegin(),negation.getBegin());
				int end=Math.max(word.getEnd(),negation.getEnd());
				ProblemTrigger obj = new ProblemTrigger(aCAS.getJCas(), begin, end);
				aCAS.addFsToIndexes(obj);
				System.out.println("NEW2: " + obj.getCoveredText());
			}				
		}
	}
	
	
	public AnnotationFS doesDepContainsNeg(Word word, List<AnnotationFS> anns){
		if (word == null)
				return null;
		for (AnnotationFS ann:anns)
			if (ann.getBegin()==word.getBegin() && ann.getEnd()==word.getEnd()){
				String pos = MorphCasUtils.getFirstPosTag(word);
				//System.out.println("word: " + word.getCoveredText());
				if (pos==null || (!pos.contains("VERB") || pos.contains("past")))
					return ann;
			}				
		return null;
	}
	
	
	public void annotateNegatedDependency(CAS aCAS, Word word,AnnotationFS sentence, AnnotationFS negation) throws CASException{
		if (word == null || negation == null)
				return;
		//negation + action
		List<AnnotationFS> actions = getActionsTriggerFromNegAction(sentence);
		for (AnnotationFS ann:actions){
			if (ann.getBegin()==word.getBegin() && ann.getEnd()==word.getEnd()){
				int begin=Math.min(word.getBegin(),negation.getBegin());
				int end=Math.max(word.getEnd(),negation.getEnd());
				NegAction obj = new NegAction(aCAS.getJCas(), begin, end);
				aCAS.addFsToIndexes(obj);
				System.out.println("NEW3: " + obj.getCoveredText());
			}				
		}
		//negation + negative
		List<AnnotationFS> negatives = getNegativeWordFromSentence(sentence);
		for (AnnotationFS ann:negatives){
			if (ann.getBegin()==word.getBegin() && ann.getEnd()==word.getEnd()){
				int begin=Math.min(word.getBegin(),negation.getBegin());
				int end=Math.max(word.getEnd(),negation.getEnd());
				PositiveWord obj = new PositiveWord(aCAS.getJCas(), begin, end);
				aCAS.addFsToIndexes(obj);
				System.out.println("NEW4: " + obj.getCoveredText());
			}				
		}
		//negation + positive
		List<AnnotationFS> positives = getPositiveWordFromSentence(sentence);
		for (AnnotationFS ann:positives){
			if (ann.getBegin()==word.getBegin() && ann.getEnd()==word.getEnd()){
				int begin=Math.min(word.getBegin(),negation.getBegin());
				int end=Math.max(word.getEnd(),negation.getEnd());
				NegativeWord obj = new NegativeWord(aCAS.getJCas(), begin, end);
				aCAS.addFsToIndexes(obj);
				aCAS.removeFsFromIndexes(negation);
				System.out.println("NEW5: " + obj.getCoveredText());
			}				
		}
	}
	
	public void printAllAnnotationsOfSentence(AnnotationFS sentence){
		System.out.printf("%n== Sentence ==%n");
        System.out.println(sentence.getCoveredText());
        System.out.printf("  %-16s %-10s %-10s  %-10s %n", "TOKEN", "BEGIN", "ENG", "CLASS");
        for (AnnotationFS token : getActionsTriggerFromNegAction(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getProblemTriggersFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getNotHappProblemTriggersFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getNegativeWordFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getPositiveWordFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getNegationForProblemFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getNegationWordFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
	}

	public List<AnnotationFS> getActionsTriggerFromNegAction(AnnotationFS sentence){
		return selectCovered(actionTriggerType, sentence);
	}
	public List<AnnotationFS> getProblemTriggersFromSentence(AnnotationFS sentence){
		return selectCovered(problemWordType, sentence);
	}
	public List<AnnotationFS> getNotHappProblemTriggersFromSentence(AnnotationFS sentence){
		return selectCovered(notProblemWordType, sentence);
	}
	public List<Word> getSentenceWords(AnnotationFS sentence){
		return selectCovered(Word.class, sentence);
	}
	public List<Dependency> getDependencies(AnnotationFS sentence){
		return selectCovered(Dependency.class, sentence);
	}	
	public List<AnnotationFS> getNegationForProblemFromSentence(AnnotationFS sentence){
		return selectCovered(negationForProblemType, sentence);
	}
	public List<AnnotationFS> getNegationWordFromSentence(AnnotationFS sentence){
		return selectCovered(negationWordType, sentence);
	}
	public List<AnnotationFS> getNegativeWordFromSentence(AnnotationFS sentence){
		return selectCovered(negativeWordType, sentence);
	}
	public List<AnnotationFS> getPositiveWordFromSentence(AnnotationFS sentence){
		return selectCovered(positiveWordType, sentence);
	}
	
	
	
	@Override
	public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {
		super.typeSystemInit(typeSystem);
		sentenceType = typeSystem.getType("com.textocat.textokit.segmentation.fstype.Sentence");
		tokenType = typeSystem.getType("com.textocat.textokit.morph.fs.Word");
		actionTriggerType = typeSystem.getType("ru.kpfu.itis.cll.general.ActionTrigger");
		negActionTriggerType = typeSystem.getType("ru.kpfu.itis.cll.general.NegAction");
		problemWordType = typeSystem.getType("ru.kpfu.itis.cll.general.ProblemTrigger");
		notProblemWordType = typeSystem.getType("ru.kpfu.itis.cll.general.NotProblemWord");
		
		negationWordType = typeSystem.getType("ru.kpfu.itis.cll.general.Negation");
		negationForProblemType = typeSystem.getType("ru.kpfu.itis.cll.general.NegationForProblem");
		positiveWordType = typeSystem.getType("ru.kpfu.itis.cll.general.PositiveWord");
		negativeWordType = typeSystem.getType("ru.kpfu.itis.cll.general.NegativeWord");
	}
	
}
