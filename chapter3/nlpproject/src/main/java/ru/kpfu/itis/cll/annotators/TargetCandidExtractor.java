package ru.kpfu.itis.cll.annotators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import static org.apache.uima.fit.util.CasUtil.select;
import static org.apache.uima.fit.util.CasUtil.selectCovered;
import static org.apache.uima.fit.util.JCasUtil.selectCovered;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceSpecifier;
import org.hamcrest.core.IsInstanceOf;

import com.textocat.textokit.depparser.Dependency;
import com.textocat.textokit.morph.fs.Word;
import com.textocat.textokit.postagger.MorphCasUtils;

import ru.kpfu.itis.cll.general.ActionTrigger;
import ru.kpfu.itis.cll.general.OpinionPhrase;
import ru.kpfu.itis.cll.general.OpinionTarget;
import ru.kpfu.itis.cll.general.ProblemPhrase;
import ru.kpfu.itis.cll.utils.FileUtil;

class Phrase{
	AnnotationFS indicator;
	Word target;
	public Phrase(AnnotationFS indicator,Word target) {
		this.indicator=indicator;
		this.target=target;
	}
	
	public AnnotationFS getIndicator() {
		return indicator;
	}
	public void setIndicator(AnnotationFS indicator) {
		this.indicator = indicator;
	}
	public Word getTarget() {
		return target;
	}
	public void setTarget(Word target) {
		this.target = target;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "("+indicator.getCoveredText()+","+target.getCoveredText()+")";
	}
}


public class TargetCandidExtractor extends CasAnnotator_ImplBase {
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_SENTENCE_ANNOTATION_TYPE = "SentenceAnnotation";
	@ConfigurationParameter(name = PN_SENTENCE_ANNOTATION_TYPE, mandatory = false, defaultValue = "com.textocat.textokit.segmentation.fstype.Sentence")
	private String sentenceAnnotationType;
	private Type sentenceType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_TOKEN_ANNOTATION_TYPE = "WordAnnotation";
	@ConfigurationParameter(name = PN_TOKEN_ANNOTATION_TYPE, mandatory = false, defaultValue = "com.textocat.textokit.morph.fs.Word")
	private String tokenAnnotationType;
	private Type tokenType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_ACTION_WORD_ANNOTATION_TYPE = "ActionTriggerAnnotation";
	@ConfigurationParameter(name = PN_ACTION_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.ActionTrigger")
	private String actionTriggerAnnotationType;
	private Type actionTriggerType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_NEG_ACTION_WORD_ANNOTATION_TYPE = "NegActionTriggerAnnotation";
	@ConfigurationParameter(name = PN_ACTION_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.NegAction")
	private String negActionAnnotationType;
	private Type negActionTriggerType = null;
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_PROBLEM_WORD_ANNOTATION_TYPE = "ProblemTriggerAnnotation";
	@ConfigurationParameter(name = PN_PROBLEM_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.ProblemTrigger")
	private String ProblemTriggerAnnotationType;
	private Type problemWordType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_DEF_WORD_ANNOTATION_TYPE = "DeficiencyPhraseAnnotation";
	@ConfigurationParameter(name = PN_DEF_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.DeficiencyPhrase")
	private String DefPhAnnotationType;
	private Type deficiencyPhType = null;		
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_IMPERATIVE_WORD_ANNOTATION_TYPE = "ImperativeTriggerAnnotation";
	@ConfigurationParameter(name = PN_IMPERATIVE_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.ImperativeTrigger")
	private String ImperativeTriggerAnnotationType;
	private Type imperativeTriggerType = null;	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_OPINION_TARGET_ANNOTATION_TYPE = "OpinionTargetAnnotation";
	@ConfigurationParameter(name = PN_OPINION_TARGET_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.OpinionTarget")
	private String OpinionTargetAnnotationType;
	private Type opinionTargetType = null;		
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_OPINION_PHRASE_ANNOTATION_TYPE = "OpinionPhraseAnnotation";
	@ConfigurationParameter(name = PN_OPINION_PHRASE_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.OpinionPhrase")
	private String OpinionPhraseAnnotationType;
	private Type opinionPhraseType = null;			
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_POSITIVE_WORD_ANNOTATION_TYPE = "PositiveWordAnnotation";
	@ConfigurationParameter(name = PN_POSITIVE_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.PositiveWord")
	private String PositiveWordAnnotationType;
	private Type positiveWordType = null;
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_NEGATIVE_WORD_ANNOTATION_TYPE = "NegativeWordAnnotation";
	@ConfigurationParameter(name = PN_NEGATIVE_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.NegativeWord")
	private String NegativeWordAnnotationType;
	private Type negativeWordType = null;		
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PN_ADD_WORD_ANNOTATION_TYPE = "AddWordAnnotation";
	@ConfigurationParameter(name = PN_ADD_WORD_ANNOTATION_TYPE, mandatory = false, defaultValue = "ru.kpfu.itis.cll.general.AddWord")
	private String addWordAnnotationType;
	private Type addWordType = null;		
			
	
	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {
		super.initialize(aContext);
	}
	
	@Override
	public void process(CAS aCAS) throws AnalysisEngineProcessException {	
		for (AnnotationFS sentence : select(aCAS, sentenceType)){
			if (sentence.getCoveredText().length()>3) {
				System.out.println(sentence.getCoveredText());
				//printAllAnnotationsOfSentence(sentence);
				//printDependencyTree(sentence);
				extractTargetsWithDependencies(aCAS, sentence, true);
				
				//select all extracted phrases
				/*List<AnnotationFS> ophrases=getOpinionPhrasesFromSentence(sentence);
				for (AnnotationFS item:ophrases){
					System.out.println("OP: "+item.getCoveredText());										
				}
				ophrases=getProblemTriggersFromSentence(sentence);
				for (AnnotationFS item:ophrases){
					System.out.println("PT: "+item.getCoveredText());										
				}*/
			}	
		}
	}
	
	
	public void addAnnotationsToIndicators(AnnotationFS sentence, List<AnnotationFS> annotations){
		annotations.addAll(getProblemTriggersFromSentence(sentence)); //for problem triggers
		annotations.addAll(getNegActionFromSentence(sentence)); //for action words
		annotations.addAll(getNegativeWordsFromSentence(sentence));	
		annotations.addAll(getAddWordsFromSentence(sentence));	
		//annotations.addAll(getPositiveWordsFromSentence(sentence));	
		annotations.addAll(getImperativeTriggersFromSentence(sentence)); //for imperative triggers
		
	}
	
	
	public void extractTargetsWithDependencies(CAS aCAS, AnnotationFS sentence, boolean shouldFindIndirectDepencies){
		List<Dependency> deps=getDependencies(sentence);
		HashSet<Phrase> phrases = new HashSet<Phrase>();

		List<AnnotationFS> annotations= new ArrayList<AnnotationFS>();
		addAnnotationsToIndicators(sentence, annotations);
		
		
		//for phrases like no monitor, no feature, e.g.
		List<AnnotationFS> deficiencyPhrases=getDeficiencyPhrasesFromSentence(sentence);
		for (AnnotationFS defPhrase:deficiencyPhrases){
			annotations.add(defPhrase);
			for (Word token:getSentenceWords(sentence)){
				if (token.getBegin()>=defPhrase.getBegin() &&
						token.getEnd()<=defPhrase.getEnd()){
					phrases.add(new Phrase(defPhrase,token));	
				}
			}	
		}
		
		
		//the main work starts here		
		phrases.addAll(findTargetsWithDirectConnections(annotations,deps));			
		if (shouldFindIndirectDepencies){
			phrases.addAll(findWordsWithInDirectConnections(annotations,deps));
		}
	
		ArrayList<AnnotationFS> arrToSave=new ArrayList<AnnotationFS>();				
		HashSet<String> itemsToSave = new HashSet<String>();
		for (Phrase phrase:phrases){
			Word target = phrase.getTarget();
			String posTag = "";
			if (target!=null)
				posTag = MorphCasUtils.getFirstPosTag(target);
			AnnotationFS indicator = phrase.getIndicator();
			//System.out.println(phrase);
			if (posTag==null||(posTag.contains("NOUN") 
					|| posTag.contains("NPRO"))
					&&!isIndicatorAnnotation(target, annotations)
					//&&lines.contains(target.getCoveredText())
						){
				try {
					Word otarget=target;
					arrToSave.add(indicator);
					
					//create target
					OpinionTarget ot=new OpinionTarget(aCAS.getJCas(),otarget.getBegin(), otarget.getEnd());
					ot.setPos(posTag);					
					System.out.println("TEMP "+indicator.getCoveredText()+";"+ot.getCoveredText());
					
					String tempRepr=indicator.getCoveredText()+";"+ot.getCoveredText();
					if (!itemsToSave.contains(tempRepr)){	
						itemsToSave.add(tempRepr);
						//create opinion phrase as a combination (indicator, target);
						int begin=Math.min(indicator.getBegin(),ot.getBegin());
						int end=Math.max(indicator.getEnd(),ot.getEnd());
						OpinionPhrase oph=new OpinionPhrase(aCAS.getJCas(), begin, end);
						oph.setOindicator((Annotation) indicator);
						//setOindicator(new ProblemPhrase(aCAS.getJCas(), phrase.getIndicator().getBegin(), phrase.getIndicator().getEnd()));
						oph.setOtarget(ot);	
						aCAS.addFsToIndexes(oph);
						aCAS.addFsToIndexes(ot);
					}
				} catch (CASException e) {  
					e.printStackTrace();
				}
			} 
		}
		for (Phrase phrase:phrases){
			AnnotationFS indicator = phrase.getIndicator();
			if (!arrToSave.contains(indicator)){
				System.out.println("DELETE "+indicator.getCoveredText());
				aCAS.removeFsFromIndexes(indicator);
			}
		}
		
	}
	
	public int getBeginInd(Dependency dep){
		int begin=-1;
		if (dep.getHead() == null)
			begin=dep.getDependent().getBegin();
		if (dep.getDependent() == null)
			begin=dep.getHead().getBegin();	
		if (begin<0)
			begin=Math.min(dep.getDependent().getBegin(),dep.getHead().getBegin());
		//System.out.println("B:" + begin);
		return begin;
	}
	public int getEngInd(Dependency dep){
		int end=-1;
		if (dep.getHead() == null)
			end=dep.getDependent().getEnd();
		if (dep.getDependent() == null)
			end=dep.getHead().getEnd();	
		if (end<0)
			end=Math.max(dep.getDependent().getEnd(),dep.getHead().getEnd());
		//System.out.println("E: " + end);
		return end;
	}
	
	public Dependency getDependencyForMultiTarget(Word target, List<Dependency> deps){
		Iterator<Dependency> depIter = deps.iterator();
		while (depIter.hasNext()) { //over dependencies
			Dependency dep = (Dependency) depIter.next();
			if (
				(doesWordEqualAnnotation(dep.getDependent(), target) 
						|| doesWordEqualAnnotation(dep.getHead(), target))){
				return dep;
			}
		}
		return null;
	}
	
	public Dependency getOrAndDepedency(Word target, List<Dependency> deps){
		Iterator<Dependency> depIter = deps.iterator();
		while (depIter.hasNext()) { //over dependencies
			Dependency dep = (Dependency) depIter.next();
			if ((doesWordEqualAnnotation(dep.getDependent(), target) || doesWordEqualAnnotation(dep.getHead(), target)) 
					&& dep.getHead()!=null
					&& dep.getDependent()!=null) {
				String pos1=MorphCasUtils.getFirstPosTag(dep.getDependent());
				String pos=MorphCasUtils.getFirstPosTag(dep.getHead());
				if (pos!=null && pos1!=null){ //&& (pos.contains("CONJ") || pos.contains("PRED")|| pos.contains("NUMR")))	
					pos1=pos1.split("&")[0];
					pos=pos.split("&")[0];
					if (pos1.equals(pos))
						return dep;
				}
			}
			if (doesWordEqualAnnotation(dep.getDependent(), target) && dep.getHead()!=null) {
				String pos=MorphCasUtils.getFirstPosTag(dep.getHead());
				if (pos!=null && (pos.contains("CONJ") || pos.contains("PRED")|| pos.contains("NUMR")))				
					return dep;
			}
		}
		return null;
	}
	
	public ArrayList<Phrase> findTargetsForProblemWord(AnnotationFS problemWord, List<Dependency> deps, boolean doCheckDependent, boolean doCheckGovernor){
		ArrayList<Phrase> phrases= new ArrayList<Phrase>();
		ArrayList<Word> tokens = new ArrayList<Word> ();
		tokens.addAll(getWordsFromAnnotation(problemWord));
		//if (tokens.isEmpty()&&(problemWord instanceof Word))
			//tokens.add((Word)problemWord);
		for (Word token:tokens){
			String posValue = MorphCasUtils.getFirstPosTag(token);
			//System.out.println(problemWord.getCoveredText()+" "+token.getCoveredText()+" "+posValue);
			if (posValue==null || (!posValue.equals("RB")&&!posValue.equals("MD"))){
				for (Dependency dep:deps){
				if (doCheckGovernor && doesWordEqualAnnotation(dep.getDependent(), token)){
					if (dep.getHead()!=null)
						phrases.add(new Phrase(problemWord,dep.getHead()));
				}
				if (doCheckDependent && doesWordEqualAnnotation(dep.getHead(), token)){
					if (dep.getDependent()!=null)
						phrases.add(new Phrase(problemWord,dep.getDependent()));
				}
			}}
		}
		for (Dependency dep:deps){
			if (doCheckGovernor && doesWordEqualAnnotation(dep.getDependent(), problemWord)){
					phrases.add(new Phrase(problemWord,dep.getHead()));
			}
			if (doCheckDependent && doesWordEqualAnnotation(dep.getHead(), problemWord)){
				phrases.add(new Phrase(problemWord,dep.getDependent()));
			}
		}
		
		
		return phrases;
	}
	public ArrayList<Phrase> findTargetsForProblemWord(AnnotationFS problemWord,AnnotationFS indirectWord, List<Dependency> deps, boolean doCheckDependent, boolean doCheckGovernor){
		ArrayList<Phrase> phrases= new ArrayList<Phrase>();
		ArrayList<Word> tokens = new ArrayList<Word> ();
		List<Word> lwords = getWordsFromAnnotation(indirectWord);
			if (lwords!=null && !lwords.isEmpty())
				tokens.addAll(lwords);
		if (tokens.isEmpty())
			tokens.add((Word)indirectWord);
		for (Word token:tokens){
			String posValue = MorphCasUtils.getFirstPosTag(token);
			//System.out.println(token.getCoveredText()+" "+posValue);
			if (posValue==null || (!posValue.equals("RB")&&!posValue.equals("MD"))){
				for (Dependency dep:deps){
				if (doCheckGovernor && doesWordEqualAnnotation(dep.getDependent(), token)){
						phrases.add(new Phrase(problemWord,dep.getHead()));
				}
				if (doCheckDependent && doesWordEqualAnnotation(dep.getHead(), token)){
					phrases.add(new Phrase(problemWord,dep.getDependent()));
				}
			}
		}
		}
		return phrases;
	}
	
	public ArrayList<Phrase> findTargetsWithDirectConnections(List<AnnotationFS> triggers, List<Dependency> deps){
		ArrayList<Phrase> phrases= new ArrayList<Phrase>();
		for (AnnotationFS indicator:triggers){ //over indicators
			ArrayList<Phrase> targets=findTargetsForProblemWord(indicator, deps, true, true);
			phrases.addAll(targets);
		}
		return phrases;
	}

	public ArrayList<Phrase> findWordsWithInDirectConnections(List<AnnotationFS> triggers, List<Dependency> deps){
		ArrayList<Phrase> tokens= new ArrayList<Phrase>();
		for (AnnotationFS problemWord:triggers){ //over problem indicators
			for (AnnotationFS token:getWordsFromAnnotation(problemWord)){
				for (Dependency dep:deps){	
					Word stoken=null;
					if (doesWordEqualAnnotation(dep.getDependent(), token))
						stoken=dep.getHead();
					if (doesWordEqualAnnotation(dep.getHead(), token))
						stoken=dep.getDependent();
					
					if (stoken!=null){
						Dependency conjdep=getOrAndDepedency(stoken,deps);
						if (conjdep!=null){
							if (conjdep.getDependent()!=null)
								tokens.addAll(findTargetsForProblemWord(problemWord, conjdep.getDependent(), deps, true, true));
							if (conjdep.getHead()!=null)
								tokens.addAll(findTargetsForProblemWord(problemWord, conjdep.getHead(), deps, true, true));
						} else
							tokens.addAll(findTargetsForProblemWord(problemWord, stoken, deps, true, true));
					}
						//depIter.remove();
					}
			}
		}		
		return tokens;
	}
	
	
	public boolean doesWordEqualAnnotation(Word token, AnnotationFS ann){
		if (token == null)
			return false;
		return token.getBegin() >= ann.getBegin() && token.getEnd() <= ann.getEnd();
	}
	public boolean isWordOpinionTarget(Word token, AnnotationFS sentence){
		List<AnnotationFS> targets=getOpinionTargetsFromSentence(sentence);
		for (AnnotationFS ann:targets)
			if (token.getBegin() == ann.getBegin() && token.getEnd() == ann.getEnd())
				return true;
		return false;
	}
	
	public boolean isIndicatorAnnotation(Word token, List<AnnotationFS> anns){
		for (AnnotationFS ann:anns)
			if (token.getBegin() >=  ann.getBegin() && token.getEnd() <= ann.getEnd())
				return true;
		return false;
	}
	
	
	
	/*
	 * print all annotations
	 */
	public void printAllAnnotationsOfSentence(AnnotationFS sentence){
		System.out.printf("%n== Sentence ==%n");
        System.out.println(sentence.getCoveredText());
        System.out.printf("  %-16s %-10s %-10s  %-10s %n", "TOKEN", "BEGIN", "ENG", "CLASS");
        for (AnnotationFS token : getProblemTriggersFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getDeficiencyPhrasesFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getImperativeTriggersFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS token : getNegativeWordsFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s %-10s %n", 
        			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
        }
        for (AnnotationFS negActionWord : getNegActionFromSentence(sentence)) {            
        	System.out.printf("%-16s %-10s %-10s  %-10s %n", 
            		negActionWord.getCoveredText(), negActionWord.getBegin(), negActionWord.getEnd(), negActionWord.getClass());
            for (AnnotationFS token : getActionsTriggerFromNegAction(negActionWord)) {
            	System.out.printf("  %-16s %-10s %-10s  %-10s %n", 
            			token.getCoveredText(), token.getBegin(), token.getEnd(), token.getClass());
            }
        }
	}
	public void printDependencyTree(AnnotationFS sentence){
      System.out.printf("== Sentence ==%n");
            System.out.printf("  %-16s %-10s %-10s %-10s %-10s %n", "TOKEN", "LEMMA", "STEM",
                    "CPOS", "POS");
            for (Word token : selectCovered(Word.class, sentence)) {
                System.out.printf("  %-16s %-10s %-10s %-10s %-10s %n", 
                        token.getCoveredText(), 
                        token.getBegin(),
                        token.getEnd(),
                        MorphCasUtils.getFirstPosTag(token),
                        MorphCasUtils.getFirstPosTag(token));
            }
            
            System.out.printf("-- Dependency relations --%n");
            System.out.printf("  %-16s %-10s %-10s %n", "TOKEN", "DEPREL", "DEP", "GOV");
            for (Dependency dep : selectCovered(Dependency.class, sentence)) {
                System.out.printf("  %-16s %-10s %-10s %n", 
                        dep.getLabel(), 
                        getWordInfo(dep.getDependent()), 
                        getWordInfo(dep.getHead()));
            }
	}
	
    public String getWordInfo(Word w){
    	if (w == null)
    		return "--";
    	String src = w.getCoveredText();
        String lemma = MorphCasUtils.getFirstLemma(w);
        String posTag = MorphCasUtils.getFirstPosTag(w);
        return String.format("%s/%s/%s", src, lemma, posTag);
    
    }
	
	
	/*
	 * find all covered annotations
	 */
	public List<Word> getSentenceWords(AnnotationFS sentence){
		return selectCovered(Word.class, sentence);
	}
	public List<Dependency> getDependencies(AnnotationFS sentence){
		return selectCovered(Dependency.class, sentence);
	}	
	public List<AnnotationFS> getNegActionFromSentence(AnnotationFS sentence){
		return selectCovered(negActionTriggerType, sentence);
	}
	public List<AnnotationFS> getActionsTriggerFromSentenceNegActions(AnnotationFS sentence){
		List<AnnotationFS> list = new ArrayList<AnnotationFS>();
		for (AnnotationFS negActionWord : getNegActionFromSentence(sentence))
			list.addAll(getActionsTriggerFromNegAction(negActionWord));
		return list;
	}
	public List<AnnotationFS> getActionsTriggerFromNegAction(AnnotationFS negActionWord){
		return selectCovered(actionTriggerType, negActionWord);
	}
	public List<Word> getWordsFromAnnotation(AnnotationFS ann){
		List<Word> list = selectCovered(Word.class, ann);
		for (Word l : list)
			System.out.println("W:" + l.getCoveredText());
		return selectCovered(Word.class, ann);
	}
	public List<AnnotationFS> getProblemTriggersFromSentence(AnnotationFS sentence){
		return selectCovered(problemWordType, sentence);
	}
	public List<AnnotationFS> getImperativeTriggersFromSentence(AnnotationFS sentence){
		return selectCovered(imperativeTriggerType, sentence);
	}
	public List<AnnotationFS> getDeficiencyPhrasesFromSentence(AnnotationFS sentence){
		return selectCovered(deficiencyPhType, sentence);
	}
	public List<AnnotationFS> getOpinionTargetsFromSentence(AnnotationFS sentence){
		return selectCovered(opinionTargetType, sentence);
	}
	public List<AnnotationFS> getOpinionPhrasesFromSentence(AnnotationFS sentence){
		return selectCovered(opinionPhraseType, sentence);
	}
	public List<AnnotationFS> getPositiveWordsFromSentence(AnnotationFS sentence){
		return selectCovered(positiveWordType, sentence);
	}
	public List<AnnotationFS> getNegativeWordsFromSentence(AnnotationFS sentence){
		//List<AnnotationFS> list = selectCovered(negativeWordType, sentence);
		//for (AnnotationFS l : list)
			//list.addAll(getSentenceWords(l));
		return selectCovered(negativeWordType, sentence);
	}
	public List<AnnotationFS> getAddWordsFromSentence(AnnotationFS sentence){
		return selectCovered(addWordType, sentence);
	}
	
	
	@Override
	public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {
		super.typeSystemInit(typeSystem);
		sentenceType = typeSystem.getType("com.textocat.textokit.segmentation.fstype.Sentence");
		tokenType = typeSystem.getType("com.textocat.textokit.morph.fs.Word");
		actionTriggerType = typeSystem.getType("ru.kpfu.itis.cll.general.ActionTrigger");
		negActionTriggerType = typeSystem.getType("ru.kpfu.itis.cll.general.NegAction");
		problemWordType = typeSystem.getType("ru.kpfu.itis.cll.general.ProblemTrigger");
		deficiencyPhType = typeSystem.getType("ru.kpfu.itis.cll.general.DeficiencyPhrase");
		imperativeTriggerType = typeSystem.getType("ru.kpfu.itis.cll.general.ImperativeTrigger");
		opinionTargetType = typeSystem.getType("ru.kpfu.itis.cll.general.OpinionTarget");
		opinionPhraseType = typeSystem.getType("ru.kpfu.itis.cll.general.OpinionPhrase");
		positiveWordType = typeSystem.getType("ru.kpfu.itis.cll.general.PositiveWord");
		negativeWordType = typeSystem.getType("ru.kpfu.itis.cll.general.NegativeWord");
		addWordType = typeSystem.getType("ru.kpfu.itis.cll.general.AddWord");
	}
}
