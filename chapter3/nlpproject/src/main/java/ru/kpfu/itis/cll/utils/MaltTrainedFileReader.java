package ru.kpfu.itis.cll.utils;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngine;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.factory.TypeSystemDescriptionFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.ruta.engine.RutaEngine;

import ru.kpfu.itis.cll.annotators.CustomWordAnnotator;
import ru.kpfu.itis.cll.annotators.TargetCandidExtractor;

import com.textocat.textokit.commons.util.PipelineDescriptorUtils;
import com.textocat.textokit.depparser.mst.MSTParsingAnnotator;
import com.textocat.textokit.morph.dictionary.MorphDictionaryAPIFactory;
import com.textocat.textokit.morph.fs.Word;
import com.textocat.textokit.morph.lemmatizer.LemmatizerAPI;
import com.textocat.textokit.postagger.MorphCasUtils;
import com.textocat.textokit.postagger.PosTaggerAPI;
import com.textocat.textokit.segmentation.SentenceSplitterAPI;
import com.textocat.textokit.tokenizer.TokenizerAPI;

import mstparser.DependencyInstance;

public class MaltTrainedFileReader {
	AnalysisEngine mainPipeline = null;
	JCas jcas=null;
	
	public MaltTrainedFileReader() {
		try {
			AnalysisEngine mainPipeline = createPipeline(true);
			JCas jcas = createJCas();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static AnalysisEngine createPipeline(boolean useDeps) throws Exception {
		Object[] configurationData = {
				PosTaggerAPI.DEFAULT_REUSE_EXISTING_WORD_ANNOTATIONS, "true",
				PosTaggerAPI.PARAM_REUSE_EXISTING_WORD_ANNOTATIONS, "true" };		
		AnalysisEngineDescription posTaggerAE = createEngineDescription(
				PosTaggerAPI.AE_POSTAGGER, configurationData);
		AnalysisEngineDescription aeDesc = createEngineDescription(
				createEngineDescription(TokenizerAPI.AE_TOKENIZER),
				createEngineDescription(SentenceSplitterAPI.AE_SENTENCE_SPLITTER),
				posTaggerAE,
				createEngineDescription(LemmatizerAPI.AE_LEMMATIZER));		
		ExternalResourceDescription morphDictDesc = MorphDictionaryAPIFactory
				.getMorphDictionaryAPI()
				.getResourceDescriptionForCachedInstance();
		morphDictDesc.setName(PosTaggerAPI.MORPH_DICTIONARY_RESOURCE_NAME);
		PipelineDescriptorUtils.getResourceManagerConfiguration(aeDesc)
				.addExternalResource(morphDictDesc);
		return createEngine(aeDesc);		
	}
	public static JCas createJCas() throws Exception {
		TypeSystemDescription tsDesc = TypeSystemDescriptionFactory
				.createTypeSystemDescription(
						"com.textocat.textokit.commons.Commons-TypeSystem",
						"com.textocat.textokit.depparser.dependency-ts",
						"ru.kpfu.itis.cll.main.BasicEnProblemExtractorEngine",
						"ru.kpfu.itis.cll.main.BasicTypeSystem",
						TokenizerAPI.TYPESYSTEM_TOKENIZER,
						SentenceSplitterAPI.TYPESYSTEM_SENTENCES,
						PosTaggerAPI.TYPESYSTEM_POSTAGGER,
						LemmatizerAPI.TYPESYSTEM_LEMMATIZER);
		JCas jcas = JCasFactory.createJCas(tsDesc);
		return jcas;
	}
	
	public void processFile(String file){
		ArrayList<String> newFile= new ArrayList<String>();
		ArrayList<String> currentSentence= new ArrayList<String>();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(new File(file)));
			String line = null;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				if (line.isEmpty()) {
					newFile.addAll(processSentence(currentSentence));
					currentSentence.removeAll(currentSentence);
				} else
					currentSentence.add(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		FileUtil.writeLines(file+"edited.txt", newFile);
	}
	public ArrayList<String> processSentence(ArrayList<String> currentS){
		ArrayList<String> newS = new ArrayList<String>();
		StringBuilder tb = new StringBuilder();
		for (String str:currentS) {
			tb.append(" "+str.split("\t")[0]);
		}
		String res = tb.toString().trim();
		try {
			parseSentence(res);
			Collection<Word> words = JCasUtil.select(jcas, Word.class);
			Iterator<Word> wordIter = words.iterator();
			for (String str:currentS) {
				str=str.trim();
				Word w = wordIter.next();
				if (!str.equals(w.getCoveredText()))
					System.err.println(str + "!=" + w.getCoveredText());
				String[] parts = str.split("\t");
				newS.add(parts[0]+"\t"+MorphCasUtils.getFirstPosTag(w)+"\t"+parts[2]);
			}
		} catch (AnalysisEngineProcessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		newS.add("\n");
		return newS;		
	}
	public void parseSentence(String sent) throws AnalysisEngineProcessException{
		jcas.reset();
		jcas.setDocumentText(sent);
		jcas.setDocumentLanguage("ru");
		mainPipeline.process(jcas);
	}
}
class Main{
	public static void main(String[] args) throws Exception {
		MaltTrainedFileReader mr= new MaltTrainedFileReader();
		mr.processFile("/Users/telena/Documents/JAVA_Development/MaltParser/maltparser-1.8.1/corpus.tab");

	}
}
