package ru.kpfu.itis.cll.main;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;

import nlpproject.WordPosLemmaWriter;

import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.factory.TypeSystemDescriptionFactory;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngine;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.util.JCasUtil.select;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.resource.ExternalResourceDescription;
import org.apache.uima.resource.metadata.MetaDataObject;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.apache.uima.ruta.engine.RutaEngine;

import com.google.common.collect.Maps;
import com.textocat.textokit.commons.util.PipelineDescriptorUtils;
import com.textocat.textokit.depparser.mst.MSTParsingAnnotator;
import com.textocat.textokit.morph.commons.SimplyWordAnnotator;
import com.textocat.textokit.morph.dictionary.MorphDictionaryAPIFactory;
import com.textocat.textokit.morph.lemmatizer.LemmatizerAPI;
import com.textocat.textokit.postagger.PosTaggerAPI;
import com.textocat.textokit.segmentation.SentenceSplitterAPI;
import com.textocat.textokit.tokenizer.TokenizerAPI;

import ru.kpfu.itis.cll.utils.POSDetector;
import ru.kpfu.itis.cll.general.DeficiencyPhrase;
import ru.kpfu.itis.cll.general.NegAction;
import ru.kpfu.itis.cll.general.NotProblemSentence;
import ru.kpfu.itis.cll.general.OpinionTarget;
import ru.kpfu.itis.cll.general.ProblemSentence;
import ru.kpfu.itis.cll.general.ImperativeTrigger;
import ru.kpfu.itis.cll.general.ProblemTrigger;
import ru.kpfu.itis.cll.utils.Evaluation;
import ru.kpfu.itis.cll.utils.FileUtil;
import ru.kpfu.itis.cll.utils.Item;
import ru.kpfu.itis.cll.annotators.CustomWordAnnotator;
import ru.kpfu.itis.cll.annotators.NegatedAnnDetector;
import ru.kpfu.itis.cll.annotators.NonDomainTargetsReducer;
import ru.kpfu.itis.cll.annotators.TargetCandidExtractor;
import ru.kpfu.itis.cll.annotators.SpecificTargetsSelector;

public class ProblemExtractorForRussian {
	private static final String OUTPUT_BASE = "output/";
	public static POSDetector detector = new POSDetector();

	public static JCas createJCas() throws Exception {
		TypeSystemDescription tsDesc = TypeSystemDescriptionFactory
				.createTypeSystemDescription(
						"com.textocat.textokit.commons.Commons-TypeSystem",
						"com.textocat.textokit.depparser.dependency-ts",
						"ru.kpfu.itis.cll.main.BasicEnProblemExtractorEngine",
						"ru.kpfu.itis.cll.main.BasicTypeSystem",
						TokenizerAPI.TYPESYSTEM_TOKENIZER,
						SentenceSplitterAPI.TYPESYSTEM_SENTENCES,
						PosTaggerAPI.TYPESYSTEM_POSTAGGER,
						LemmatizerAPI.TYPESYSTEM_LEMMATIZER);
		JCas jcas = JCasFactory.createJCas(tsDesc);
		return jcas;
	}

	public static AnalysisEngine createPipeline(boolean useDeps) throws Exception {
		// UIMAFramework.getLogger().setLevel(Level.OFF);

		AnalysisEngineDescription textmarker = createEngineDescription(
				RutaEngine.class, RutaEngine.PARAM_MAIN_SCRIPT,
				"ru.kpfu.itis.cll.main.BasicProblemExtractor",
				RutaEngine.PARAM_DEBUG, true);
		AnalysisEngineDescription clausemarker = createEngineDescription(
				RutaEngine.class, RutaEngine.PARAM_MAIN_SCRIPT,
				"ru.kpfu.itis.cll.main.ClauseProblemExtractor",
				RutaEngine.PARAM_DEBUG, false);

		AnalysisEngineDescription parserDesc = MSTParsingAnnotator
				.createDescription(new URL(
						"file:/Users/telena/Documents/JAVA_Development/MaltParser/mstparser/dep-full.model"));

		Object[] configurationData = {
				PosTaggerAPI.DEFAULT_REUSE_EXISTING_WORD_ANNOTATIONS, "true",
				PosTaggerAPI.PARAM_REUSE_EXISTING_WORD_ANNOTATIONS, "true" };
		
		//an additional dictionary with Mystem lemmas used in Clause-based Method
		Object[] configurationSW = {CustomWordAnnotator.PARAM_MODEL_LOCATION, 
				"/Users/telena/Documents/JAVA_Development/"
				+ "newworkspace/nlpproject/src/main/resources/dictionaries/stemmed/apps_ru.txt"};
		
		AnalysisEngineDescription posTaggerAE = createEngineDescription(
				PosTaggerAPI.AE_POSTAGGER, configurationData);

		AnalysisEngineDescription aeDesc = null;
		
		if (useDeps)
			aeDesc = createEngineDescription(
					createEngineDescription(TokenizerAPI.AE_TOKENIZER),
					createEngineDescription(SentenceSplitterAPI.AE_SENTENCE_SPLITTER),
					posTaggerAE,
					createEngineDescription(LemmatizerAPI.AE_LEMMATIZER),
					//create SimplyWord annotations and set them lemmas from Mystem
					createEngineDescription(CustomWordAnnotator.class, configurationSW),				
					parserDesc,	
					textmarker,
					/*	//use it if you want to find negations by dependency tree instead of textmarker	
					createEngineDescription(
							RutaEngine.class, RutaEngine.PARAM_MAIN_SCRIPT,
							"ru.kpfu.itis.cll.main.BasicProblemMapping",
							RutaEngine.PARAM_DEBUG, false),					
					createEngineDescription(NegatedAnnDetector.class),					
					createEngineDescription(
							RutaEngine.class, RutaEngine.PARAM_MAIN_SCRIPT,
							"ru.kpfu.itis.cll.main.BasicProblemLogics",
							RutaEngine.PARAM_DEBUG, false));*/
					// print some information about Annotations
					createEngineDescription(WordPosLemmaWriter.class),
					//find targets
					createEngineDescription(TargetCandidExtractor.class),
					//mark non-domain targes
		            createEngineDescription(SpecificTargetsSelector.class,
		            		   SpecificTargetsSelector.PARAM_MODEL_LOCATION,"/Users/telena/Documents/JAVA_Development/newworkspace/nlpproject/src/main/resources/dictionaries/sim/fin-cands-w2v.txt",
		            		   SpecificTargetsSelector.PARAM_MIN_SCORE_VALUE,Float.valueOf("0.0001"),
		            		   SpecificTargetsSelector.PARAM_IS_MIN_SCORE_PRTC,true),
		            //reduce non-domain
		            createEngineDescription(NonDomainTargetsReducer.class),
					clausemarker);


		ExternalResourceDescription morphDictDesc = MorphDictionaryAPIFactory
				.getMorphDictionaryAPI()
				.getResourceDescriptionForCachedInstance();
		morphDictDesc.setName(PosTaggerAPI.MORPH_DICTIONARY_RESOURCE_NAME);
		PipelineDescriptorUtils.getResourceManagerConfiguration(aeDesc)
				.addExternalResource(morphDictDesc);

		AnalysisEngine mainPipeline = createEngine(aeDesc);
		return mainPipeline;
	}

	public static void runAnalysisEngineForCorpus(AnalysisEngine mainPipeline,
			JCas jcas) throws Exception {		
		String mainPath="/Users/telena/Dropbox/ProblemExtraction/final_corpus/ru/";
		ArrayList<Item> objects = getItemsFromRussianTexts(mainPath, 2);

		for (Item obj : objects) {
			jcas.reset();
			//System.out.println("!!!"+ obj.getText());
			jcas.setDocumentText(obj.getText());
			jcas.setDocumentLanguage("ru");
			mainPipeline.process(jcas);
			Collection<ProblemSentence> prphrases = select(jcas,
					ProblemSentence.class);				
			// Collection<NegAction> negac=select(jcas, NegAction.class);
			Collection<DeficiencyPhrase> defphrases = select(jcas,
					DeficiencyPhrase.class);
			Collection<OpinionTarget> targets = select(jcas,
					OpinionTarget.class);
			Collection<NotProblemSentence> nonprphrases = select(jcas,
					NotProblemSentence.class);			
			int res = fanalyze(jcas, obj,prphrases, nonprphrases, defphrases,targets);
			
			for (AnnotationFS date:CasUtil.selectAll(jcas.getCas()))
	            System.out.println("Found: " + date.getCoveredText()+" "+date.getType()); 
			System.out.println("!!!!" + obj.toString());  	    
		}
		Evaluation.runEval(objects);
	}

	public static void runAnalysisEngineForText(AnalysisEngine mainPipeline,
			JCas jcas, String text) throws Exception {
		System.out.println(text);
		jcas.reset();
		jcas.setDocumentText(text);
		jcas.setDocumentLanguage("ru");
		mainPipeline.process(jcas);
		
		Collection<ProblemSentence> prphrases = select(jcas,
				ProblemSentence.class);
		// Collection<NegAction> negac=select(jcas, NegAction.class);
		Collection<DeficiencyPhrase> defphrases = select(jcas,
				DeficiencyPhrase.class);
		Collection<OpinionTarget> targets = select(jcas,
				OpinionTarget.class);
		Collection<NotProblemSentence> nonprphrases = select(jcas,
				NotProblemSentence.class);
		
		 ArrayList<AnnotationFS> defs = new  ArrayList<AnnotationFS>();
		for (AnnotationFS item:defphrases){
        	System.out.println(item.getCoveredText());
            LinkedList<String> tags = detector.getTags(item.getCoveredText(),"pos");
        	if (tags.size()==0){
        		defs.add(new Annotation(1, null));
        		break;
        	}
        	for (String tag:tags){
        		if (tag.equals("substantive")){
        			defs.add(new Annotation(1, null));
        			break;
        		}
        	}
        	defs.add(new Annotation(1, null));
		}
		if (prphrases.size()>0 || defs.size()>0) {
			System.out.println("pr");	
	    } else { 
	    	Collection<ImperativeTrigger> impphrases=select(jcas, ImperativeTrigger.class);
			if (!impphrases.isEmpty())
				System.out.println("pr");       	
			else 
				System.out.println("no--pr");
	    } 
		 
        for (AnnotationFS date:CasUtil.selectAll(jcas.getCas()))
            System.out.println("Found: " + date.getCoveredText()+" "+date.getType());
		
	}
	
	
	public static int fanalyze(JCas jcas, Item obj,
			Collection<ProblemSentence> prphrases, 
			Collection<NotProblemSentence> nonprphrases,
			Collection<DeficiencyPhrase> defphrases,
			Collection<OpinionTarget> targets){
        int res=0; 
        if (!targets.isEmpty()&&nonprphrases.isEmpty()) {        
        //if (prphrases.size()>0 || defphrases.size()>0) {
	    		res=1;
	        	obj.setProblemClass();        	
	    } else {      
	      Collection<ImperativeTrigger> impphrases=select(jcas, ImperativeTrigger.class);
		  if (impphrases.size()>0){
			 res=1;
		     obj.setProblemClass();
		  } else 
		     obj.setNoProblemClass(); 
	    } 
	    return res;
	}
	

	public static void main(String[] args) throws Exception {
		AnalysisEngine mainPipeline = createPipeline(true);
		JCas jcas = createJCas();
		runAnalysisEngineForCorpus(mainPipeline, jcas);
		//String text="когда калину только купили; дверь багажника закрывалась плохо; приходилось очень сильно хлопать.";		
		//String text="показывает штрафы двухлетней давности. они оплачены, а отображаются как просроченные.";
		//String text="в приложении тинькофф банка я вижу сколько осталось до конца лимита, а здесь нет.";
		//String text="не могу сделать перевод между счетами выдает неизвестная ошибка. исправьте пожалуйста.";
		//String text="на трассе особо не покатаешься; трудно обгонять.";
		//String text="наблюдать";
		//runAnalysisEngineForText(mainPipeline,jcas,text);

	}

	public static ArrayList<Item> getItemsFromRussianTexts(String mainPath,
			int datasetID) {
		//String fileName = "otzovik_cars_rating_full.csv_stemmed.txt";
		String fileName = "mobile_app_ru_rating_only2.txt_stemmed.txt";
		ArrayList<Item> objects = new ArrayList<Item>();
		FileUtil.readCSVLines(mainPath + fileName, objects, 2, 3, 4);
		return objects;
	}

}
