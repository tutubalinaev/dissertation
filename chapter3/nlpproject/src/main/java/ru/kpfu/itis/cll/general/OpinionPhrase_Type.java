
/* First created by JCasGen Mon Nov 09 16:33:50 MSK 2015 */
package ru.kpfu.itis.cll.general;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** Type defined in ru.kpfu.itis.cll.general
 * Updated by JCasGen Mon Nov 09 19:41:58 MSK 2015
 * @generated */
public class OpinionPhrase_Type extends Annotation_Type {
  /** @generated 
   * @return the generator for this type
   */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (OpinionPhrase_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = OpinionPhrase_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new OpinionPhrase(addr, OpinionPhrase_Type.this);
  			   OpinionPhrase_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new OpinionPhrase(addr, OpinionPhrase_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = OpinionPhrase.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("ru.kpfu.itis.cll.general.OpinionPhrase");
 
  /** @generated */
  final Feature casFeat_oindicator;
  /** @generated */
  final int     casFeatCode_oindicator;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getOindicator(int addr) {
        if (featOkTst && casFeat_oindicator == null)
      jcas.throwFeatMissing("oindicator", "ru.kpfu.itis.cll.general.OpinionPhrase");
    return ll_cas.ll_getRefValue(addr, casFeatCode_oindicator);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setOindicator(int addr, int v) {
        if (featOkTst && casFeat_oindicator == null)
      jcas.throwFeatMissing("oindicator", "ru.kpfu.itis.cll.general.OpinionPhrase");
    ll_cas.ll_setRefValue(addr, casFeatCode_oindicator, v);}
    
  
 
  /** @generated */
  final Feature casFeat_otarget;
  /** @generated */
  final int     casFeatCode_otarget;
  /** @generated
   * @param addr low level Feature Structure reference
   * @return the feature value 
   */ 
  public int getOtarget(int addr) {
        if (featOkTst && casFeat_otarget == null)
      jcas.throwFeatMissing("otarget", "ru.kpfu.itis.cll.general.OpinionPhrase");
    return ll_cas.ll_getRefValue(addr, casFeatCode_otarget);
  }
  /** @generated
   * @param addr low level Feature Structure reference
   * @param v value to set 
   */    
  public void setOtarget(int addr, int v) {
        if (featOkTst && casFeat_otarget == null)
      jcas.throwFeatMissing("otarget", "ru.kpfu.itis.cll.general.OpinionPhrase");
    ll_cas.ll_setRefValue(addr, casFeatCode_otarget, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	 * @generated
	 * @param jcas JCas
	 * @param casType Type 
	 */
  public OpinionPhrase_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_oindicator = jcas.getRequiredFeatureDE(casType, "oindicator", "uima.tcas.Annotation", featOkTst);
    casFeatCode_oindicator  = (null == casFeat_oindicator) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_oindicator).getCode();

 
    casFeat_otarget = jcas.getRequiredFeatureDE(casType, "otarget", "ru.kpfu.itis.cll.general.OpinionTarget", featOkTst);
    casFeatCode_otarget  = (null == casFeat_otarget) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_otarget).getCode();

  }
}



    