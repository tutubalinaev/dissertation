package ru.kpfu.itis.cll.annotators;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;

import com.textocat.textokit.morph.fs.SimplyWord;
import com.textocat.textokit.morph.fs.Word;
import com.textocat.textokit.morph.fs.Wordform;
import com.textocat.textokit.postagger.MorphCasUtils;
import com.textocat.textokit.segmentation.fstype.Sentence;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.ruta.type.Document;

import ru.kpfu.itis.cll.utils.FileUtil;


public class CustomWordAnnotator extends JCasAnnotator_ImplBase {
	
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PARAM_MODEL_LOCATION = "";
	@ConfigurationParameter(name = PARAM_MODEL_LOCATION, mandatory = true)
	private String swFileName;
	
	// -------------------------------------------------------------------------------------------------------------------
	//for storing word's Mystem lemmas; 
	private static HashMap<String, String> dictLemmas;

    public static AnalysisEngineDescription createDescription() throws ResourceInitializationException {
        return AnalysisEngineFactory.createEngineDescription(CustomWordAnnotator.class);
    }
    
	@Override
	public void initialize(UimaContext context)
		throws ResourceInitializationException
	{
		super.initialize(context);
		dictLemmas = new HashMap<String, String>();
		FileUtil.readHash(swFileName, dictLemmas);
		getLogger().info("Loaded words' lemmas from [" + swFileName + "]");
	}	

    

    @Override
    public void process(JCas jCas) throws AnalysisEngineProcessException {
        makeSimplyWords(jCas);
    }
    
    public static void makeSimplyWords(JCas jCas) {
    	printSentence(jCas, JCasUtil.select(jCas, Sentence.class));
        makeSimplyWords(jCas, JCasUtil.select(jCas, Word.class));
    }
    
    public static void printSentence(JCas jCas, Iterable<Sentence> sents){
    	for (Sentence srcS : sents) {
    		System.out.println(srcS.getCoveredText() + " " + srcS.getBegin()+ " "+ srcS.getEnd() );
    		if (srcS.getEnd()-srcS.getBegin()<5)
    			jCas.getCas().removeFsFromIndexes(srcS);
    	}
    }

    public static void makeSimplyWords(JCas jCas, Iterable<Word> aWords) {
        for (Word srcWord : aWords) {
            SimplyWord resWord = new SimplyWord(jCas, srcWord.getBegin(), srcWord.getEnd());
            resWord.setToken(srcWord.getToken());
            FSArray wfs = srcWord.getWordforms();
            if (wfs != null && wfs.size() > 0) {
                Wordform wf = (Wordform) wfs.get(0);
                resWord.setPosTag(wf.getPos());
                resWord.setGrammems(wf.getGrammems());
                String key = srcWord.getCoveredText().toLowerCase();
                if (dictLemmas.containsKey(key))
                	resWord.setLemma(dictLemmas.get(key)); //from Mystem
                else 
                	resWord.setLemma(wf.getLemma()); //from Textokit
                resWord.setLemmaId(wf.getLemmaId());
            }
            resWord.addToIndexes();
            //jCas.addFsToIndexes(resWord);
        }
        
        
    }
}
