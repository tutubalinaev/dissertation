package ru.kpfu.itis.cll.utils;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by telena on 11.05.15.
 */
public class Evaluation {

    final static Logger logger = Logger.getLogger(Evaluation.class);
    //pi - pParameters for doc-sentiment distribution M*L;
    public static void runEval(ArrayList<Item> objects){
        Map<String,Integer> positive = new HashMap<String,Integer>();
        positive.put("tp",0);
        positive.put("tn",0);
        positive.put("fn",0);
        positive.put("fp",0);
        Map<String,Integer> negative = new HashMap<String,Integer>();
        negative.put("tp",0);
        negative.put("tn",0);
        negative.put("fn",0);
        negative.put("fp",0);

        for(int m = 0; m < objects.size(); m++) {
            classLabel etalon = objects.get(m).classID;
            classLabel pred = objects.get(m).predictedID;
          
            
            countDiff(etalon,pred,positive,negative);
        }
        for (String key:positive.keySet())
            logger.info("positive."+ key + " " + positive.get(key));
        for (String key:negative.keySet())
            logger.info("negative." + key + " " + negative.get(key));

        computeResults(positive,negative);
    }

    static void countDiff(classLabel etalonValue, classLabel resultValue, Map<String,Integer> positive,Map<String,Integer> negative) {
    	
    	if (resultValue==null)
    		return;
    	
    	if (etalonValue.equals(resultValue)) {
            if (etalonValue.equals(classLabel.problem)) {
                positive.put("tp", positive.get("tp") + 1);
            } else {
                negative.put("tp", negative.get("tp") + 1);
            }
        }

        if (!etalonValue.equals(resultValue)) {
        	
            if (etalonValue.equals(classLabel.noproblem)) {
                if (resultValue.equals(classLabel.problem)) {
                    positive.put("fp", positive.get("fp") + 1);
                }
            }
            if (etalonValue.equals(classLabel.problem)) {
                if (resultValue.equals(classLabel.noproblem)) {
                    negative.put("fp", negative.get("fp") + 1);
                }
            }

            if (etalonValue.equals(classLabel.problem)) {
                positive.put("fn", positive.get("fn") + 1);
            } else if(etalonValue.equals(classLabel.noproblem)) {
                negative.put("fn", negative.get("fn") + 1);
            }

        }

        if (etalonValue.equals(classLabel.noproblem)) {
            if (resultValue.equals(classLabel.noproblem)) {
                positive.put("tn", positive.get("tn") + 1);
            }
        }
        if (etalonValue.equals(classLabel.problem)) {
            if (resultValue.equals(classLabel.problem)) {
                negative.put("tn", negative.get("tn") + 1);
            }
        }
    }

    static void computeResults(Map<String,Integer> positive,Map<String,Integer> negative){

        double acc_pos=(positive.get("tp")+positive.get("tn"))/(double)(positive.get("tp")+positive.get("tn")+positive.get("fp")+positive.get("fn"));
        double acc_neg=(negative.get("tp")+negative.get("tn"))/(double)(negative.get("tp")+negative.get("tn")+negative.get("fp")+negative.get("fn"));
        logger.info("acc_pos:" + acc_pos+"; acc_neg:" + acc_neg);
        logger.info((acc_pos+ acc_neg)/2);

        double ball_acc=0.5*negative.get("tp")/(double)(negative.get("tp")+negative.get("fn"))+0.5*negative.get("tn")/(double)(negative.get("tn")+negative.get("fp"));
        System.out.println("Balanced acc "+ball_acc);


        double pres_pos=positive.get("tp")/(double)(positive.get("tp")+positive.get("fp"));
        double pres_neg=negative.get("tp")/(double)(negative.get("tp")+negative.get("fp"));
        double recall_pos=positive.get("tp")/(double)(positive.get("tp")+positive.get("fn"));
        double recall_neg=negative.get("tp")/(double)(negative.get("tp")+negative.get("fn"));

        double f_pos=2*pres_pos*recall_pos/(pres_pos+recall_pos);
        double f_neg=2*pres_neg*recall_neg/(pres_neg+recall_neg);

        logger.info("R_pos:" + recall_pos + "; P:" + pres_pos);
        logger.info("R_neg:" + recall_neg + "; P:" + pres_neg);
        logger.info("F_pos:" + f_pos + "; F_neg:" + f_neg);
        
        logger.info(getShortN(acc_pos)+"\t"+getShortN(pres_pos)+"\t"+getShortN(recall_pos)+"\t"+getShortN(f_pos));
        

        computeMacroAv(positive,negative);
        computeMicroAv(positive,negative);
    }
    
    static void computeMacroAv(Map<String,Integer> positive,Map<String,Integer> negative){
        double pres_pos=positive.get("tp")/(double)(positive.get("tp")+positive.get("fp"));
        double pres_neg=negative.get("tp")/(double)(negative.get("tp")+negative.get("fp"));
        double recall_pos=positive.get("tp")/(double)(positive.get("tp")+positive.get("fn"));
        double recall_neg=negative.get("tp")/(double)(negative.get("tp")+negative.get("fn"));

        double macro_pres=(pres_pos+ pres_neg)/2;
        double macro_recall=(recall_pos+ recall_neg)/2;
        double f1=2*macro_pres*macro_recall/(macro_pres+macro_recall);

        logger.info("macro_pres:" + macro_pres);
        logger.info("macro_recall:" + macro_recall);;
        logger.info("F1:" + f1);
    }

    static void computeMicroAv(Map<String,Integer> positive,Map<String,Integer> negative){
        double micro_pres=(positive.get("tp")+negative.get("tp"))/(double)(positive.get("tp")+positive.get("fp")+negative.get("tp")+negative.get("fp"));
        double micro_recall=(positive.get("tp")+negative.get("tp"))/(double)(negative.get("tp")+negative.get("fn")+positive.get("tp")+positive.get("fn"));

        double f1=2*micro_pres*micro_recall/(micro_pres+micro_recall);
        logger.info("micro_pres:" + micro_pres);
        logger.info("micro_recall:" + micro_recall);;
        logger.info("F1:" + f1);
    }
    static double getShortN(double x){
        return Math.floor(x * 1000) / 1000;
    }

}
