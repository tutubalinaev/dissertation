package ru.kpfu.itis.cll.annotators;

import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectCovered;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.net.URL;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.CasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.TypeSystem;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

import com.textocat.textokit.morph.commons.SimplyWordAnnotator;
import com.textocat.textokit.morph.fs.SimplyWord;
import com.textocat.textokit.segmentation.fstype.Sentence;

import ru.kpfu.itis.cll.general.OpinionPhrase;
import ru.kpfu.itis.cll.general.OpinionTarget;
import au.com.bytecode.opencsv.CSVReader;

public class SpecificTargetsSelector extends JCasAnnotator_ImplBase {
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PARAM_MODEL_LOCATION = "";
	@ConfigurationParameter(name = PARAM_MODEL_LOCATION, mandatory = true)
	private String swFileName;
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PARAM_MIN_SCORE_VALUE  = "MinSimScoreForDomainSpecificTargets";
	@ConfigurationParameter(name = PARAM_MIN_SCORE_VALUE, mandatory = true, defaultValue="0.0")
	private Double MIN_SCORE;
	// -------------------------------------------------------------------------------------------------------------------
	public static final String PARAM_IS_MIN_SCORE_PRTC  = "ValueIsMinScoreParticularForWord";
	@ConfigurationParameter(name = PARAM_IS_MIN_SCORE_PRTC, mandatory = true, defaultValue="false")
	private boolean IS_MIN_SCORE_PART;
	
	//for storing word's similarity scores; 
	private Map<String, ArrayList<Double>> wordScoresSets;
	//for storing min_score for each word;
	private Map<String, Double> wordMinScoresSets;
	
	@Override
	public void initialize(UimaContext context)
		throws ResourceInitializationException
	{
		super.initialize(context);
		try {
			wordScoresSets = new HashMap<String, ArrayList<Double>>();
			wordMinScoresSets = new HashMap<String, Double>();
			
			URL source = new URL("file://" + swFileName);			
			InputStream is = source.openStream();
			
			File testCorpusFile = new File(swFileName);              
	        CSVReader reader = null;
			try {
				reader = new CSVReader(new InputStreamReader(is,"UTF-8"));			
			} catch (IOException e) {
				e.printStackTrace();
			}
	        String [] nextLine;
	        reader.readNext();
			while ((nextLine = reader.readNext()) != null) {
				String word=nextLine[0];
				ArrayList<Double> scores = new ArrayList<Double>();
				for (int i=1; i<nextLine.length-3;i++)
					scores.add(Double.valueOf(nextLine[i]));					
				Double min_score=0.0;//Double.valueOf(nextLine[nextLine.length-1]);		
				Double min_score2=Double.valueOf(nextLine[nextLine.length-2]);
				Double min_score3=Double.valueOf(nextLine[nextLine.length-3]);	
				min_score = Math.max(min_score, Math.max(min_score2, min_score3));
				wordScoresSets.put(word, scores);
				wordMinScoresSets.put(word, min_score);					
			}
			getLogger().info("Loaded words' weights from [" + swFileName + "]");
		}
		catch (IOException e1) {
			throw new ResourceInitializationException(e1);
		}
	}	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		for (AnnotationFS sentence : select(aJCas, Sentence.class)){
			if (sentence.getCoveredText().length()>3) {
				for (OpinionPhrase phrase:select(aJCas, OpinionPhrase.class)){
					OpinionTarget target=phrase.getOtarget();
					String targetLemma = getLemma(aJCas, target);
					ArrayList<Double> scores = wordScoresSets.get(targetLemma);	
					Double min_score=0.0;
					Double avg=0.0;
					if (scores!=null){
						avg = Collections.max(scores);//calculateAverage(scores);
						min_score=(IS_MIN_SCORE_PART==true)?wordMinScoresSets.get(targetLemma):MIN_SCORE;
						getLogger().info("(word,POS, weight,min_score)=(" + target.getCoveredText()+","+target.getPos()+","+avg+","+min_score+ ")");						
					}
					target.setDomainInd(avg<min_score&&(target.getPos()!=null&&!target.getPos().contains("PRP"))?false:true);
										
					//System.out.println(phrase);
				}
			}
		}
	}
	
	private String getLemma(JCas jCas, OpinionTarget ot){
		for(SimplyWord w : JCasUtil.select(jCas, SimplyWord.class)) { 
			if ((w.getBegin()==ot.getBegin()) && (w.getEnd()==ot.getEnd())){
				System.out.println("Lemma: " + w.getLemma());
				return w.getLemma();
			}
		}
		return ot.getCoveredText();
	}
		
	private double calculateAverage(List <Double> marks) {
		Double sum = 0.0;
		  if(!marks.isEmpty()) {
		    for (Double mark : marks) {
		        sum += mark;
		    }
		    return sum.doubleValue() / marks.size();
		  }
		return sum;
	}
	
	/*
	public void typeSystemInit(TypeSystem typeSystem) throws AnalysisEngineProcessException {
		sentenceType = typeSystem.getType("de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence");
		opinionTargetType = typeSystem.getType("ru.kpfu.itis.cll.general.OpinionTarget");
	}
	 */
}
