package ru.kpfu.itis.cll.utils;
import java.io.*;
import java.util.*;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;

public class FileUtil {

	public static void readLines(String file, ArrayList<String> lines) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(new File(file)));
			String line = null;
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static void readCSVLines(String fileName, ArrayList<Item> lines, int classID, int textID, int stemmedTextID) {
		File testCorpusFile = new File(fileName);              
        CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(testCorpusFile));			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        String [] nextLine;
        try {
        	reader.readNext();
			while ((nextLine = reader.readNext()) != null) {
				Item item = new Item(nextLine[textID], nextLine[stemmedTextID], nextLine[classID]);
				lines.add(item);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void readHash(String file, HashMap<String, String> typeMap) {

		ArrayList<String> lines = new ArrayList<String>();

		readLines(file, lines);
		for (int i = 0; i < lines.size(); i++) {
			String[] tokens = lines.get(i).split("\t");
			typeMap.put(tokens[0], tokens[1]);
		}
	}
	public static void writeLines(String file, ArrayList<?> counts) {
		BufferedWriter writer = null;

		try {

			writer = new BufferedWriter(new FileWriter(new File(file)));

			for (int i = 0; i < counts.size(); i++) {
				writer.write(counts.get(i) + "\n");
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}