Данная папка содержит контрольные выборки в формате csv о продуктах пяти предметных областей:
(i) auto_en_rating.txt, baby_en_rating.txt, baby_en_rating.txt -- высказывания на английском языке c Amazon о машинах, инструментах и детских товарах, соответственно.
(ii) otzovik_cars_stemmed.txt -- высказывания на русском языке о машинах с otzovik.com
(iii) mobile_app_ru_stemmed.txt -- короткие сообщения о финансовых мобильных приложениях с Google Play