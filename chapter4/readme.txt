This code contains an implementation of topic probabilistic models:
1) TPrPhModel -- please refer to the paper [Elena Tutubalina. Target-Based Topic Model for Problem Phrase Extraction // Advances in Information Retrieval. — Springer International Publishing, 2015. — pp. 271—277.]
2) TSPM -- please refer to the papers [Тутубалина Е. В. Совместная вероятностная тематическая модель для идентификации проблемных высказываний, связанных нарушением функциональности продуктов // Труды Института системного программирования РАН. — 2015. — Т. 4, № 27. — С. 100—120.], [Elena Tutubalina. Topic-Sentiment-Problem Model for Extracting Failures from Product Reviews. Accepted to Text, Speech, and Dialogue. — Springer International Publishing, 2016]
-----------------------------------
An input file is an file where each line represents a json object. 
[TRAINING DATASETS]
Example of a review about a mobile application in Russian:
JSON:{"stemmedText": "автор , не NEG_прислушиваться NEG_пользователь ?  или работа над программа заканчивать ?  отвечать хоть коммент , ли виджет ? ", "tags": ["none"], "text": " Автор, вы не прислушиваетесь к пользователю? Или работа над программой закончена? Ответьте хоть в одном из комментов, будет ли виджет? ", "sentscore": "5", "problemscore": "0", "user/location": "none"}
where an object contains the following parameters: 
(i) "text" is an original user's text, 
(ii) "stemmedText" is a stemmed text (by Mystem library), 
(iii) "sentscore" is a user's rating for a product,
(iv) "problemscore": is a class of problem ("0" eq. unknown, unlabeled)
--
Example of a review about Amazon product in English:
JSON:{"review/profileName": "Philip B. Davison", "product/price": "5.56", "review/time": "1281139200", "product/productId": "B000C5G43A", "review/helpfulness": "2/2", "review/summary": "Great item but very tight fit", "review/userId": "A2CORJXW6VMIPZ", "product/title": "Dorman 20771 PEDAL-UP! Brake Pedal Pad", "review/score": "5.0", "user/location": "california", "review/text": "This item was perfect to replace my old worn out pad, only bit is its a very tight fit (no complaints to that as it means it won't move or fall off) but have a screwdriver within reach to help get that final corner on to the brake pedal."}
This json object is an extended object from the Amazon dump with the new tag "user/location" (for USTM-FT(W) model).
The unlabeled datasets for training are available at https://yadi.sk/d/mUUx4mVtmeeaX
-----------------------------------
[TESTING DATASETS]
Example of a review about a mobile application in Russian for testing:
JSON:{"stemmedText": "сделать , код смска сам вставляться .  а так прекрасно работать . ", "tags": ["none"], "text": " Сделали бы еще , что бы код из смски сам вставлялся. А так прекрасно работает. ", "sentscore": "5", "problemscore": "no-problem", "user/location": "none"}
where an object contains the following parameters: 
(i) "text" is an original user's text, 
(ii) "stemmedText" is a stemmed text (by NLTK or Mystem library), 
(iii) "sentscore" is a user's rating for a product,
(iv) "problemscore": is a class of problem, i.e. whether a product contains an actual information of problem situation ("no-problem" or "problem")
--
Example of a review about Amazon product in English (adopted from different work):
JSON:{"text": "In addition, the sound would not come through the speakers.", "problemscore": "problem", "stemmedText": "addit , sound not neg_com speaker . addition , sound not neg_come speakers . ", "user/location": "none"}
-----------------------------------
Links:
NLTK library - www.nltk.org
Mystem library - https://tech.yandex.ru/mystem/
Amazon dump - https://snap.stanford.edu/data/web-Amazon.html
-----------------------------------
Preprocessing steps
During a preprocessing step, we removed stopwords, converted word tokens to lowercase, and applied stemming for all words using NLTK and Mystem for texts in English and Russian, respectively. If a word is preceded by a negation in English, we marked this word with the negation mark ``neg\_".  Words in negated contexts (defined as text spans between a negation word and a punctuation mark) are modified in conjunction with the negation for Russian, because sentences in Russian do not any specific structure to determine a related negation. We reduced words that occur less than three times in the datasets. You can find functions related to these steps in the Python scripts.