package ru.kfu.itis.issst.main;

import ru.kfu.itis.issst.utils.*;
import org.json.*;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by telena on 22.06.15.
 */
//the modeLabel depends on folder's files: if one file contains all json objects or not
enum modeLabel {plural, singular}
public class Documents {
    /**
     * Class for corpus which consists of M documents
     * The document's json format:
     * {"sentscore": "1.0", "problemscore": "problem", tags:["3","4"], "text": "I purchased a different jacket when I first got my motorcycle."}
     */
    ArrayList<Document> docs;
    ArrayList<Document> docsTest;
    Map<String, Integer> termToIndexMap;
    ArrayList<String> indexToTermMap;
    Map<String, Integer> termCountMap;
    double avgLen = 0.0;

    //term-sentiment
    ArrayList<Integer> indexToSentimentMap;
    //term-problem
    ArrayList<Integer> indexToProblemMap;

    List<String> places;
    HashMap<String,String> posdict;

    modeLabel readingMode;

    public Documents(modeLabel readingMode) {
        this.readingMode=readingMode;
        docs = new ArrayList<Document>();
        docsTest = new ArrayList<Document>();
        termToIndexMap = new HashMap<String, Integer>();
        indexToTermMap = new ArrayList<String>();
        termCountMap = new HashMap<String, Integer>();
        indexToSentimentMap = new ArrayList<Integer>();
        indexToProblemMap = new ArrayList<Integer>();
    }
    //each document is one file in the folder; read all files
    public  ArrayList<Document> readMultipleFiles(String docsPath, int nToBreak){
        ArrayList<Document>temp=new ArrayList<Document>();
        for(File docFile : new File(docsPath).listFiles()){
            if (docFile.getAbsolutePath().contains(".txt")) {
                try {
                    ArrayList<String> docLines = new ArrayList<String>();
                    FileUtil.readLines(docFile.getAbsolutePath(), docLines);
                    Document doc = new Document(docLines, termToIndexMap, indexToTermMap, termCountMap, indexToSentimentMap, indexToProblemMap, places, posdict);
                    temp.add(doc);
                    if (temp.size() > nToBreak)
                        break;

                } catch (Exception e){
                    System.out.println(e);
                }
            }
        }
        return temp;
    }
    //read all objects from one file per line
    public  ArrayList<Document> readSingularFile(String docsPath, int nToBreak){
        ArrayList<Document>temp=new ArrayList<Document>();
        for(File docFile : new File(docsPath).listFiles()){
            if (docFile.getAbsolutePath().contains(".txt")) {
                try {
                    ArrayList<String> docLines = new ArrayList<String>();
                    FileUtil.readLines(docFile.getAbsolutePath(), docLines);
                    for (String line:docLines) {
                        ArrayList<String> oneLine= new  ArrayList<String>();
                        oneLine.add(line);
                        Document doc = new Document(oneLine, termToIndexMap, indexToTermMap, termCountMap, indexToSentimentMap, indexToProblemMap, places, posdict);
                        temp.add(doc);
                        if (temp.size() > nToBreak)
                            break;
                    }
                } catch (Exception e){
                    System.out.println(e);
                }
            }
        }
        return temp;
    }
    //read one dataset and then shuffle docs
    public void readAndShuffleDocs(String docsPath, double perc){
        int nToBreak=(docsPath.contains("auto"))?10000:20000;
        System.out.println(docsPath);
        ArrayList<Document>temp=null;
        if (readingMode.equals(modeLabel.plural))
            temp=readMultipleFiles(docsPath,nToBreak);
        else
            temp=readSingularFile(docsPath, nToBreak);

        for (Document doc:temp)
            avgLen += doc.docWords.length;
        avgLen/=temp.size();
        //Collections.shuffle(temp);
        docs.addAll(temp.subList(0, (int) (temp.size() * perc)));
        docsTest.addAll(temp.subList((int)(temp.size()*perc), temp.size()));
    }
    //read training dataset
    public void readDocs(String docsPath) {
        int nToBreak=30000;
        if (readingMode.equals(modeLabel.plural))
            docs=readMultipleFiles(docsPath,nToBreak);
        else
            docs=readSingularFile(docsPath, nToBreak);

        for (Document doc:docs)
            avgLen += doc.docWords.length;

        avgLen/=docs.size();
        //computeStats();
    }
    //read testing dataset
    public void readTestDocs(String docsPath) {
        int nToBreak=10000;
        if (readingMode.equals(modeLabel.plural))
            docsTest=readMultipleFiles(docsPath,nToBreak);
        else
            docsTest=readSingularFile(docsPath, nToBreak);
    }

    public void computeStats(){
        int[] ress=new int[5];
        for (Document doc:docs){
            for (int i=0; i<doc.docTags.length; i++)
                ress[i]+=doc.docTags[i];
        }
        System.out.println(Arrays.toString(ress));
    }

    public static class Document {
        public double rating = 0.0;
        int[] docWords;
        int[] priorSentiLabels;
        int[] priorProblemLabels;
        int[] posTypeLabels;
        int[] docTags;

        public Document(ArrayList<String> docLines, Map<String, Integer> termToIndexMap, ArrayList<String> indexToTermMap, Map<String, Integer> termCountMap,
                        ArrayList<Integer> indexToSentimentMap,  ArrayList<Integer> indexToProblemMap, List<String> places, HashMap<String,String> posdict) {
                 //Read file and initialize word index array
            ArrayList<String> words = new ArrayList<String>();

            String str = new String();
            for (String line : docLines) {
                str += line;
            }
            JSONObject obj = new JSONObject(str);
            //add text and words
            String text = obj.getString(obj.has("stemmedText")?"stemmedText":"text");
            FileUtil.tokenizeAndLowerCase(text, words);
            //add sentiment and text
            String sentimentTag = obj.getString("sentscore");
            String problemTag = obj.getString("problemscore");

            //chanding problem labels to 1 or 5 for easier evaluation
            if (problemTag.length()>3)
                this.rating = (problemTag.equals("problem"))?1.0:5.0;
            else
                this.rating = Double.parseDouble(problemTag);

            JSONArray arr = obj.getJSONArray("tags");

            int T=5;
            docTags = new int[T];
            for (int i = 0; i < arr.length(); i++) {
                double tag = Double.parseDouble(sentimentTag);
                docTags[((int)tag)-1] = 1;
               /*if (tag <= 3.0)
                    docTags[0] = 1;
               else if (tag > 3.0)
                    docTags[1] = 1;*/
            }

            //Remove stop words and noise words
            for (int i = 0; i < words.size(); i++) {
                String word = words.get(i);
                //а-яа-яА-Я
                word=word.replaceAll("[^a-zA-Zа-яА-Я_ \t\n]","");
                word=word.replaceAll("neg_","");
                if (StopwordsEN.isStopword(word) || isNoiseWord(words.get(i)) || word.length() < 3) {
                    words.remove(i);
                    i--;
                }
            }

            //
            ArrayList<String> bigrams = new ArrayList<String>();
            /*for (int i=0; i<words.size()-1;i++)
                bigrams.add(words.get(i)+" "+words.get(i+1));

            for (int i = 0; i < bigrams.size(); i++) {
                String word = bigrams.get(i);
                //а-яа-яА-Я
                word=word.replaceAll("[^a-zA-Zа-яА-Я_ \t\n]","");
                if (StopwordsEN.isStopword(word) || isNoiseWord(words.get(i)) || word.length() < 3 || word.contains("neg")) {
                    bigrams.remove(i);
                    i--;
                }
            }*/
           /*for (int i = 0; i < words.size(); i++) {
                String word = words.get(i);
                if (!word.contains("neg")) {
                    words.remove(i);
                    i--;
                }
            }*/
            //words.removeAll(words);

            //Transfer word to index
            this.docWords = new int[words.size()+bigrams.size()];
            this.priorSentiLabels = new int[words.size()+bigrams.size()];
            this.priorProblemLabels = new int[words.size()+bigrams.size()];
            this.posTypeLabels = new int[words.size()+bigrams.size()];

            for (int i = 0; i < words.size(); i++) {
                String word = words.get(i);
                priorProblemLabels[i] = getProblemLabel(word);
                priorSentiLabels[i] = getSentimentLabel(word);
            }
            for (int i = 0; i < bigrams.size(); i++) {
                String bigram = bigrams.get(i);
                String word1= bigram.split(" ")[0];
                String word2= bigram.split(" ")[1];

                priorProblemLabels[words.size()+i] = getBigramSummary(getProblemLabel(bigram),getBigramSummary(getProblemLabel(word1),getProblemLabel(word2)));
                priorSentiLabels[words.size()+i] = getBigramSummary(getSentimentLabel(bigram), getBigramSummary(getSentimentLabel(word1), getSentimentLabel(word2)));
            }
            words.addAll(bigrams);
            bigrams=null;

            for (int i = 0; i < words.size(); i++) {
                String word = words.get(i);

                if (!termToIndexMap.containsKey(word)) {
                    int newIndex = termToIndexMap.size();
                    termToIndexMap.put(word, newIndex);
                    indexToTermMap.add(word);
                    //termCountMap.put(word, new Integer(1));
                    indexToSentimentMap.add(priorSentiLabels[i]);
                    indexToProblemMap.add(priorProblemLabels[i]);
                    docWords[i] = newIndex;
                } else {
                    docWords[i] = termToIndexMap.get(word);
                    //termCountMap.put(word, termCountMap.get(word) + 1);
                }
            }
            words.clear();
        }

        public boolean isNoiseWord(String string) {
            // TODO Auto-generated method stub
            string = string.toLowerCase().trim();
            Pattern MY_PATTERN = Pattern.compile(".*[a-zA-Zа-яА-Я]+.*");
            Matcher m = MY_PATTERN.matcher(string);
            // filter @xxx and URL
            if (string.matches(".*www\\..*") || string.matches(".*\\.com.*") ||
                    string.matches(".*http:.*"))
                return true;
            if (!m.matches()) {
                return true;
            } else
                return false;
        }



    }

    public ArrayList<Document> getDocsTest() {
        return docsTest;
    }

    public void setDocsTest(ArrayList<Document> docsTest) {
        this.docsTest = docsTest;
    }

    static int getProblemLabel(String word){
        if (ProblemWords.isProblemWord(word))
            return -1;
        else
            return NotProblemWords.isNotProblemWord(word) ? 1 : 0;
    }
    static int getSentimentLabel(String word){
        if (NegativeWords.isNegativeWord(word))
            return -1;
        else
            return PositiveWords.isPositiveWord(word) ? 1 : 0;

    }

    static int getBigramSummary(int label1, int label2){
        int result=label1+label2;
        result=(result==-2)?-1:result;
        result=(result==2)?1:result;
        return result;
    }
}
