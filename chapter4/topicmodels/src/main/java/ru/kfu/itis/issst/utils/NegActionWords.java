package ru.kfu.itis.issst.utils;

import java.io.*;
import java.util.*;

/**
 * Created by TutubalinaEV on 15.07.2015.
 */
public class NegActionWords {
    /**
     * The hash set containing the list of words
     */
    protected HashSet m_Words = null;

    /**
     * The default NegActionWords object
     */
    protected static NegActionWords m_NegActionWords;

    static {
        if (m_NegActionWords == null) {
            m_NegActionWords = new NegActionWords();
        }
    }

    public NegActionWords() {
        m_Words = new HashSet();
        //addActionRUWords();
        ArrayList<String> lines=new ArrayList<String>();
        FileUtil.readLines("data/dictionaries/negaction_ru.txt",lines);
        m_Words.addAll(lines);
    }

    /**
     * removes all words
     */
    public void clear() {
        m_Words.clear();
    }

    /**
     * adds the given word to the list (is automatically converted to
     * lower case and trimmed)
     *
     * @param word the word to add
     */
    public void add(String word) {
        if (word.trim().length() > 0)
            m_Words.add(word.trim().toLowerCase());
    }

    /**
     * removes the word from the list
     *
     * @param word the word to remove
     * @return true if the word was found in the list and then removed
     */
    public boolean remove(String word) {
        return m_Words.remove(word);
    }

    /**
     * Returns a sorted enumeration over all stored words
     *
     * @return the enumeration over all words
     */
    public Enumeration elements() {
        Iterator iter;
        Vector list;

        iter = m_Words.iterator();
        list = new Vector();

        while (iter.hasNext())
            list.add(iter.next());

        // sort list
        Collections.sort(list);

        return list.elements();
    }

    /**
     * Generates a new  object from the given file
     *
     * @param filename the file to read the words from
     * @throws Exception if reading fails
     */
    public void read(String filename) throws Exception {
        read(new File(filename));
    }

    /**
     * Generates a new  object from the given file
     *
     * @param file the file to read the words from
     * @throws Exception if reading fails
     */
    public void read(File file) throws Exception {
        read(new BufferedReader(new FileReader(file)));
    }

    /**
     * Generates a new  object from the reader. The reader is
     * closed automatically.
     *
     * @param reader the reader to get the words from
     * @throws Exception if reading fails
     */
    public void read(BufferedReader reader) throws Exception {
        String line;

        clear();

        while ((line = reader.readLine()) != null) {
            line = line.trim();
            // comment?
            if (line.startsWith("#"))
                continue;
            add(line);
        }

        reader.close();
    }

    /**
     * Writes the current words to the given file
     *
     * @param filename the file to write the words to
     * @throws Exception if writing fails
     */
    public void write(String filename) throws Exception {
        write(new File(filename));
    }

    /**
     * Writes the current words to the given file
     *
     * @param file the file to write the words to
     * @throws Exception if writing fails
     */
    public void write(File file) throws Exception {
        write(new BufferedWriter(new FileWriter(file)));
    }

    /**
     * Writes the current words to the given writer. The writer is closed
     * automatically.
     *
     * @param writer the writer to get the words from
     * @throws Exception if writing fails
     */
    public void write(BufferedWriter writer) throws Exception {
        Enumeration enm;

        // header
        writer.write("# generated " + new Date());
        writer.newLine();

        enm = elements();

        while (enm.hasMoreElements()) {
            writer.write(enm.nextElement().toString());
            writer.newLine();
        }

        writer.flush();
        writer.close();
    }

    /**
     * returns the current words in a string
     *
     * @return the current words
     */
    public String toString() {
        Enumeration enm;
        StringBuffer result;

        result = new StringBuffer();
        enm = elements();
        while (enm.hasMoreElements()) {
            result.append(enm.nextElement().toString());
            if (enm.hasMoreElements())
                result.append(",");
        }

        return result.toString();
    }

    /**
     * Returns true if the given string is a negative word.
     *
     * @param word the word to test
     * @return true if the word is a stopword
     */
    public boolean is(String word) {
        return m_Words.contains(word.toLowerCase());
    }

    /**
     * Returns true if the given string is a negative word.
     *
     * @param str the word to test
     * @return true if the word is a stopword
     */
    public static boolean isNegActionWord(String str) {
        return m_NegActionWords.is(str.toLowerCase());
    }

}

