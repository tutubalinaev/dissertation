package ru.kfu.itis.issst.utils;


import java.io.*;
import java.util.*;


public class StopwordsEN {

    /**
     * The hash set containing the list of stopwords
     */
    protected HashSet m_Words = null;

    /**
     * The default stopwords object (stoplist based on Rainbow)
     */
    protected static StopwordsEN m_StopwordsEN;

    static {
        if (m_StopwordsEN == null) {
            m_StopwordsEN = new StopwordsEN();
        }
    }

    public StopwordsEN() {
        m_Words = new HashSet();
        addStopENWords();
        addStopRUWords();
        ArrayList<String> lines=new ArrayList<String>();

        //FileUtil.readLines("data/low-freqs/ow-freq-comp.txt",lines);
        //FileUtil.readLines("data/low-freqs/low-freq-cars-st.txt",lines);
        //FileUtil.readLines("data/low-freqs/low-freq-baby-st.txt",lines);
        //FileUtil.readLines("data/low-freqs/low-freq-comp-st.txt",lines);
        //FileUtil.readLines("data/low-freqs/low-freq-tools-st.txt",lines);
        //FileUtil.readLines("data/low-freqs/low-freq-baby.txt",lines);

        // RU
        //financial data
        FileUtil.readLines("data/low-freqs/low-freq-fin-bigrams.txt",lines);
        FileUtil.readLines("data/low-freqs/low-freq-fin-ru.txt",lines);

        m_Words.addAll(lines);
    }

    private void addStopRUWords() {
        add("так-как");
        add("как-нибудь");
        add("сие");
        add("а");
        add("е");
        add("и");
        add("ж");
        add("м");
        add("о");
        add("на");
        add("об");
        add("но");
        add("он");
        add("его");
        add("ee");
        add("мне");
        add("мои");
        add("мож");
        add("она");
        add("они");
        add("оно");
        add("мной");
        add("много");
        add("многочисленное");
        add("многочисленная");
        add("многочисленные");
        add("многочисленный");
        add("мною");
        add("мой");
        add("мог");
        add("могут");
        add("можно");
        add("может");
        add("можхо");
        add("мор");
        add("моя");
        add("моё");
        //add("мочь");
        add("над");
        add("нее");
        add("оба");
        add("нам");
        add("нем");
        add("нами");
        add("ними");
        add("мимо");
       // add("немного");
        add("одной");
        add("одного");
        add("менее");
        add("однажды");
        add("однако");
        add("меня");
        add("нему");
        add("меньше");
        add("ней");
        add("наверху");
        add("него");
        add("ниже");
        add("мало");
        add("надо");
        add("один");
        add("одиннадцать");
        add("одиннадцатый");
        add("назад");
        add("наиболее");
        add("недавно");
        add("миллионов");
        add("недалеко");
        add("между");
        add("низко");
        add("меля");
        add("нельзя");
        add("нибудь");
        add("непрерывно");
        add("наконец");
        add("никогда");
        add("никуда");
        add("нас");
        add("наш");
        add("нет");
        add("нею");
        add("неё");
        add("них");
        add("мира");
        add("наша");
        add("наше");
        add("наши");
        add("ничего");
        add("начала");
        add("нередко");
        add("несколько");
        add("обычно");
        add("опять");
        add("около");
        add("мы");
        add("ну");
        add("нх");
        add("от");
        add("отовсюду");
        add("особенно");
        add("нужно");
        add("очень");
        add("отсюда");
        add("в");
        add("во");
        add("вон");
        add("вниз");
        add("внизу");
        add("вокруг");
        add("вот");
        add("восемнадцать");
        add("восемнадцатый");
        add("восемь");
        add("восьмой");
        add("вверх");
        add("вам");
        add("вами");
        add("важное");
        add("важная");
        add("важные");
        add("важный");
        add("вдали");
        add("везде");
        add("ведь");
        add("вас");
        add("ваш");
        add("ваша");
        add("ваше");
        add("ваши");
        add("впрочем");
        add("весь");
        add("вдруг");
        add("вы");
        add("все");
        add("второй");
        add("всем");
        add("всеми");
        add("времени");
        add("время");
        add("всему");
        add("всего");
        add("всегда");
        add("всех");
        add("всею");
        add("всю");
        add("вся");
        add("всё");
        add("всюду");
        add("г");
        add("год");
        add("говорил");
        add("говорит");
        add("года");
        add("году");
        add("где");
        add("да");
        add("ее");
        add("за");
        add("из");
        add("ли");
        add("же");
        add("им");
        add("до");
        add("по");
        add("ими");
        add("под");
        add("иногда");
        add("довольно");
        add("именно");
        add("долго");
        add("позже");
        add("более");
        add("должно");
        add("пожалуйста");
        add("значит");
        add("иметь");
        add("больше");
        add("пока");
        add("ему");
        add("имя");
        add("пор");
        add("пора");
        add("потом");
        add("потому");
        add("после");
        add("почему");
        add("почти");
        add("посреди");
        add("ей");
        add("два");
        add("две");
        add("двенадцать");
        add("двенадцатый");
        add("двадцать");
        add("двадцатый");
        add("двух");
        add("его");
        add("дел");
        add("или");
        add("без");
        add("день");
        add("занят");
        add("занята");
        add("занято");
        add("заняты");
        add("действительно");
        add("давно");
        add("девятнадцать");
        add("девятнадцатый");
        add("девять");
        add("девятый");
        add("даже");
        add("алло");
        add("жизнь");
        add("далеко");
        add("близко");
        add("здесь");
        add("дальше");
        add("для");
        add("лет");
        add("зато");
        add("даром");
        add("первый");
        add("перед");
        add("затем");
        add("зачем");
        add("лишь");
        add("десять");
        add("десятый");
        add("ею");
        add("её");
        add("их");
        add("бы");
        add("еще");
        add("при");
        add("был");
        add("про");
        add("процентов");
        add("против");
        add("просто");
        add("бывает");
        add("бывь");
        add("если");
        add("люди");
        add("была");
        add("были");
        add("было");
        add("будем");
        add("будет");
        add("будете");
        add("будешь");
        add("прекрасно");
        add("буду");
        add("будь");
        add("будто");
        add("будут");
        add("ещё");
        add("пятнадцать");
        add("пятнадцатый");
        add("друго");
        add("другое");
        add("другой");
        add("другие");
        add("другая");
        add("других");
        add("есть");
        add("пять");
        add("быть");
        add("лучше");
        add("пятый");
        add("к");
        add("ком");
        add("конечно");
        add("кому");
        add("кого");
        add("когда");
        add("которой");
        add("которого");
        add("которая");
        add("которые");
        add("который");
        add("которых");
        add("кем");
        add("каждое");
        add("каждая");
        add("каждые");
        add("каждый");
        add("кажется");
        add("как");
        add("какой");
        add("какая");
        add("кто");
        add("кроме");
        add("куда");
        add("кругом");
        add("с");
        add("т");
        add("у");
        add("я");
        add("та");
        add("те");
        add("уж");
        add("со");
        add("то");
        add("том");
        add("снова");
        add("тому");
        add("совсем");
        add("того");
        add("тогда");
        add("тоже");
        add("собой");
        add("тобой");
        add("собою");
        add("тобою");
        add("сначала");
        add("только");
        add("уметь");
        add("тот");
        add("тою");
        add("хорошо");
        add("хотеть");
        add("хочешь");
        add("хоть");
        add("хотя");
        add("свое");
        add("свои");
        add("твой");
        add("своей");
        add("своего");
        add("своих");
        add("свою");
        add("твоя");
        add("твоё");
        add("раз");
        add("уже");
        add("сам");
        add("там");
        add("тем");
        add("чем");
        add("сама");
        add("сами");
        add("теми");
        add("само");
        add("рано");
        add("самом");
        add("самому");
        add("самой");
        add("самого");
        add("семнадцать");
        add("семнадцатый");
        add("самим");
        add("самими");
        add("самих");
        add("саму");
        add("семь");
        add("чему");
        add("раньше");
        add("сейчас");
        add("чего");
        add("сегодня");
        add("себе");
        add("тебе");
        add("сеаой");
        add("человек");
        add("разве");
        add("теперь");
        add("себя");
        add("тебя");
        add("седьмой");
        add("спасибо");
        add("слишком");
        add("так");
        add("такое");
        add("такой");
        add("такие");
        add("также");
        add("такая");
        add("сих");
        add("тех");
        add("чаще");
        add("четвертый");
        add("через");
        add("часто");
        add("шестой");
        add("шестнадцать");
        add("шестнадцатый");
        add("шесть");
        add("четыре");
        add("четырнадцать");
        add("четырнадцатый");
        add("сколько");
        add("сказал");
        add("сказала");
        add("сказать");
        add("ту");
        add("ты");
        add("три");
        add("эта");
        add("эти");
        add("что");
        add("это");
        add("чтоб");
        add("этом");
        add("этому");
        add("этой");
        add("этого");
        add("чтобы");
        add("этот");
        add("стал");
        add("туда");
        add("этим");
        add("этими");
        add("рядом");
        add("тринадцать");
        add("тринадцатый");
        add("этих");
        add("третий");
        add("тут");
        add("эту");
        add("суть");
        add("чуть");
        add("тысяч");
    }

    private void addStopENWords() {
        add("a");
        add("s");
        add("able");
        add("about");
        add("above");
        add("according");
        add("accordingly");
        add("across");
        add("actually");
        add("after");
        add("afterwards");
        add("again");
        add("against");
        add("ain");
        add("t");
        add("all");
        add("allow");
        add("allows");
        add("almost");
        add("alone");
        add("along");
        add("already");
        add("also");
        add("although");
        add("always");
        add("am");
        add("among");
        add("amongst");
        add("an");
        add("and");
        add("another");
        add("any");
        add("anybody");
        add("anyhow");
        add("anyone");
        add("anything");
        add("anyway");
        add("anyways");
        add("anywhere");
        add("apart");
        add("appear");
        add("appreciate");
        add("appropriate");
        add("are");
        add("aren");
        add("t");
        add("around");
        add("as");
        add("aside");
        add("ask");
        add("asking");
        add("associated");
        add("at");
        add("available");
        add("away");
        add("awfully");
        add("be");
        add("became");
        add("because");
        add("become");
        add("becomes");
        add("becoming");
        add("been");
        add("before");
        add("beforehand");
        add("behind");
        add("being");
        add("believe");
        add("below");
        add("beside");
        add("besides");
        add("best");
        add("better");
        add("between");
        add("beyond");
        add("both");
        add("brief");
        add("but");
        add("by");
        add("c");
        add("mon");
        add("c");
        add("s");
        add("came");
        add("can");
        add("can");
        add("t");
        add("cannot");
        add("cant");
        add("cause");
        add("causes");
        add("certain");
        add("certainly");
        add("changes");
        add("clearly");
        add("co");
        add("com");
        add("come");
        add("comes");
        add("concerning");
        add("consequently");
        add("consider");
        add("considering");
        add("contain");
        add("containing");
        add("contains");
        add("corresponding");
        add("could");
        add("couldn");
        add("t");
        add("course");
        add("currently");
        add("definitely");
        add("described");
        add("despite");
        add("did");
        add("didn");
        add("t");
        add("different");
        add("do");
        add("does");
        add("doesn");
        add("t");
        add("doing");
        add("don");
        add("t");
        add("done");
        add("down");
        add("downwards");
        add("during");
        add("each");
        add("edu");
        add("eg");
        add("eight");
        add("either");
        add("else");
        add("elsewhere");
        add("enough");
        add("entirely");
        add("especially");
        add("et");
        add("etc");
        add("even");
        add("ever");
        add("every");
        add("everybody");
        add("everyone");
        add("everything");
        add("everywhere");
        add("ex");
        add("exactly");
        add("example");
        add("except");
        add("far");
        add("few");
        add("fifth");
        add("first");
        add("five");
        add("followed");
        add("following");
        add("follows");
        add("for");
        add("former");
        add("formerly");
        add("forth");
        add("four");
        add("from");
        add("further");
        add("furthermore");
        add("get");
        add("gets");
        add("getting");
        add("given");
        add("gives");
        add("go");
        add("goes");
        add("going");
        add("gone");
        add("got");
        add("gotten");
        add("greetings");
        add("had");
        add("hadn");
        add("t");
        add("happens");
        add("hardly");
        add("has");
        add("hasn");
        add("t");
        add("have");
        add("haven");
        add("t");
        add("having");
        add("he");
        add("he");
        add("s");
        add("hello");
        add("help");
        add("hence");
        add("her");
        add("here");
        add("here");
        add("s");
        add("hereafter");
        add("hereby");
        add("herein");
        add("hereupon");
        add("hers");
        add("herself");
        add("hi");
        add("him");
        add("himself");
        add("his");
        add("hither");
        add("how");
        add("howbeit");
        add("i");
        add("d");
        add("i");
        add("ll");
        add("i");
        add("m");
        add("i");
        add("ve");
        add("ie");
        add("if");
        add("ignored");
        add("immediate");
        add("in");
        add("inasmuch");
        add("inc");
        add("indeed");
        add("indicate");
        add("indicated");
        add("indicates");
        add("inner");
        add("insofar");
        add("instead");
        add("into");
        add("inward");
        add("is");
        add("isn");
        add("t");
        add("it");
        add("it");
        add("d");
        add("it");
        add("ll");
        add("it");
        add("s");
        add("its");
        add("itself");
        add("just");
        add("keep");
        add("keeps");
        add("kept");
        add("know");
        add("knows");
        add("known");
        add("last");
        add("lately");
        add("later");
        add("latter");
        add("latterly");
        add("least");
        add("less");
        add("lest");
        add("let");
        add("let");
        add("s");
        add("like");
        add("liked");
        add("likely");
        add("little");
        add("look");
        add("looking");
        add("looks");
        add("ltd");
        add("mainly");
        add("many");
        add("may");
        add("maybe");
        add("me");
        add("mean");
        add("meanwhile");
        add("merely");
        add("might");
        add("more");
        add("moreover");
        add("most");
        add("mostly");
        add("much");
        add("must");
        add("my");
        add("myself");
        add("name");
        add("namely");
        add("nd");
        add("near");
        add("nearly");
        add("necessary");
        add("need");
        add("needs");
        add("neither");
        add("never");
        add("nevertheless");
        add("new");
        add("next");
        add("nine");
        add("no");
        add("nobody");
        add("non");
        add("none");
        add("noone");
        add("nor");
        add("normally");
        add("not");
        add("nothing");
        add("novel");
        add("now");
        add("nowhere");
        add("obviously");
        add("of");
        add("off");
        add("often");
        add("oh");
        add("ok");
        add("okay");
        add("old");
        add("on");
        add("once");
        add("one");
        add("ones");
        add("only");
        add("onto");
        add("or");
        add("other");
        add("others");
        add("otherwise");
        add("ought");
        add("our");
        add("ours");
        add("ourselves");
        add("out");
        add("outside");
        add("over");
        add("overall");
        add("own");
        add("particular");
        add("particularly");
        add("per");
        add("perhaps");
        add("placed");
        add("please");
        add("plus");
        add("possible");
        add("presumably");
        add("probably");
        add("provides");
        add("que");
        add("quite");
        add("qv");
        add("rather");
        add("rd");
        add("re");
        add("really");
        add("regarding");
        add("regards");
        add("relatively");
        add("respectively");
        add("right");
        add("said");
        add("same");
        add("saw");
        add("say");
        add("saying");
        add("says");
        add("second");
        add("secondly");
        add("see");
        add("seeing");
        add("seem");
        add("seemed");
        add("seeming");
        add("seems");
        add("seen");
        add("self");
        add("selves");
        add("sensible");
        add("sent");
        add("serious");
        add("seriously");
        add("seven");
        add("several");
        add("shall");
        add("she");
        add("should");
        add("shouldn");
        add("t");
        add("since");
        add("six");
        add("so");
        add("some");
        add("somebody");
        add("somehow");
        add("someone");
        add("something");
        add("sometime");
        add("sometimes");
        add("somewhat");
        add("somewhere");
        add("soon");
        add("sorry");
        add("specified");
        add("specify");
        add("specifying");
        add("still");
        add("sub");
        add("such");
        add("sup");
        add("sure");
        add("t");
        add("s");
        add("take");
        add("taken");
        add("tell");
        add("tends");
        add("th");
        add("than");
        add("thank");
        add("thanks");
        add("thanx");
        add("that");
        add("that");
        add("s");
        add("thats");
        add("the");
        add("their");
        add("theirs");
        add("them");
        add("themselves");
        add("then");
        add("thence");
        add("there");
        add("there");
        add("s");
        add("thereafter");
        add("thereby");
        add("therefore");
        add("therein");
        add("theres");
        add("thereupon");
        add("these");
        add("they");
        add("they");
        add("d");
        add("they");
        add("ll");
        add("they");
        add("re");
        add("they");
        add("ve");
        add("think");
        add("third");
        add("this");
        add("thorough");
        add("thoroughly");
        add("those");
        add("though");
        add("three");
        add("through");
        add("throughout");
        add("thru");
        add("thus");
        add("to");
        add("together");
        add("too");
        add("took");
        add("toward");
        add("towards");
        add("tried");
        add("tries");
        add("truly");
        add("try");
        add("trying");
        add("twice");
        add("two");
        add("un");
        add("under");
        add("unfortunately");
        add("unless");
        add("unlikely");
        add("until");
        add("unto");
        add("up");
        add("upon");
        add("us");
        add("use");
        add("used");
        add("useful");
        add("uses");
        add("using");
        add("usually");
        add("value");
        add("various");
        add("very");
        add("via");
        add("viz");
        add("vs");
        add("want");
        add("wants");
        add("was");
        add("wasn");
        add("t");
        add("way");
        add("we");
        add("we");
        add("d");
        add("we");
        add("ll");
        add("we");
        add("re");
        add("we");
        add("ve");
        add("welcome");
        add("well");
        add("went");
        add("were");
        add("weren");
        add("t");
        add("what");
        add("what");
        add("s");
        add("whatever");
        add("when");
        add("whence");
        add("whenever");
        add("where");
        add("where");
        add("s");
        add("whereafter");
        add("whereas");
        add("whereby");
        add("wherein");
        add("whereupon");
        add("wherever");
        add("whether");
        add("which");
        add("while");
        add("whither");
        add("who");
        add("who");
        add("s");
        add("whoever");
        add("whole");
        add("whom");
        add("whose");
        add("why");
        add("will");
        add("willing");
        add("wish");
        add("with");
        add("within");
        add("without");
        add("won");
        add("t");
        add("wonder");
        add("would");
        add("would");
        add("wouldn");
        add("t");
        add("yes");
        add("yet");
        add("you");
        add("you");
        add("d");
        add("you");
        add("ll");
        add("you");
        add("re");
        add("you");
        add("ve");
        add("your");
        add("yours");
        add("yourself");
        add("yourselves");
        add("zero");
    }


    public void stopwords2() {
        m_Words = new HashSet();

        //StopwordsEN list from Rainbow
        add("a");
        add("able");
        add("about");
        add("above");
        add("according");
        add("accordingly");
        add("across");
        add("actually");
        add("after");
        add("afterwards");
        add("again");
//    add("against");
        add("all");
//    add("allow");
//    add("allows");
//    add("almost");
        add("alone");
        add("along");
        add("already");
        add("also");
//    add("although");
        add("always");
        add("am");
        add("among");
        add("amongst");
        add("an");
        add("and");
        add("another");
        add("any");
        add("anybody");
        add("anyhow");
        add("anyone");
        add("anything");
        add("anyway");
        add("anyways");
        add("anywhere");
        add("apart");
        add("appear");
//    add("appreciate");
        add("appropriate");
        add("are");
        add("around");
        add("as");
        add("aside");
        add("ask");
        add("asking");
        add("associated");
        add("at");
        add("available");
        add("away");
//    add("awfully");
        add("b");
        add("be");
        add("became");
        add("because");
        add("become");
        add("becomes");
        add("becoming");
        add("been");
        add("before");
        add("beforehand");
        add("behind");
        add("being");
        add("believe");
        add("below");
        add("beside");
        add("besides");
//    add("best");
//    add("better");
        add("between");
        add("beyond");
        add("both");
        add("but");
        add("brief");
        add("by");
        add("c");
        add("came");
        add("can");
        add("certain");
        add("certainly");
        add("clearly");
        add("co");
        add("com");
        add("come");
        add("comes");
        add("contain");
        add("containing");
        add("contains");
        add("corresponding");
        add("could");
        add("course");
        add("currently");
        add("d");
        add("definitely");
        add("described");
        add("despite");
        add("did");
        add("different");
        add("do");
        add("does");
        add("doing");
        add("done");
        add("down");
        add("downwards");
        add("during");
        add("e");
        add("each");
        add("edu");
        add("eg");
        add("eight");
        add("either");
        add("else");
        add("elsewhere");
        add("enough");
        add("entirely");
        add("especially");
        add("et");
        add("etc");
        add("even");
        add("ever");
        add("every");
        add("everybody");
        add("everyone");
        add("everything");
        add("everywhere");
        add("ex");
        add("exactly");
        add("example");
        add("except");
        add("f");
        add("far");
        add("few");
        add("fifth");
        add("first");
        add("five");
        add("followed");
        add("following");
        add("follows");
        add("for");
        add("former");
        add("formerly");
        add("forth");
        add("four");
        add("from");
        add("further");
        add("furthermore");
        add("g");
        add("get");
        add("gets");
        add("getting");
        add("given");
        add("gives");
        add("go");
        add("goes");
        add("going");
        add("gone");
        add("got");
        add("gotten");
//    add("greetings");
        add("h");
        add("had");
        add("happens");
//    add("hardly");
        add("has");
        add("have");
        add("having");
        add("he");
        add("hello");
        add("help");
        add("hence");
        add("her");
        add("here");
        add("hereafter");
        add("hereby");
        add("herein");
        add("hereupon");
        add("hers");
        add("herself");
        add("hi");
        add("him");
        add("himself");
        add("his");
        add("hither");
//    add("hopefully");
        add("how");
        add("howbeit");
        add("however");
        add("i");
        add("ie");
        add("if");
//    add("ignored");
        add("immediate");
        add("in");
        add("inasmuch");
        add("inc");
        add("indeed");
        add("indicate");
        add("indicated");
        add("indicates");
        add("inner");
        add("insofar");
        add("instead");
        add("into");
        add("inward");
        add("is");
        add("it");
        add("its");
        add("itself");
        add("j");
        add("just");
        add("k");
        add("keep");
        add("keeps");
        add("kept");
//    add("know");
//    add("knows");
//    add("known");
        add("l");
        add("last");
        add("lately");
        add("later");
        add("latter");
        add("latterly");
        add("least");
        add("less");
        add("lest");
        add("let");
        add("like");
        add("liked");
        add("likely");
        add("little");
        add("ll"); //added to avoid words like you'll,I'll etc.
        add("look");
        add("looking");
        add("looks");
        add("ltd");
        add("m");
        add("mainly");
        add("many");
        add("may");
        add("maybe");
        add("me");
//    add("mean");
        add("meanwhile");
//    add("merely");
        add("might");
        add("more");
        add("moreover");
        add("most");
        add("mostly");
        add("much");
        add("must");
        add("my");
        add("myself");
        add("n");
        add("name");
        add("namely");
        add("nd");
        add("near");
        add("nearly");
        add("necessary");
        add("need");
        add("needs");
//    add("neither");
//    add("never");
//    add("nevertheless");
        add("new");
        add("next");
        add("nine");
        add("normally");
//    add("novel");
        add("no");
        add("nobody");
        add("non");
        add("none");
        add("noone");
        add("nor");
        add("normally");
        add("not");
        add("n't");
        add("nothing");
        add("novel");
        add("now");
        add("nowhere");
        add("now");
        add("nowhere");
        add("o");
        add("obviously");
        add("of");
        add("off");
        add("often");
        add("oh");
        add("ok");
        add("okay");
//    add("old");
        add("on");
        add("once");
        add("one");
        add("ones");
        add("only");
        add("onto");
        add("or");
        add("other");
        add("others");
        add("otherwise");
        add("ought");
        add("our");
        add("ours");
        add("ourselves");
        add("out");
        add("outside");
        add("over");
        add("overall");
        add("own");
        add("p");
        add("particular");
        add("particularly");
        add("per");
        add("perhaps");
        add("placed");
        add("please");
        add("plus");
        add("possible");
        add("presumably");
        add("probably");
        add("provides");
        add("q");
        add("que");
        add("quite");
        add("qv");
        add("r");
        add("rather");
        add("rd");
        add("re");
        add("really");
        add("reasonably");
        add("regarding");
        add("regardless");
        add("regards");
        add("relatively");
        add("respectively");
        add("right");
        add("s");
        add("said");
        add("same");
        add("saw");
        add("say");
        add("saying");
        add("says");
        add("second");
        add("secondly");
        add("see");
        add("seeing");
//    add("seem");
//    add("seemed");
//    add("seeming");
//    add("seems");
        add("seen");
        add("self");
        add("selves");
        add("sensible");
        add("sent");
        // add("serious");
        // add("seriously");
        add("seven");
        add("several");
        add("shall");
        add("she");
        add("should");
        add("since");
        add("six");
        add("so");
        add("some");
        add("somebody");
        add("somehow");
        add("someone");
        add("something");
        add("sometime");
        add("sometimes");
        add("somewhat");
        add("somewhere");
        add("soon");
        add("sorry");
        add("specified");
        add("specify");
        add("specifying");
        add("still");
        add("sub");
        add("such");
        add("sup");
        add("sure");
        add("t");
        add("take");
        add("taken");
        add("tell");
        add("tends");
        add("th");
        add("than");
//    add("thank");
//    add("thanks");
//    add("thanx");
        add("that");
        add("thats");
        add("the");
        add("their");
        add("theirs");
        add("them");
        add("themselves");
        add("then");
        add("thence");
        add("there");
        add("thereafter");
        add("thereby");
        add("therefore");
        add("therein");
        add("theres");
        add("thereupon");
        add("these");
        add("they");
        add("think");
        add("third");
        add("this");
        add("thorough");
        add("thoroughly");
        add("those");
        add("though");
        add("three");
        add("through");
        add("throughout");
        add("thru");
        add("thus");
        add("to");
        add("together");
        add("too");
        add("took");
        add("toward");
        add("towards");
        add("tried");
        add("tries");
        add("truly");
        add("try");
        add("trying");
        add("twice");
        add("two");
        add("u");
        add("un");
        add("under");
//    add("unfortunately");
//    add("unless");
//    add("unlikely");
        add("until");
        add("unto");
        add("up");
        add("upon");
        add("us");
        add("use");
        add("used");
//    add("useful");
        add("uses");
        add("using");
        add("usually");
        add("uucp");
        add("v");
        add("value");
        add("various");
        add("ve"); //added to avoid words like I've,you've etc.
        add("very");
        add("via");
        add("viz");
        add("vs");
        add("w");
        add("want");
        add("wants");
        add("was");
//    add("way");
        add("we");
//    add("welcome");
//    add("well");
        add("went");
        add("were");
        add("what");
//    add("whatever");
        add("when");
        add("whence");
        add("whenever");
        add("where");
        add("whereafter");
        add("whereas");
        add("whereby");
        add("wherein");
        add("whereupon");
        add("wherever");
        add("whether");
        add("which");
        add("while");
        add("whither");
        add("who");
        add("whoever");
        add("whole");
        add("whom");
        add("whose");
        add("why");
        add("will");
        add("willing");
        add("wish");
        add("with");
        add("within");
        add("without");
        add("wonder");
        add("would");
        add("would");
        add("x");
        add("y");
//    add("yes");
        add("yet");
        add("you");
        add("your");
        add("yours");
        add("yourself");
        add("yourselves");
        add("z");
        add("zero");
        // add new
        add("i'm");
        add("he's");
        add("she's");
        add("you're");
        add("i'll");
        add("you'll");
        add("she'll");
        add("he'll");
        add("it's");
        add("don't");
        add("can't");
        add("didn't");
        add("i've");
        add("that's");
        add("there's");
        add("isn't");
        add("what's");
        add("rt");
        add("doesn't");
        add("w/");
        add("w/o");
    }

    /**
     * removes all stopwords
     */
    public void clear() {
        m_Words.clear();
    }

    /**
     * adds the given word to the stopword list (is automatically converted to
     * lower case and trimmed)
     *
     * @param word the word to add
     */
    public void add(String word) {
        if (word.trim().length() > 0)
            m_Words.add(word.trim().toLowerCase());
    }

    /**
     * removes the word from the stopword list
     *
     * @param word the word to remove
     * @return true if the word was found in the list and then removed
     */
    public boolean remove(String word) {
        return m_Words.remove(word);
    }

    /**
     * Returns a sorted enumeration over all stored stopwords
     *
     * @return the enumeration over all stopwords
     */
    public Enumeration elements() {
        Iterator iter;
        Vector list;

        iter = m_Words.iterator();
        list = new Vector();

        while (iter.hasNext())
            list.add(iter.next());

        // sort list
        Collections.sort(list);

        return list.elements();
    }

    /**
     * Generates a new StopwordsEN object from the given file
     *
     * @param filename the file to read the stopwords from
     * @throws Exception if reading fails
     */
    public void read(String filename) throws Exception {
        read(new File(filename));
    }

    /**
     * Generates a new StopwordsEN object from the given file
     *
     * @param file the file to read the stopwords from
     * @throws Exception if reading fails
     */
    public void read(File file) throws Exception {
        read(new BufferedReader(new FileReader(file)));
    }

    /**
     * Generates a new StopwordsEN object from the reader. The reader is
     * closed automatically.
     *
     * @param reader the reader to get the stopwords from
     * @throws Exception if reading fails
     */
    public void read(BufferedReader reader) throws Exception {
        String line;

        clear();

        while ((line = reader.readLine()) != null) {
            line = line.trim();
            // comment?
            if (line.startsWith("#"))
                continue;
            add(line);
        }

        reader.close();
    }

    /**
     * Writes the current stopwords to the given file
     *
     * @param filename the file to write the stopwords to
     * @throws Exception if writing fails
     */
    public void write(String filename) throws Exception {
        write(new File(filename));
    }

    /**
     * Writes the current stopwords to the given file
     *
     * @param file the file to write the stopwords to
     * @throws Exception if writing fails
     */
    public void write(File file) throws Exception {
        write(new BufferedWriter(new FileWriter(file)));
    }

    /**
     * Writes the current stopwords to the given writer. The writer is closed
     * automatically.
     *
     * @param writer the writer to get the stopwords from
     * @throws Exception if writing fails
     */
    public void write(BufferedWriter writer) throws Exception {
        Enumeration enm;

        // header
        writer.write("# generated " + new Date());
        writer.newLine();

        enm = elements();

        while (enm.hasMoreElements()) {
            writer.write(enm.nextElement().toString());
            writer.newLine();
        }

        writer.flush();
        writer.close();
    }

    /**
     * returns the current stopwords in a string
     *
     * @return the current stopwords
     */
    public String toString() {
        Enumeration enm;
        StringBuffer result;

        result = new StringBuffer();
        enm = elements();
        while (enm.hasMoreElements()) {
            result.append(enm.nextElement().toString());
            if (enm.hasMoreElements())
                result.append(",");
        }

        return result.toString();
    }

    /**
     * Returns true if the given string is a stop word.
     *
     * @param word the word to test
     * @return true if the word is a stopword
     */
    public boolean is(String word) {
        return m_Words.contains(word.toLowerCase());
    }

    /**
     * Returns true if the given string is a stop word.
     *
     * @param str the word to test
     * @return true if the word is a stopword
     */
    public static boolean isStopword(String str) {
        return m_StopwordsEN.is(str.toLowerCase());
    }

}
