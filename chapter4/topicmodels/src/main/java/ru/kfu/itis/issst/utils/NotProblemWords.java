package ru.kfu.itis.issst.utils;

import java.io.*;
import java.util.*;

/**
 * Created by telena on 21.06.15.
 */
public class NotProblemWords {

    /**
     * The hash set containing the list of words
     */
    protected HashSet m_Words = null;

    /**
     * The default NotProblemWords object
     */
    protected static NotProblemWords m_NotProblemWords;

    static {
        if (m_NotProblemWords == null) {
            m_NotProblemWords = new NotProblemWords();
        }
    }

    public NotProblemWords() {
        m_Words = new HashSet();
        addNotProblemENWords();
        ArrayList<String> lines=new ArrayList<String>();
        FileUtil.readLines("data/dictionaries/negproblemwords_ru.txt",lines);
        m_Words.addAll(lines);
    }

    private void addNotProblemENWords() {
        add("perfect");
        add("neg_difficult");
        add("neg_noise");
        add("neg_crash");
        add("neg_crack");
        add("neg_could have");
        add("neg_malfunction");
        add("neg_trouble");
        add("neg_need");
        add("neg_necessary");
        add("neg_barely enough");
        add("neg_small");
        add("neg_fail");
        add("neg_useless");
        add("neg_flaw");
        add("neg_unresponsive");
        add("neg_infamous");
        add("neg_death");
        add("neg_break");
        add("neg_distract");
        add("neg_strange");
        add("neg_suddenly");
        add("neg_needless");
        add("neg_unwanted");
        add("neg_bad");
        add("neg_disappointed");
        add("neg_garbage");
        add("neg_horrible");
        add("neg_fault");
        add("neg_loss");
        add("neg_stop");
        add("neg_hardly use");
        add("neg_fail");
        add("neg_crash");
        add("neg_overload");
        add("neg_trip");
        add("neg_fix");
        add("neg_mess");
        add("neg_break");
        add("neg_over charge");
        add("neg_disrupt");
        add("neg_die");
        add("neg_drop");
        add("neg_bite");
        add("neg_f**k");
        add("neg_trouble");
        add("neg_foil");
        add("neg_crash");
        add("neg_failure");
        add("neg_issue");
        add("neg_out age");
        add("neg_problem");
        add("neg_trouble");
        add("neg_hang");
        add("neg_hang up");
        add("neg_hang out");
        add("neg_hang off");
        add("neg_screw");
        add("neg_cut");
        add("neg_knock");
        add("neg_act");
        add("neg_reboot");
        add("neg_rebuild");
        add("neg_sharp");
        add("neg_stuck");
        add("neg_remove");
        add("neg_over heat");
        add("neg_popping up");
        add("neg_down forcefully");
        add("neg_goes blank");
        add("neg_wipe");
        add("neg_faster");
        add("neg_smear");
        add("neg_blotchy");
        add("neg_scratch ");
        add("neg_bloat ware");
        add("neg_upset");
        add("neg_tinny");
        add("neg_treble");
        add("neg_to come and go");
        add("neg_slow");
        add("neg_twist");
        add("neg_detract");
        add("neg_gone");
        add("neg_reconfigure");
        add("neg_requiring");
        add("neg_fragile");
        add("neg_misaligned");
        add("neg_fingerprint");
        add("neg_downgrading");
        add("neg_removing");
        add("neg_snap");
        add("neg_tight");
        add("neg_freeze");
        add("neg_staining");
        add("neg_locking");
        add("neg_loose");
        add("neg_fail");
        add("neg_crash");
        add("neg_overload");
        add("neg_trip");
        add("neg_mess");
        add("neg_break");
        add("neg_overcharge");
        add("neg_disrupt");
        add("work");
        add("function");
        add("connect");
        add("run");
        add("stable");
        add("neg_wipe");
        add("neg_faster");
        add("neg_smear");
        add("neg_blotchy");
        add("neg_scratch ");
        add("neg_bloat ware");
        add("neg_upset");
        add("neg_tinny");
        add("neg_treble");
        add("neg_slow");
        add("neg_twist");
        add("neg_detract");
        add("neg_gone");
        add("neg_reconfigure");
        add("neg_requiring");
        add("neg_fragile");
        add("neg_misaligned");
        add("neg_downgrading");
        add("neg_removing");
        add("neg_snap");
        add("neg_tight");
        add("neg_freeze");
        add("neg_staining");
        add("neg_locking");
        add("neg_loose");
        add("neg_freezed");
        add("neg_freezes");
        add("neg_freezing");
        add("neg_removed");
        add("neg_removes");
        add("neg_removing");
        add("neg_Imperfect");
        add("neg_difficult");
        add("neg_noise");
        add("neg_crash");
        add("neg_crashed");
        add("neg_crashes");
        add("neg_crashing");
        add("neg_crack");
        add("neg_could have");
        add("neg_malfunction");
        add("neg_trouble");
        add("neg_necessary");
        add("neg_barely enough");
        add("neg_fail");
        add("neg_useless");
        add("neg_flaw");
        add("neg_unresponsive");
        add("neg_infamous");
        add("neg_death");
        add("neg_break");
        add("neg_breaking");
        add("neg_distract");
        add("neg_strange");
        add("neg_suddenly");
        add("neg_needless");
        add("neg_unwanted");
        add("neg_bad");
        add("neg_disappointed");
        add("neg_garbage");
        add("neg_horrible");
        add("neg_fault");
        add("neg_loss");
        add("neg_stop");
        add("neg_hardly use");
        add("neg_leak");
        add("neg_leaks");
        add("neg_failed");
        add("neg_trying");
        add("neg_negative");
        add("neg_no");
        add("neg_not");
        add("neg_n't");
        add("neg_never");
        add("neg_neither");
        add("neg_nor");
        add("neg_none");
        add("neg_nobody");
        add("neg_nowhere");
        add("neg_nothing");
        add("neg_cannot");
        add("neg_without");
        add("neg_no one");
        add("neg_no way");
        add("neg_in no way");
        add("neg_stop");
        add("neg_refuse");
        add("neg_cease");
        add("neg_solved");
        add("neg_stopped");
        add("neg_breaking");
        add("neg_wish");
        add("neg_tinkering");
        add("neg_headed back");
        add("neg_send back");
        add("neg_return");
        add("neg_gimmick");
        add("neg_stupid");
        add("neg_request assistance");
        add("neg_inadvertently");
        add("neg_exchange");
        add("neg_be ready");
        add("neg_thought");
        add("neg_tech service");
        add("neg_tech support");
        add("neg_check");
        add("neg_fear");
        add("neg_frustration");
        add("neg_would be very helpful");
        add("neg_jumpy");
        add("neg_annoyance");
        add("neg_bother");
        add("neg_afraid");
        add("neg_support only");
        add("neg_attempted");
        add("neg_issue");
        add("neg_issues");
        add("neg_problem");
        add("neg_problems");
        add("neg_error");
        add("neg_errors");
        add("neg_mess");
        add("neg_reallyyyy");
        add("neg_delay");
        add("neg_minor");
        add("neg_for years");
        add("neg_troubleshoot");
        add("neg_step backward ");
        add("neg_backward");
        add("neg_no");
        add("neg_none");
        add("neg_at least");
        add("neg_sometimes");
        add("neg_not last");
        add("neg_something");
        add("neg_nothing to");
        add("neg_still");
        add("neg_too");
        add("neg_to much");
        add("neg_too much");
        add("neg_too many");
        add("neg_after");
        add("neg_rather");
        add("neg_anymore");
        add("neg_wish");
        add("neg_tinkering");
        add("neg_headed back");
        add("neg_send back");
        add("neg_return");
        add("neg_gimmick");
        add("neg_stupid");
        add("neg_request assistance");
        add("neg_inadvertently");
        add("neg_exchange");
        add("neg_be ready");
        add("neg_thought");
        add("neg_tech service");
        add("neg_tech support");
        add("neg_check");
        add("neg_fear");
        add("neg_frustration");
        add("neg_would be very helpful");
        add("neg_jumpy");
        add("neg_annoyance");
        add("neg_bother");
        add("neg_afraid");
        add("neg_support only");
        add("neg_attempted");
        add("neg_refuse connect");
        add("neg_refuse get");
        add("neg_refuse perform");
        add("neg_refuse receive");
        add("neg_refuse send");
        add("neg_refuse run");
        add("neg_refuse stable");
        add("neg_refuse respond");
        add("neg_cease work");
        add("neg_cease function");
        add("neg_cease connect");
        add("neg_cease get");
        add("neg_cease perform");
        add("neg_cease receive");
        add("neg_cease send");
        add("neg_cease run");
        add("neg_cease stable");
        add("neg_cease respond");
        add("neg_n't work");
        add("neg_n't function");
        add("neg_n't connect");
        add("neg_n't get");
        add("neg_n't perform");
        add("neg_n't receive");
        add("neg_n't send");
        add("neg_n't run");
        add("neg_n't stable");
        add("neg_n't respond");
        add("neg_die");
        add("neg_drop");
        add("neg_bite");
        add("neg_f**k");
        add("neg_trouble");
        add("neg_foil");
        add("neg_failure");
        add("neg_issue");
        add("neg_outage");
        add("neg_problem");
        add("neg_f**k up");
        add("neg_hang up");
        add("neg_screw up");
        add("neg_cut up");
        add("neg_knock up");
        add("neg_act up");
        add("neg_f**k off");
        add("neg_hang off");
        add("neg_screw off");
        add("neg_cut off");
        add("neg_knock off");
        add("neg_act off");
        add("neg_f**k out");
        add("neg_hang out");
        add("neg_screw out");
        add("neg_cut out");
        add("neg_knock out");
        add("neg_act out");

        addStemmedWords();
    }

    private void addStemmedWords() {
        add("perfect");
        add("neg_difficult");
        add("neg_nois");
        add("neg_work");
        add("neg_crash");
        add("neg_crack");
        add("neg_could hav");
        add("neg_malfunct");
        add("neg_troubl");
        add("neg_need");
        add("neg_necessari");
        add("neg_barely enough");
        add("neg_small");
        add("neg_fail");
        add("neg_useless");
        add("neg_flaw");
        add("neg_unrespons");
        add("neg_infam");
        add("neg_death");
        add("neg_break");
        add("neg_distract");
        add("neg_strang");
        add("neg_suddenli");
        add("neg_needless");
        add("neg_unwant");
        add("neg_bad");
        add("neg_disappoint");
        add("neg_garbag");
        add("neg_horribl");
        add("neg_fault");
        add("neg_loss");
        add("neg_stop");
        add("neg_hardly us");
        add("neg_fail");
        add("neg_crash");
        add("neg_overload");
        add("neg_trip");
        add("neg_fix");
        add("neg_mess");
        add("neg_break");
        add("neg_over charg");
        add("neg_disrupt");
        add("neg_work");
        add("neg_function");
        add("neg_connect");
        add("neg_get");
        add("neg_perform");
        add("neg_receiv");
        add("neg_send");
        add("neg_run");
        add("neg_stabl");
        add("neg_respond");
        add("neg_die");
        add("neg_drop");
        add("neg_bite");
        add("neg_f**k");
        add("neg_troubl");
        add("neg_foil");
        add("neg_crash");
        add("neg_failur");
        add("neg_issu");
        add("neg_out ag");
        add("neg_problem");
        add("neg_troubl");
        add("neg_hang");
        add("neg_hang up");
        add("neg_hang out");
        add("neg_hang off");
        add("neg_screw");
        add("neg_screw up");
        add("neg_screw off");
        add("neg_screw  out");
        add("neg_cut");
        add("neg_knock");
        add("neg_act");
        add("neg_reboot");
        add("neg_rebuild");
        add("neg_sharp");
        add("neg_stuck");
        add("neg_remov");
        add("neg_over heat");
        add("neg_popping up");
        add("neg_down forc");
        add("neg_goes blank");
        add("neg_wipe");
        add("neg_faster");
        add("neg_smear");
        add("neg_blotchi");
        add("neg_scratch");
        add("neg_bloat war");
        add("neg_upset");
        add("neg_tinni");
        add("neg_trebl");
        add("neg_to come and go");
        add("neg_slow");
        add("neg_twist");
        add("neg_detract");
        add("neg_gone");
        add("neg_reconfigur");
        add("neg_requir");
        add("neg_fragil");
        add("neg_misalign");
        add("neg_fingerprint");
        add("neg_downgrad");
        add("neg_remov");
        add("neg_snap");
        add("neg_tight");
        add("neg_freez");
        add("neg_stain");
        add("neg_lock");
        add("neg_loos");
        add("neg_fail");
        add("neg_crash");
        add("neg_overload");
        add("neg_trip");
        add("neg_fix");
        add("neg_mess");
        add("neg_break");
        add("neg_overcharg");
        add("neg_disrupt");
        add("work");
        add("funct");
        add("connect");
        add("get");
        add("erform");
        add("rec");
        add("send");
        add("run");
        add("stabl");
        add("neg_refuse work");
        add("neg_refuse funct");
        add("neg_rebuild");
        add("neg_sharp");
        add("neg_stuck");
        add("neg_remov");
        add("neg_over heat");
        add("neg_popping up");
        add("neg_down forc");
        add("neg_goes blank");
        add("neg_wipe");
        add("neg_faster");
        add("neg_smear");
        add("neg_blotchi");
        add("neg_scratch");
        add("neg_bloat war");
        add("neg_upset");
        add("neg_tinni");
        add("neg_trebl");
        add("neg_to come and go");
        add("neg_slow");
        add("neg_twist");
        add("neg_detract");
        add("neg_gone");
        add("neg_reconfigur");
        add("neg_requir");
        add("neg_fragil");
        add("neg_misalign");
        add("neg_downgrad");
        add("neg_remov");
        add("neg_snap");
        add("neg_tight");
        add("neg_freez");
        add("neg_stain");
        add("neg_lock");
        add("neg_loos");
        add("neg_freez");
        add("neg_freez");
        add("neg_freez");
        add("neg_remov");
        add("neg_remov");
        add("neg_remov");
        add("neg_Imperfect");
        add("neg_difficult");
        add("neg_nois");
        add("neg_crash");
        add("neg_crash");
        add("neg_crash");
        add("neg_crash");
        add("neg_crack");
        add("neg_could hav");
        add("neg_malfunct");
        add("neg_troubl");
        add("neg_necessari");
        add("neg_barely enough");
        add("neg_fail");
        add("neg_useless");
        add("neg_flaw");
        add("neg_unrespons");
        add("neg_infam");
        add("neg_death");
        add("neg_break");
        add("neg_break");
        add("neg_distract");
        add("neg_strang");
        add("neg_suddenli");
        add("neg_needless");
        add("neg_unwant");
        add("neg_bad");
        add("neg_disappoint");
        add("neg_garbag");
        add("neg_horribl");
        add("neg_fault");
        add("neg_loss");
        add("neg_stop");
        add("neg_hardly us");
        add("neg_leak");
        add("neg_leak");
        add("neg_fail");
        add("neg_tri");
        add("neg_neg");
        add("neg_no");
        add("neg_not");
        add("neg_n't");
        add("neg_never");
        add("neg_neither");
        add("neg_nor");
        add("neg_none");
        add("neg_nobodi");
        add("neg_nowher");
        add("neg_noth");
        add("neg_cannot");
        add("neg_without");
        add("neg_no on");
        add("neg_no way");
        add("neg_in no way");
        add("neg_stop");
        add("neg_refus");
        add("neg_ceas");
        add("neg_solv");
        add("neg_stop");
        add("neg_break");
        add("neg_wish");
        add("neg_tinker");
        add("neg_headed back");
        add("neg_send back");
        add("neg_return");
        add("neg_gimmick");
        add("neg_stupid");
        add("neg_request assist");
        add("neg_inadvert");
        add("neg_exchang");
        add("neg_be readi");
        add("neg_thought");
        add("neg_tech servic");
        add("neg_tech support");
        add("neg_check");
        add("neg_fear");
        add("neg_frustrat");
        add("neg_would be very help");
        add("neg_jumpi");
        add("neg_annoy");
        add("neg_bother");
        add("neg_afraid");
        add("neg_support onli");
        add("neg_attempt");
        add("neg_issu");
        add("neg_issu");
        add("neg_problem");
        add("neg_problem");
        add("neg_error");
        add("neg_error");
        add("neg_mess");
        add("neg_reallyyyy");
        add("neg_delay");
        add("neg_minor");
        add("neg_for year");
        add("neg_troubleshoot");
        add("neg_step backward");
        add("neg_backward");
        add("neg_no");
        add("neg_none");
        add("neg_at least");
        add("neg_sometim");
        add("neg_not last");
        add("neg_someth");
        add("neg_nothing to");
        add("neg_still");
        add("neg_too");
        add("neg_to much");
        add("neg_too much");
        add("neg_too mani");
        add("neg_after");
        add("neg_rather");
        add("neg_anymor");
        add("neg_wish");
        add("neg_tinker");
        add("neg_headed back");
        add("neg_send back");
        add("neg_return");
        add("neg_gimmick");
        add("neg_stupid");
        add("neg_request assist");
        add("neg_inadvert");
        add("neg_exchang");
        add("neg_be readi");
        add("neg_thought");
        add("neg_tech servic");
        add("neg_tech support");
        add("neg_check");
        add("neg_fear");
        add("neg_frustrat");
        add("neg_would be very help");
        add("neg_jumpi");
        add("neg_annoy");
        add("neg_bother");
        add("neg_afraid");
        add("neg_support onli");
        add("neg_attempt");
        add("neg_refuse connect");
        add("neg_refuse get");
        add("neg_refuse perform");
        add("neg_refuse rec");
        add("neg_refuse send");
        add("neg_refuse run");
        add("neg_refuse st");
        add("neg_refuse respond");
        add("neg_cease work");
        add("neg_cease funct");
        add("neg_cease connect");
        add("neg_cease get");
        add("neg_cease perform");
        add("neg_cease rec");
        add("neg_cease send");
        add("neg_cease run");
        add("neg_cease st");
        add("neg_cease respond");
        add("neg_neg_work");
        add("neg_neg_funct");
        add("neg_neg_connect");
        add("neg_neg_get");
        add("neg_neg_perform");
        add("neg_neg_rec");
        add("neg_neg_send");
        add("neg_neg_run");
        add("neg_neg_stabl");
        add("neg_neg_respond");
        add("neg_die");
        add("neg_drop");
        add("neg_bite");
        add("neg_f**k");
        add("neg_fuck");
        add("neg_troubl");
        add("neg_foil");
        add("neg_failur");
        add("neg_issu");
        add("neg_outag");
        add("neg_problem");
        add("neg_f**k up");
        add("neg_fuck up");
        add("neg_hang up");
        add("neg_screw up");
        add("neg_cut up");
        add("neg_knock up");
        add("neg_act up");
        add("neg_f**k off");
        add("neg_hang off");
        add("neg_screw off");
        add("neg_cut off");
        add("neg_knock off");
        add("neg_act off");
        add("neg_f**k out");
        add("neg_hang out");
        add("neg_screw out");
        add("neg_cut out");
        add("neg_knock out");
        add("neg_act out");
    }

    /**
     * removes all words
     */
    public void clear() {
        m_Words.clear();
    }

    /**
     * adds the given word to the list (is automatically converted to
     * lower case and trimmed)
     *
     * @param word the word to add
     */
    public void add(String word) {
        if (word.trim().length() > 0)
            m_Words.add(word.trim().toLowerCase());
    }

    /**
     * removes the word from the list
     *
     * @param word the word to remove
     * @return true if the word was found in the list and then removed
     */
    public boolean remove(String word) {
        return m_Words.remove(word);
    }

    /**
     * Returns a sorted enumeration over all stored words
     *
     * @return the enumeration over all words
     */
    public Enumeration elements() {
        Iterator iter;
        Vector list;

        iter = m_Words.iterator();
        list = new Vector();

        while (iter.hasNext())
            list.add(iter.next());

        // sort list
        Collections.sort(list);

        return list.elements();
    }

    /**
     * Generates a new  object from the given file
     *
     * @param filename the file to read the words from
     * @throws Exception if reading fails
     */
    public void read(String filename) throws Exception {
        read(new File(filename));
    }

    /**
     * Generates a new  object from the given file
     *
     * @param file the file to read the words from
     * @throws Exception if reading fails
     */
    public void read(File file) throws Exception {
        read(new BufferedReader(new FileReader(file)));
    }

    /**
     * Generates a new  object from the reader. The reader is
     * closed automatically.
     *
     * @param reader the reader to get the words from
     * @throws Exception if reading fails
     */
    public void read(BufferedReader reader) throws Exception {
        String line;

        clear();

        while ((line = reader.readLine()) != null) {
            line = line.trim();
            // comment?
            if (line.startsWith("#"))
                continue;
            add(line);
        }

        reader.close();
    }

    /**
     * Writes the current words to the given file
     *
     * @param filename the file to write the words to
     * @throws Exception if writing fails
     */
    public void write(String filename) throws Exception {
        write(new File(filename));
    }

    /**
     * Writes the current words to the given file
     *
     * @param file the file to write the words to
     * @throws Exception if writing fails
     */
    public void write(File file) throws Exception {
        write(new BufferedWriter(new FileWriter(file)));
    }

    /**
     * Writes the current words to the given writer. The writer is closed
     * automatically.
     *
     * @param writer the writer to get the words from
     * @throws Exception if writing fails
     */
    public void write(BufferedWriter writer) throws Exception {
        Enumeration enm;

        // header
        writer.write("# generated " + new Date());
        writer.newLine();

        enm = elements();

        while (enm.hasMoreElements()) {
            writer.write(enm.nextElement().toString());
            writer.newLine();
        }

        writer.flush();
        writer.close();
    }

    /**
     * returns the current words in a string
     *
     * @return the current words
     */
    public String toString() {
        Enumeration enm;
        StringBuffer result;

        result = new StringBuffer();
        enm = elements();
        while (enm.hasMoreElements()) {
            result.append(enm.nextElement().toString());
            if (enm.hasMoreElements())
                result.append(",");
        }

        return result.toString();
    }

    /**
     * Returns true if the given string is a stop word.
     *
     * @param word the word to test
     * @return true if the word is a stopword
     */
    public boolean is(String word) {
        return m_Words.contains(word.toLowerCase());
    }

    /**
     * Returns true if the given string is a stop word.
     *
     * @param str the word to test
     * @return true if the word is a stopword
     */
    public static boolean isNotProblemWord(String str) {
        return m_NotProblemWords.is(str.toLowerCase());
    }

}


