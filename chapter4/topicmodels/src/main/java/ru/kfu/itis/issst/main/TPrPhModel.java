package ru.kfu.itis.issst.main;

import ru.kfu.itis.issst.conf.PathConfig;
import ru.kfu.itis.issst.utils.Evaluation;
import ru.kfu.itis.issst.utils.NegativeWords;
import ru.kfu.itis.issst.utils.PositiveWords;
import ru.kfu.itis.issst.utils.ProblemWords;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Double.isNaN;

/**
 * Created by TutubalinaEV on 19.08.2015.
 */
public class TPrPhModel extends TPModel {

    /*joint topics for two types of words*/
    int[][] ndk;
    int[] nd;
    /*for words without problem labels*/
    int[][] nkw_1;
    int[] nk_1;
    int[][] z_1;

    /*for words with problem labels*/
    int[][][] ndkr_2;
    int[][] ndk_2;
    int[][][] nkrw_2;
    int[][] nkr_2;

    double[][] theta;//Parameters for doc-topic distribution M*K
    double[][][] pi;//Parameters for doc-topic-problem distribution M*K*R
    double[][] phi_1;//Parameters for topic-word distribution K*V
    double[][][] phi_2;//Parameters for topic--problem-word distribution K*R*V


    double[][] beta_rw;//beta prior (R x V)
    double[] betaSum_r; //beta prior  R

    int[][] postype;

    public TPrPhModel(LdaGibbsSampling.modelparameters modelparam) {
        super(modelparam);
    }

    public void initializeModel(Documents docSet) {
        eta *= docSet.avgLen / R;
        alpha = 50 / (float) K;
        logger.info("New parameters: (eta, alpha)=(" + eta + "," + alpha + ")");

        super.initializeModel(docSet);
        // TODO Auto-generated method stub

        /*for joint topics*/
        ndk = new int[M][K];
        nd = new int[M];

        /*for words without problem labels*/
        nkw_1 = new int[K][V];
        nk_1 = new int[K];

        /*for words with problem labels*/
        ndkr_2 = new int[M][K][R];
        ndk_2 = new int[M][K];
        nkrw_2 = new int[K][R][V];
        nkr_2 = new int[K][V];

        theta = new double[M][K];//Parameters for doc-topic distribution M*K
        pi = new double[M][K][R];//Parameters for doc-topic-problem distribution M*K*R
        phi_1 = new double[K][V];//Parameters for topic-word distribution K*V
        phi_2 = new double[K][R][V];//Parameters for topic--problem-word distribution K*R*V

        //beta parameters
        beta_rw = new double[R][V];//beta prior (R x V)
        betaSum_r = new double[R]; //beta prior R

        postype = new int[M][];
        for (int m = 0; m < M; m++) {
            int N = docSet.docs.get(m).docWords.length;
            postype[m] = new int[N];
            for (int n = 0; n < N; n++) {
                postype[m][n] = docSet.docs.get(m).posTypeLabels[n];
            }
        }

        prior2beta(docSet);
        initEstimate(docSet);
    }

    void initEstimate(Documents docSet) {
        //initialize topic label z for each word
        z_1 = new int[M][];
        z = new int[M][];
        r = new int[M][];

        for (int m = 0; m < M; m++) { //for document
            int N = docSet.docs.get(m).docWords.length;
            z_1[m] = new int[N];
            z[m] = new int[N];
            r[m] = new int[N];
            for (int n = 0; n < N; n++) { //for each word

                int problemLab = 0;
                if (problm[m][n] != 0) {
                    problemLab = (problm[m][n] > 0) ? R - 2 : R - 1; // incorporate prior information into the model
                } else {
                    problemLab = (int) (Math.random() * R);
                }

                int initTopic = (int) (Math.random() * K);// From 0 to K - 1
                int initRLabel = problemLab;// From 0 to R - 1

                if (postype[m][n] == 1) { //without problem labels
                    z_1[m][n] = initTopic;
                    nkw_1[initTopic][doc[m][n]]++;
                    nk_1[initTopic]++;
                } else { //with problem labels
                    z[m][n] = initTopic;
                    r[m][n] = initRLabel;

                    ndkr_2[m][initTopic][initRLabel]++;
                    ndk_2[m][initTopic]++;
                    nkrw_2[initTopic][initRLabel][doc[m][n]]++;
                    nkr_2[initTopic][initRLabel]++;
                }

                ndk[m][initTopic]++;
                nd[m]++;
            }
        }
    }

    public void prior2beta(Documents docSet) {
        //neural, no-problem, problem
        for (int t = 0; t < V; t++) {
            double[] lambda_R = getProblemPriorsByWord(docSet.indexToProblemMap.get(t));
            for (int r = 0; r < R; r++)
                beta_rw[r][t] = lambda_R[r];
        }
        for (int r = 0; r < R; r++) {
            betaSum_r[r] = 0.0;
            for (int t = 0; t < V; t++) {
                betaSum_r[r] += beta_rw[r][t];
            }
        }
    }

    //K*V
    void computePhi_1() {
        for (int z = 0; z < K; z++) {
            for (int w = 0; w < V; w++) {
                phi_1[z][w] = (nkw_1[z][w]+beta) / (nk_1[z]+beta*V);
            }
        }
    }

    //K*R*V
    void computePhi_2() {
        for (int z = 0; z < K; z++) {
            for (int r = 0; r < R; r++) {
                for (int w = 0; w < V; w++) {
                    phi_2[z][r][w] = (nkrw_2[z][r][w]+beta_rw[r][w]) / (nkr_2[z][r]+betaSum_r[r]);
                }
            }
        }
    }

    //theta = new double[M][K];
    void computeTheta() {
        for (int m = 0; m < M; m++) {
            for (int z = 0; z < K; z++) {
                theta[m][z] = (ndk[m][z]+alpha) / (nd[m]+K*alpha);
            }
        }
    }

    //pi = new double[M][K][R];
    void computePi() {
        for (int m = 0; m < M; m++) {
            for (int z = 0; z < K; z++) {
                for (int r = 0; r < R; r++) {
                    pi[m][z][r] = (ndkr_2[m][z][r] + eta) / (ndk_2[m][z]+R*eta);
                }
            }
        }
    }

    public void inferenceModel(Documents docSet) throws IOException {
        // TODO Auto-generated method stub
        if (iterations < saveStep + beginSaveIters) {
            System.err.println("Error: the number of iterations should be larger than " + (saveStep + beginSaveIters));
            System.exit(0);
        }
        for (int i = 0; i < iterations; i++) {
            //System.out.println("Iteration " + i);
            if ((i >= beginSaveIters) && (((i - beginSaveIters) % saveStep) == 0)) {
                //Saving the model
                System.out.println("Saving model at iteration " + i + " ... ");
                //updateDistributions();
                //saveIteratedModel(i, docSet);
            }
            if (updateParaStep > 0 && i % updateParaStep == 0) {
                //updateParameters();
            }
            //Use Gibbs Sampling to update z[][]
            for (int m = 0; m < M; m++) {
                int N = docSet.docs.get(m).docWords.length;
                for (int n = 0; n < N; n++) {
                    // Sample from p(z_i|z_-i, w)
                    if (postype[m][n]==1) { //without problems
                        int[] res = sampleTopicZ(m, n);
                        z_1[m][n] = res[0];
                    } else {
                        int[] res = sampleTopicZandProblemLabels(m, n);
                        z[m][n] = res[0];
                        r[m][n] = res[1];
                    }
                }
            }
        }
        updateDistributions();
        System.out.println("Perplexity");
    }

    void updateDistributions(){
        computeTheta();
        computePhi_1();
        computePhi_2();
        computePi();
    }

    private int[] sampleTopicZ(int m, int n) {
        int oldTopic = z_1[m][n];

        ndk[m][oldTopic]--;
        nd[m]--;
            /*for words without problem labels*/
        nkw_1[oldTopic][doc[m][n]]--;
        nk_1[oldTopic]--;

        double[] p = new double[K];
        for (int z = 0; z < K; z++) {
            p[z] = ((ndk[m][z] + alpha)/(nd[z]+K*alpha))*
                    (nkw_1[z][doc[m][n]]+beta) / (nk_1[z]+beta*V);
        }

        int newTopic = getProbNorm(p, K);
        ndk[m][newTopic]++;
        nd[m]++;
        nkw_1[newTopic][doc[m][n]]++;
        nk_1[newTopic]++;

        int[] res = {newTopic};
        return res;
    }

    private int[] sampleTopicZandProblemLabels(int m, int n) {
        int oldTopic = z[m][n];
        int oldRLabel= r[m][n];

        ndk[m][oldTopic]--;
        nd[m]--;
        ndkr_2[m][oldTopic][oldRLabel]--;
        ndk_2[m][oldTopic]--;
        nkrw_2[oldTopic][oldRLabel][doc[m][n]]--;
        nkr_2[oldTopic][oldRLabel]--;

        double[][] p = new double[K][R];
        for (int z = 0; z < K; z++) {
            for (int r = 0; r < R; r++) {
                p[z][r] = ((ndk[m][z] + alpha) / (nd[z] + K * alpha)) *
                        ((ndkr_2[m][z][r] + eta) / (ndk_2[m][z]+R*eta))*
                        ((nkrw_2[z][r][doc[m][n]]+beta_rw[r][doc[m][n]]) / (nkr_2[z][r]+betaSum_r[r]));
            }
        }
        int[] labels = getProbNorm(p, K, R);
        int newTopic = labels[0];
        int newRLabel = labels[1];

        ndk[m][newTopic]++;
        nd[m]++;
        ndkr_2[m][newTopic][newRLabel]++;
        ndk_2[m][newTopic]++;
        nkrw_2[newTopic][newRLabel][doc[m][n]]++;
        nkr_2[newTopic][newRLabel]++;

        int[] res = {newTopic,newRLabel};
        return res;
    }

    //EVALUATION
    public double[][] runSentimentPrediction(Documents docSet) {
        double[][] pi_q = new double[Mtest][R];
        for (int m = 0; m < Mtest; m++) {
            Documents.Document d=docSet.docsTest.get(m);
            int N = d.docWords.length;
            for (int r = 0; r < R; r++) {
                pi_q[m][r] = 1;
                double value = 0;
                for (int n = 0; n < N; n++) { //for each word
                    int postype=d.posTypeLabels[n];
                    if (postype!=1)
                        for (int z = 0; z < K; z++) {
                            for (int l = 0; l < R; l++) {
                                value += ((nkrw_2[z][r][doctest[m][n]] + beta_rw[r][doctest[m][n]]) / (nkr_2[z][r] + betaSum_r[r]));
                            }
                        }
                    if (value != 0)
                        pi_q[m][r] *= value;
                }
            }
        }
        Evaluation.runEval(Mtest, pi_q, docSet, R - 2);
        return pi_q;
    }

    //ESTIMATE
    public void estimateModel(Documents docSet) throws IOException {
        int[][] ztest_1 = new int[Mtest][];
        int[][] ztest = new int[Mtest][];
        int[][] rtest = new int[Mtest][];

        int[][] ndk_test = new int [Mtest][K];
        int[] nd_test = new int [Mtest];

       /*for words without problem labels*/
        int[][] nkw_1_test = new int[K][V];
        int[] nk_1_test = new int[K];

        /*for words with problem labels*/
        int[][][] ndkr_2_test = new int[Mtest][K][R];
        int[][] ndk_2_test = new int[Mtest][K];
        int[][][] nkrw_2_test = new int[K][R][V];
        int[][] nkr_2_test = new int[K][V];

        int[][] problm_test = new int[Mtest][];
        int[][] postype_test = new int[M][];
        for (int m = 0; m < Mtest; m++) {
            //Notice the limit of memory
            int N = docSet.docsTest.get(m).docWords.length;
            problm_test[m] = new int[N];
            postype_test[m] = new int[N];
            for (int n = 0; n < N; n++) {
                problm_test[m][n] = docSet.docsTest.get(m).priorProblemLabels[n];
                postype_test[m][n] = docSet.docsTest.get(m).posTypeLabels[n];
            }
        }

        for(int m = 0; m < Mtest; m++) {
            int N = docSet.docsTest.get(m).docWords.length;
            ztest[m] = new int[N];
            rtest[m] = new int[N];
            ztest_1[m] = new int[N];
            for (int n = 0; n < N; n++) {
                int problemLab = 0;
                if (problm_test[m][n] != 0) {
                    problemLab = (problm_test[m][n] > 0) ? R - 2 : R - 1; // incorporate prior information into the model
                } else {
                    problemLab = (int) (Math.random() * R);
                }

                int initTopic = (int) (Math.random() * K);// From 0 to K - 1
                int initRLabel = problemLab;// From 0 to R - 1

                if (postype_test[m][n] == 1) { //without problem labels
                    ztest_1[m][n] = initTopic;
                    nkw_1_test[initTopic][doctest[m][n]]++;
                    nk_1_test[initTopic]++;
                } else { //with problem labels
                    ztest[m][n] = initTopic;
                    rtest[m][n] = initRLabel;

                    ndkr_2_test[m][initTopic][initRLabel]++;
                    ndk_2_test[m][initTopic]++;
                    nkrw_2_test[initTopic][initRLabel][doctest[m][n]]++;
                    nkr_2_test[initTopic][initRLabel]++;
                }

                ndk_test[m][initTopic]++;
                nd_test[m]++;

            }
        }

        for(int i = 0; i < iterations; i++){
            //System.out.println("Iteration " + i);
            //Use Gibbs Sampling to update z[][]
            for(int m = 0; m < Mtest; m++){
                int N = docSet.docsTest.get(m).docWords.length;
                for(int n = 0; n < N; n++){

                    if (postype_test[m][n]==1) { //without problems
                        int oldTopic = ztest_1[m][n];
                        ndk_test[m][oldTopic]--;
                        nd_test[m]--;
                        /*for words without problem labels*/
                        nkw_1_test[oldTopic][doctest[m][n]]--;
                        nk_1_test[oldTopic]--;

                        double[] p = new double[K];

                        for (int z = 0; z < K; z++) {
                            //System.out.println(m+" " +z );
                            p[z] = ((ndk_test[m][z] + alpha)/
                                    (nd_test[m]+K*alpha))*
                                    (nkw_1[z][doctest[m][n]]+nkw_1_test[z][doctest[m][n]]+beta) / (nk_1[z]+nk_1_test[z]+beta*V);
                        }

                        int newTopic = getProbNorm(p, K);
                        ndk_test[m][newTopic]++;
                        nd_test[m]++;
                        nkw_1_test[newTopic][doctest[m][n]]++;
                        nk_1_test[newTopic]++;

                        ztest_1[m][n] = newTopic;

                    } else {
                            int oldTopic = ztest[m][n];
                            int oldRLabel= rtest[m][n];

                            ndk_test[m][oldTopic]--;
                            nd_test[m]--;
                            ndkr_2_test[m][oldTopic][oldRLabel]--;
                            ndk_2_test[m][oldTopic]--;
                            nkrw_2_test[oldTopic][oldRLabel][doctest[m][n]]--;
                            nkr_2_test[oldTopic][oldRLabel]--;

                            double[][] p = new double[K][R];
                            for (int z = 0; z < K; z++) {
                                for (int r = 0; r < R; r++) {
                                    p[z][r] = ((ndk_test[m][z] + alpha) / (nd_test[m] + K * alpha)) *
                                            ((ndkr_2_test[m][z][r] + eta) / (ndk_2_test[m][z]+R*eta))*
                                            ((nkrw_2[z][r][doctest[m][n]]+nkrw_2_test[z][r][doctest[m][n]]+beta_rw[r][doctest[m][n]]) / (nkr_2[z][r]+nkr_2_test[z][r]+betaSum_r[r]));
                                }
                            }
                            int[] labels = getProbNorm(p, K, R);
                            int newTopic = labels[0];
                            int newRLabel = labels[1];

                            ndk_test[m][newTopic]++;
                            nd_test[m]++;
                            ndkr_2_test[m][newTopic][newRLabel]++;
                            ndk_2_test[m][newTopic]++;
                            nkrw_2_test[newTopic][newRLabel][doctest[m][n]]++;
                            nkr_2_test[newTopic][newRLabel]++;
                            ztest[m][n] = newTopic;
                            rtest[m][n] = newRLabel;
                    }
                }
            }
        }

        double loglik = 0;
        double Wtest=0;

        // compute theta
        double[][] theta_q = new double[Mtest][K];
        for (int m = 0; m < Mtest; m++) {
            for (int k = 0; k < K; k++) {
                theta_q[m][k] = (ndk_test[m][k]+alpha) / (nd_test[m]+K*alpha);
            }
        }

        double[][][]pi_q = new double[Mtest][K][R];
        for (int m = 0; m < Mtest; m++) {
            for (int k = 0; k < K; k++) {
                for (int r = 0; r < R; r++) {
                    pi_q[m][k][r] = (ndkr_2_test[m][k][r] + eta) / (ndk_2_test[m][k]+R*eta);
                }
            }
        }


        // compute ppx
        for (int m = 0; m < Mtest; m++) {
            Wtest+=doctest[m].length;
            double sumW=0;
            for (int wordId = 0; wordId < doctest[m].length; wordId++) {

                double sumForSZ = 0;
                for (int z = 0; z < K; z++) {
                    for (int r = 0; r < R; r++) {
                        if (postype_test[m][wordId]==1)
                            sumForSZ +=theta_q[m][z]*phi_1[z][doctest[m][wordId]];
                        else
                            sumForSZ += pi_q[m][z][r]*phi_2[z][r][doctest[m][wordId]]* theta_q[m][z];
                    }
                }
                //sumW+=sumForSZ;
                /*for (int w = 0; w < doctest[m].length; w++)
                    if (w==wordId) occur++;*/
               if (sumForSZ!=0 && !isNaN(sumForSZ))
                    loglik += Math.log(sumForSZ);
                //System.out.println(loglik+" "+Math.log(sumForSZ));
            }
        }
        logger.info(loglik);
        logger.info(Wtest);
        logger.info(Math.exp(-loglik / Wtest));
    }

    public void saveIteratedModel(int iters, Documents docSet) throws IOException {
        super.saveIteratedModel(iters,docSet);
        String resPath = PathConfig.LdaResultsPath;
        String modelName = "tprphmodel_" + iters;

        BufferedWriter writer = new BufferedWriter(new FileWriter(resPath + modelName + ".twords-1"));
        int topNum = 20; //Find the top 20 topic words in each topic
        for (int i = 0; i < K; i++) {
                for (int r = 0; r < R; r++){
                    List<Integer> tWordsIndexArray = new ArrayList<Integer>();
                    for (int j = 0; j < V; j++) {
                        tWordsIndexArray.add(new Integer(j));
                    }
                    Collections.sort(tWordsIndexArray, new TPrPhModel.TwordsComparable(phi_2[i][r]));

                    writer.write("topic " + i + "; problem " + r + "\t:\t");
                    for (int t = 0; t < topNum; t++) {
                        String word = docSet.indexToTermMap.get(tWordsIndexArray.get(t));
                        String label="";
                        if (PositiveWords.isPositiveWord(word))
                            label=label+"+";
                        if (NegativeWords.isNegativeWord(word))
                            label=label+"-";
                        if (ProblemWords.isProblemWord(word))
                            label=label+"!";
                        writer.write(word +" "+label+" "+ phi_2[i][r][tWordsIndexArray.get(t)] + "\t");
                        //writer.write(docSet.indexToTermMap.get(tWordsIndexArray.get(t)) + " " + phi[i][l][r][tWordsIndexArray.get(t)] + "\t");
                    }
                    writer.write("\n");
                }
        }
        for (int i = 0; i < K; i++) {
                List<Integer> tWordsIndexArray = new ArrayList<Integer>();
                for (int j = 0; j < V; j++) {
                    tWordsIndexArray.add(new Integer(j));
                }
                Collections.sort(tWordsIndexArray, new TPrPhModel.TwordsComparable(phi_1[i]));
                writer.write("topic " + i + "\t:\t");
                for (int t = 0; t < topNum; t++) {
                    String word = docSet.indexToTermMap.get(tWordsIndexArray.get(t));
                    String label="";
                    if (PositiveWords.isPositiveWord(word))
                        label=label+"+";
                    if (NegativeWords.isNegativeWord(word))
                        label=label+"-";
                    if (ProblemWords.isProblemWord(word))
                        label=label+"!";
                    writer.write(word +" "+label+" "+ phi_1[i][tWordsIndexArray.get(t)] + "\t");
                    //writer.write(docSet.indexToTermMap.get(tWordsIndexArray.get(t)) + " " + phi[i][l][r][tWordsIndexArray.get(t)] + "\t");
                }
                writer.write("\n");
        }
        writer.close();
    }

}
