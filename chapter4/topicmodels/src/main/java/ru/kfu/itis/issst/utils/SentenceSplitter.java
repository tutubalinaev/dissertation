package ru.kfu.itis.issst.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class SentenceSplitter {
    public static final String DICTIONARY_FILE = "dictionary.txt";
    private Set<String> words;

    public String splitSentence(String sentence) {
        if (words.contains(sentence)) {
            return sentence;
        }

        for (int i = sentence.length() - 1; i > 0; i--){
            String left = sentence.substring(0, i);
            String right = sentence.substring(i, sentence.length());

            if (words.contains(left)) {
                right = splitSentence(right);
                if (right != null) {
                    return left + " " + right;
                }

                // undo our choice by going back to sentence
            }
        }

        return null;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scan = new Scanner(System.in);
        String toSplit = "";

        while (!toSplit.equals("q")) {
            System.out.print("Query: ");
            toSplit = scan.nextLine();
            System.out.println("Splitting: " + toSplit);

            SentenceSplitter ss = new SentenceSplitter();
            System.out.println("Got: " + ss.splitSentence(toSplit));
        }
        scan.close();
    }

    public SentenceSplitter() throws FileNotFoundException {
        this.readDictionary();
    }

    private void readDictionary() throws FileNotFoundException {
        Scanner input = new Scanner(new File(DICTIONARY_FILE));
        words = new HashSet<String>();
        while (input.hasNext()) {
            this.words.add(input.next());
        }
        input.close();
    }
}