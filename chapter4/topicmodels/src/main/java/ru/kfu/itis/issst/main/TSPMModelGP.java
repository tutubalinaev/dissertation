package ru.kfu.itis.issst.main;

/*
* In TSPM(GP), we assume that words’ problem
* labels are sampled from a corpus-specific distribution
* of pairs (topic, sentiment) independently from a review.
* */

import ru.kfu.itis.issst.utils.*;
import ru.kfu.itis.issst.conf.PathConfig;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TSPMModelGP extends TPModel {
    int[][] sentm;
    int[][] l;//sentiment label array l_d_n
    int L;//vocabulary size, topic number, document number

    int[][] ndk;
    int[] nd;
    int[][][] ndkl;
    int[][] nkl;
    int[][][] nklr;
    int[][][][] nklrw;

    double[][][][] phi;//Parameters for topic-sentiment-problem-word distribution K*L*R*V

    double[][][] beta_lrw;//beta prior (K x L x R x V)
    double[][] betaSum_lr; //beta prior K x L x R

    private float gamma;

    public TSPMModelGP(LdaGibbsSampling.modelparameters modelparam) {
        super(modelparam);
        gamma = modelparam.gamma;
        L = modelparam.sentimentNum;
    }

    public void initializeModel(Documents docSet) {
        gamma *= docSet.avgLen / L;
        eta *= docSet.avgLen / R;
        alpha=50/(float)K;
        logger.info("New parameters: (gamma, eta, alpha)=("+gamma+","+eta+","+alpha+")");

        super.initializeModel(docSet);
        // TODO Auto-generated method stub

        ndk = new int[M][K];
        nd = new int[M];
        ndkl = new int[M][K][L];
        nkl = new int[K][L];
        nklr = new int[K][L][R];
        nklrw = new int[K][L][R][V];

        phi = new double[K][L][R][V];

        //beta parameters
        beta_lrw = new double[L][R][V];//beta prior (L x K x V)
        betaSum_lr = new double[L][R]; //beta prior L x K
        prior2beta(docSet);

        //initialize documents index array
        sentm = new int[M][];
        for (int m = 0; m < M; m++) {
            int N = docSet.docs.get(m).docWords.length;
            sentm[m] = new int[N];
            for (int n = 0; n < N; n++) {
                sentm[m][n] = docSet.docs.get(m).priorSentiLabels[n];;
            }
        }

        initEstimate(docSet);
    }


    void initEstimate(Documents docSet) {
        //initialize topic label z for each word
        z = new int[M][];
        l = new int[M][];
        r = new int[M][];

        for (int m = 0; m < M; m++) { //for document
            int N = docSet.docs.get(m).docWords.length;
            z[m] = new int[N];
            l[m] = new int[N];
            r[m] = new int[N];
            for (int n = 0; n < N; n++) { //for each word
                int sentiLab = 0;
                if (sentm[m][n]!=0) {
                    sentiLab = (sentm[m][n]>0)?L-2:L-1; // incorporate prior information into the model
                } else {
                    sentiLab = (int) (Math.random() * L);
                }
                int problemLab = 0;
                if (problm[m][n]!=0) {
                    problemLab = (problm[m][n]>0)?R-2:R-1; // incorporate prior information into the model
                } else {
                    problemLab = (int) (Math.random() * R);
                }

                int initTopic = (int) (Math.random() * K);// From 0 to K - 1
                int initSLabel = sentiLab;// From 0 to S - 1
                int initRLabel = problemLab;// From 0 to R - 1

                z[m][n] = initTopic;
                l[m][n] = initSLabel;
                r[m][n] = initRLabel;

                ndk[m][initTopic]++;
                nd[m]++;
                ndkl[m][initTopic][initSLabel]++;
                nkl[initTopic][initSLabel]++;
                nklr[initTopic][initSLabel][initRLabel]++;
                nklrw[initTopic][initSLabel][initRLabel][doc[m][n]]++;
            }
        }
    }
    //join sentiment and problem priors
    public void prior2beta(Documents docSet) {
        for (int t = 0; t < V; t++) {
            double[] lambda_L = getSentimentPriorsByWord(docSet.indexToSentimentMap.get(t));
            double[] lambda_R = getProblemPriorsByWord(docSet.indexToProblemMap.get(t));
            for (int l = 0; l < L; l++)
                for (int r = 0; r < R; r++)
                    beta_lrw[l][r][t] =  lambda_L[l] + lambda_R[r];
        }
        for (int l = 0; l < L; l++) {
            for (int r = 0; r < R; r++) {
                betaSum_lr[l][r] = 0.0;
                for (int t = 0; t < V; t++) {
                    betaSum_lr[l][r] += beta_lrw[l][r][t];
                }
            }
        }
        System.out.println("-");
    }

    private double[] getSentimentPriorsByWord(int sentiment) {
        //neural, positive, negative
        //[0.01, 0.01, 0.01], [0.01, 1, 0.001] and [0.01, 0.001, 1]
        double[] lambda = new double[L];
        for (int i=0; i<L; i++)
            lambda[i] = beta;
        if (sentiment == 1) {
            lambda[1] = 1.0;
            lambda[L-1] = 0.001;
        } else if (sentiment == -1) {
            lambda[1] = 0.001;
            lambda[L-1] = 1.0;
        }
        return lambda;
    }

    //phi = new double[K][L][R][V];
    void computePhi() {
        for (int z = 0; z < K; z++) {
            for (int l = 0; l < L; l++) {
                for (int r = 0; r < R; r++) {
                    for (int w = 0; w < V; w++) {
                        phi[z][l][r][w] = (nklrw[z][l][r][w]+beta_lrw[l][r][w]) / (nklr[z][l][r]+betaSum_lr[l][r]);
                    }
                }
            }
        }
    }

    public void inferenceModel(Documents docSet) throws IOException {
        // TODO Auto-generated method stub
        if (iterations < saveStep + beginSaveIters) {
            System.err.println("Error: the number of iterations should be larger than " + (saveStep + beginSaveIters));
            System.exit(0);
        }
        for (int i = 1; i <= iterations; i++) {
            //System.out.println("Iteration " + i);
            if ((i >= beginSaveIters) && (((i - beginSaveIters) % saveStep) == 0)) {
                //Saving the model
                //System.out.println("Saving model at iteration " + i + " ... ");
            }
            if (i>0 & updateParaStep > 0 && i % updateParaStep == 0) {

                //this.runSentimentPrediction(docSet);
                updateBetaParam(i);
                //this.runSentimentPrediction(docSet);
            }
            //Use Gibbs Sampling to update z[][]
            for (int m = 0; m < M; m++) {
                int N = docSet.docs.get(m).docWords.length;
                for (int n = 0; n < N; n++) {
                    // Sample from p(z_i|z_-i, w)
                    int[] res = sampleTopicZ(m, n);

                    l[m][n] = res[0];
                    z[m][n] = res[1];
                    r[m][n] = res[2];
                }
            }
        }
        updateDistributions();
    }

    void updateDistributions(){
        //computeXi();
        //computeTheta();
        computePhi();
        //computePi();
    }

    private int[] sampleTopicZ(int m, int n) {
        int oldTopic = z[m][n];
        int oldSLabel = l[m][n];
        int oldRLabel = r[m][n];

        ndk[m][oldTopic]--;
        nd[m]--;
        ndkl[m][oldTopic][oldSLabel]--;
        nkl[oldTopic][oldSLabel]--;
        nklr[oldTopic][oldSLabel][oldRLabel]--;
        nklrw[oldTopic][oldSLabel][oldRLabel][doc[m][n]]--;

        double[][][] p = new double[K][L][R];

        for (int z = 0; z < K; z++) {
            for (int l = 0; l < L; l++) {
                for (int r = 0; r < R; r++) {
                    p[z][l][r] = ((nklrw[z][l][r][doc[m][n]]+beta_lrw[l][r][doc[m][n]]) / (nklr[z][l][r]+betaSum_lr[l][r]))
                            * ((nklr[z][l][r] + eta) / (nkl[z][l] + R*eta))
                            * ((ndk[m][z]+alpha) / (nd[m]+K*alpha))
                            * ((ndkl[m][z][l] + gamma) / (ndk[m][z]+L*gamma));
                }
            }
        }
        int[] labels = getProbNorm(p, K, L, R);
        int newLabel = labels[0];
        int newTopic = labels[1];
        int newRlabel = labels[2];

        ndk[m][newTopic]++;
        nd[m]++;
        ndkl[m][newTopic][newLabel]++;
        nkl[newTopic][newLabel]++;
        nklr[newTopic][newLabel][newRlabel]++;
        nklrw[newTopic][newLabel][newRlabel][doc[m][n]]++;

        int[] res = {newLabel, newTopic, newRlabel};

        return res;
    }

    public void saveIteratedModel(int iters, Documents docSet) throws IOException {
        super.saveIteratedModel(iters,docSet);
        String resPath = PathConfig.LdaResultsPath;
        String modelName = "stpm(gp)_" + iters;

        BufferedWriter writer = new BufferedWriter(new FileWriter(resPath + modelName + ".twords-2"));
        int topNum = 20; //Find the top 20 topic words in each topic
        for (int i = 0; i < K; i++) {
            for (int l = 0; l < L; l++) {
                for (int r = 0; r < R; r++){
                    List<Integer> tWordsIndexArray = new ArrayList<Integer>();
                    for (int j = 0; j < V; j++) {
                        tWordsIndexArray.add(new Integer(j));
                    }
                    Collections.sort(tWordsIndexArray, new TSPMModel.TwordsComparable(phi[i][l][r]));
                    writer.write("topic " + i + "; sentiment " + l+"; problem " + r + "\t:\t");
                    for (int t = 0; t < topNum; t++) {
                        String word = docSet.indexToTermMap.get(tWordsIndexArray.get(t));
                        String label="";
                        if (PositiveWords.isPositiveWord(word))
                            label=label+"+";
                        if (NegativeWords.isNegativeWord(word))
                            label=label+"-";
                        if (ProblemWords.isProblemWord(word))
                            label=label+"!";
                        writer.write(word +" "+label+" "+ phi[i][l][r][tWordsIndexArray.get(t)] + "\t");
                        //writer.write(docSet.indexToTermMap.get(tWordsIndexArray.get(t)) + " " + phi[i][l][r][tWordsIndexArray.get(t)] + "\t");
                    }
                    writer.write("\n");
                }
            }
        }
        writer.close();
        writer = new BufferedWriter(new FileWriter(resPath + modelName + ".tbetas"));
        for (int j = 0; j < V; j++) {
            writer.write(docSet.indexToTermMap.get(j) + "\n");
            for (int l = 0; l < L; l++) {
                for (int r = 0; r < R; r++) {
                    writer.write("sentiment " + l + "; topic " + ";\t" + beta_lrw[l][r][j]);
                    writer.write("\n");
                }
            }
            writer.write("\n");
        }
        writer.close();
    }

    //EVALUATION
    public double[][] runSentimentPrediction(Documents docSet) {
        double[][] pi_q = new double[Mtest][R];
        for (int m = 0; m < Mtest; m++) {
            int N = docSet.docsTest.get(m).docWords.length;
            for (int r = 0; r < R; r++) {
                pi_q[m][r] = 1;
                double value = 0;
                for (int n = 0; n < N; n++) { //for each word
                    for (int z = 0; z < K; z++) {
                        for (int l = 0; l < L; l++) {
                            value += ((nklrw[z][l][r][doctest[m][n]] + beta_lrw[l][r][doctest[m][n]]) / (nklr[z][l][r] + betaSum_lr[l][r]));
                        }
                    }
                    if (value != 0)
                        pi_q[m][r] *= value;
                }
            }
        }
        Evaluation.runEval(Mtest, pi_q, docSet, R - 2);
        return pi_q;
    }

    public void estimateModel(Documents docSet) throws IOException {
        logger.info("Perplexity is computing....");

        int[][] ztest = new int[Mtest][];
        int[][] ltest = new int[Mtest][];
        int[][] rtest = new int[Mtest][];

        int[][] nkl_test=new int[K][L];
        int[][] ndk_test=new int[Mtest][K];
        int[] nd_test=new int[Mtest];
        int[][][][]nklrw_test=new int[K][L][R][V];
        int[][][] nklr_test=new int[K][L][R];
        int[][][] ndkl_test=new int[Mtest][K][L];

        int[][] sentm_test = new int[Mtest][];
        int[][] problm_test = new int[Mtest][];
        for (int m = 0; m < Mtest; m++) {
            //Notice the limit of memory
            int N = docSet.docsTest.get(m).docWords.length;
            sentm_test[m] = new int[N];
            problm_test[m] = new int[N];
            for (int n = 0; n < N; n++) {
                sentm_test[m][n] = docSet.docsTest.get(m).priorSentiLabels[n];
                problm_test[m][n] = docSet.docsTest.get(m).priorProblemLabels[n];
            }
        }

        for(int m = 0; m < Mtest; m++) {
            int N = docSet.docsTest.get(m).docWords.length;
            ztest[m] = new int[N];
            ltest[m] = new int[N];
            rtest[m] = new int[N];
            for (int n = 0; n < N; n++) {
                int sentiLab = 0;
                if (sentm_test[m][n]!=0) {
                    sentiLab = (sentm_test[m][n]>0)?L-2:L-1; // incorporate prior information into the model
                } else {
                    sentiLab = (int) (Math.random() * L);
                }
                int problemLab = 0;
                if (problm_test[m][n]!=0) {
                    problemLab = (problm_test[m][n]>0)?R-2:R-1; // incorporate prior information into the model
                } else {
                    problemLab = (int) (Math.random() * R);
                }

                int initTopic = (int) (Math.random() * K);// From 0 to K - 1
                int initSLabel = sentiLab;// From 0 to S - 1
                int initRLabel = problemLab;// From 0 to R - 1

                ztest[m][n] = initTopic;
                ltest[m][n] = initSLabel;
                rtest[m][n] = initRLabel;

                ndk_test[m][initTopic]++;
                nd_test[m]++;
                ndkl_test[m][initTopic][initSLabel]++;
                nkl_test[initTopic][initSLabel]++;
                nklr_test[initTopic][initSLabel][initRLabel]++;
                nklrw_test[initTopic][initSLabel][initRLabel][doctest[m][n]]++;
            }
        }

        for(int i = 0; i < iterations; i++){
            //System.out.println("Iteration " + i);
            //Use Gibbs Sampling to update z[][]
            for(int m = 0; m < Mtest; m++){
                int N = docSet.docsTest.get(m).docWords.length;
                for(int n = 0; n < N; n++){
                    int oldTopic = ztest[m][n];
                    int oldSLabel = ltest[m][n];
                    int oldRLabel = rtest[m][n];

                    ndk_test[m][oldTopic]--;
                    nd_test[m]--;
                    nkl_test[oldTopic][oldSLabel]--;
                    ndkl_test[m][oldTopic][oldSLabel]--;
                    nklr_test[oldTopic][oldSLabel][oldRLabel]--;
                    nklrw_test[oldTopic][oldSLabel][oldRLabel][doctest[m][n]]--;

                    double[][][] p = new double[K][L][R];

                    for (int z = 0; z < K; z++) {
                        for (int l = 0; l < L; l++) {
                            for (int r = 0; r < R; r++) {
                                p[z][l][r] = ((nklrw[z][l][r][doctest[m][n]]+nklrw_test[z][l][r][doctest[m][n]]+beta_lrw[l][r][doctest[m][n]]) / (nklr[z][l][r]+nklr_test[z][l][r]+betaSum_lr[l][r]))
                                        * ((nklr_test[z][l][r] + eta) / (nkl_test[z][l] + R*eta))
                                        * ((ndk_test[m][z]+alpha) / (nd_test[m]+K*alpha))
                                        * ((ndkl_test[m][z][l] + gamma) / (ndk_test[m][z]+L*gamma));
                            }
                        }
                    }
                    int[] labels = getProbNorm(p, K, L, R);
                    int newLabel = labels[0];
                    int newTopic = labels[1];
                    int newRlabel = labels[2];

                    ndk_test[m][newTopic]++;
                    nd_test[m]++;
                    nkl_test[newTopic][newLabel]++;
                    ndkl_test[m][newTopic][newLabel]++;
                    nklr_test[newTopic][newLabel][newRlabel]++;
                    nklrw_test[newTopic][newLabel][newRlabel][doctest[m][n]]++;

                    ztest[m][n] = newTopic;
                    ltest[m][n] = newLabel;
                    rtest[m][n] = newRlabel;
                }
            }
        }

        double loglik = 0;
        double Wtest=0;

        // compute theta
        double[][] theta_q = new double[Mtest][K];
        for (int m = 0; m < Mtest; m++) {
            for (int k = 0; k < K; k++) {
                theta_q[m][k] = (ndk_test[m][k]+alpha) / (nd_test[m]+K*alpha);
            }
        }

        double[][][]pi_q = new double[Mtest][K][L];
        for (int m = 0; m < Mtest; m++) {
            for (int k = 0; k < K; k++) {
                for (int l = 0; l < L; l++) {
                    pi_q[m][k][l] = (ndkl_test[m][k][l] + gamma) / (ndk_test[m][k]+L*gamma);
                }
            }
        }
        double[][][][]xi_q = new double[Mtest][K][L][R];
        for (int m = 0; m < Mtest; m++) {
            for (int z = 0; z < K; z++) {
                for (int l = 0; l < L; l++) {
                    for (int r = 0; r < R; r++) {
                        xi_q[m][z][l][r] = (nklr_test[z][l][r] + eta) / (nkl_test[z][l] + R*eta);
                    }
                }
            }
        }

        // compute ppx
        for (int m = 0; m < Mtest; m++) {
            Wtest+=doctest[m].length;
            double sumW=0;
            for (int wordId = 0; wordId < doctest[m].length; wordId++) {
                double sumForSZ = 0;
                for (int z = 0; z < K; z++) {
                    for (int l = 0; l < L; l++) {
                        for (int r = 0; r < R; r++) {
                            sumForSZ += pi_q[m][z][l]*phi[z][l][r][doctest[m][wordId]]* xi_q[m][z][l][r]* theta_q[m][z];
                        }
                    }
                }
                if (sumForSZ!=0)
                    loglik += Math.log(sumForSZ);;
            }
        }
        logger.info(loglik);
        logger.info(Wtest);
        logger.info(Math.exp(-loglik / Wtest));
    }

    //updating priors based on Nikolenko's paper
    private void updateBetaParam(int niter){
        System.out.println("updateBetaParam " + niter);
        int[][][] nlrt=new int[L][R][V];
        double[] nt=new double[V];
        for (int z = 0; z < K; z++) {
            for (int l = 0; l < L; l++) {
                for (int r = 0; r < R; r++) {
                    for (int t = 0; t < V; t++) {
                        nlrt[l][r][t] += nklrw[z][l][r][t];
                        nt[t]+=nklrw[z][l][r][t];
                    }
                }
            }
        }

        for (int l = 0; l < L; l++)  {
            for (int r = 0; r < R; r++) {
                betaSum_lr[l][r]=0;
                for(int t = 0; t < V; t++) {
                    double tp=1/((double)nt[t]*Math.max(1,(double)updateParaStep/niter));
                    if (nlrt[l][r][t]!=0)
                        beta_lrw[l][r][t]=tp*nlrt[l][r][t];
                    betaSum_lr[l][r] += beta_lrw[l][r][t];
                }
            }
        }
    }
}