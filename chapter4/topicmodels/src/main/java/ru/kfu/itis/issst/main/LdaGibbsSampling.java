package ru.kfu.itis.issst.main;

import org.apache.log4j.Logger;
import ru.kfu.itis.issst.conf.ConstantConfig;
import ru.kfu.itis.issst.conf.PathConfig;
import ru.kfu.itis.issst.utils.FileUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by TutubalinaEV on 22.06.2015.
 */
public class LdaGibbsSampling {

    final public static Logger logger = Logger.getLogger(TPModel.class);

    public static class modelparameters {
        public float alpha = 0.5f; //usual value is 50 / K
        public float beta = 0.01f;//usual value is 0.1
        public float gamma = 0.01f;
        public float eta = 0.01f;
        public static int topicNum = 10;
        public int sentimentNum = 3;
        public int problemNum = 2;
        public int iteration = 100;
        public int saveStep = 10;
        public int beginSaveIters = 50;

        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append("alpha = " + alpha + ";");
            sb.append("beta = " + beta + ";");
            sb.append("gamma = " + gamma + ";");
            sb.append("eta = " + eta + ";");
            sb.append("topicNum = " + topicNum + ";");
            sb.append("sentimentNum = " + sentimentNum + ";");
            sb.append("problemNum = " + problemNum + ";");
            sb.append("iteration = " + iteration + ";");
            return sb.toString();
        }
    }

    /**
     * Get parameters from configuring file. If the
     * configuring file has value in it, use the value.
     * Else the default value in program will be used
     *
     * @param ldaparameters
     * @param parameterFile
     * @return void
     */
    public static void getParametersFromFile(modelparameters ldaparameters,
                                             String parameterFile) {
        // TODO Auto-generated method stub
        ArrayList<String> paramLines = new ArrayList<String>();
        FileUtil.readLines(parameterFile, paramLines);
        for (String line : paramLines) {
            String[] lineParts = line.split("\t");
            switch (parameters.valueOf(lineParts[0])) {
                case alpha:
                    ldaparameters.alpha = Float.valueOf(lineParts[1]);
                    break;
                case beta:
                    ldaparameters.beta = Float.valueOf(lineParts[1]);
                    break;
                case gamma:
                    ldaparameters.gamma = Float.valueOf(lineParts[1]);
                    break;
                case eta:
                    ldaparameters.eta = Float.valueOf(lineParts[1]);
                    break;
                case topicNum:
                    ldaparameters.topicNum = Integer.valueOf(lineParts[1]);
                    break;
                case sentimentNum:
                    ldaparameters.sentimentNum = Integer.valueOf(lineParts[1]);
                    break;
                case problemNum:
                    ldaparameters.problemNum = Integer.valueOf(lineParts[1]);
                    break;
                case iteration:
                    ldaparameters.iteration = Integer.valueOf(lineParts[1]);
                    break;
                case saveStep:
                    ldaparameters.saveStep = Integer.valueOf(lineParts[1]);
                    break;
                case beginSaveIters:
                    ldaparameters.beginSaveIters = Integer.valueOf(lineParts[1]);
                    break;
            }
        }
    }

    public enum parameters {
        alpha, beta, gamma, eta, topicNum, sentimentNum, problemNum, iteration, saveStep, beginSaveIters;
    }

    //main function to load
    public static void trainLDA() throws IOException {
        String resultPath = PathConfig.LdaResultsPath;
        String parameterFile = ConstantConfig.LDAPARAMETERFILE;
        modelparameters ldaparameters = new modelparameters();
        getParametersFromFile(ldaparameters, parameterFile);

        //PathConfig.ldaDocsPath = "/Users/telena/Documents/DATA/fin_mobile_apps/xmls/train/";
        //PathConfig.ldaTestDocsPath = "/Users/telena/Documents/DATA/fin_mobile_apps/xmls/test/";

        Documents docSet = new Documents(modeLabel.singular);

        //docSet.readAndShuffleDocs(PathConfig.ldaDocsPath, 0.9);
        docSet.readDocs(PathConfig.ldaDocsPath);
        docSet.readTestDocs(PathConfig.ldaTestDocsPath);

        logger.info("wordMap size " + docSet.termToIndexMap.size());
        logger.info("docSet avgLen " + docSet.avgLen);
        FileUtil.mkdir(new File(resultPath));

        TSPMModel model1 = new TSPMModel(ldaparameters);
        System.out.println("1 Initialize the model ...");
        model1.initializeModel(docSet);
        System.out.println("2 Learning and Saving the model ...");
        model1.inferenceModel(docSet);
        model1.runSentimentPrediction(docSet);
        System.out.println("3 Output the final model ...");
        //model1.estimateModel(docSet);
        model1.saveIteratedModel(ldaparameters.iteration, docSet);

    }


    /**
     * @param args
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        trainLDA();
    }
}
