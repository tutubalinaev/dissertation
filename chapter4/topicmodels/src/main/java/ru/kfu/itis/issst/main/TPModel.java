package ru.kfu.itis.issst.main;

import org.apache.log4j.Logger;
import ru.kfu.itis.issst.conf.PathConfig;
import ru.kfu.itis.issst.utils.FileUtil;
import ru.kfu.itis.issst.utils.NegativeWords;
import ru.kfu.itis.issst.utils.PositiveWords;
import ru.kfu.itis.issst.utils.ProblemWords;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by TutubalinaEV on 19.08.2015.
 */
public class TPModel {

    final static Logger logger = Logger.getLogger(TPModel.class);

    int[][] doc;//word index array
    int[][] doctest;

    float alpha;
    float beta;
    float eta;

    int[][] z;//topic label array    z_d_n
    int[][] r;//problem label array r_d_n

    int[][] problm;

    int V, K, R, M, Mtest;

    int iterations;//Times of iterations
    int saveStep;//The number of iterations between two saving
    int beginSaveIters;//Begin save model at this iteration
    int updateParaStep = 200;

    public TPModel(LdaGibbsSampling.modelparameters modelparam) {
        alpha = modelparam.alpha;
        beta = modelparam.beta;
        eta  = modelparam.eta;
        iterations = modelparam.iteration;
        K = modelparam.topicNum;
        R = modelparam.problemNum;
        saveStep = modelparam.saveStep;
        beginSaveIters = modelparam.beginSaveIters;

        //logs
        logger.info(this.getClass());
        logger.info(modelparam);
    }

    public void initializeModel(Documents docSet) {
        // TODO Auto-generated method stub

        M = docSet.docs.size();
        Mtest = docSet.docsTest.size();
        V = docSet.termToIndexMap.size();

        //initialize documents index array
        doc = new int[M][];
        problm = new int[M][];

        for (int m = 0; m < M; m++) {
            //Notice the limit of memory
            int N = docSet.docs.get(m).docWords.length;
            doc[m] = new int[N];
            problm[m] = new int[N];
            for (int n = 0; n < N; n++) {
                doc[m][n] = docSet.docs.get(m).docWords[n];
                problm[m][n] = docSet.docs.get(m).priorProblemLabels[n];
            }
        }
        doctest = new int[Mtest][];
        for (int m = 0; m < Mtest; m++) {
            int N = docSet.docsTest.get(m).docWords.length;
            doctest[m] = new int[N];
            for (int n = 0; n < N; n++) {
                doctest[m][n] = docSet.docsTest.get(m).docWords[n];
            }
        }

        logger.info("TRAIN's size:" + M);
        logger.info("TEST's size:" + Mtest);
        logger.info("V's size:" + V);
    }

    double[] getProblemPriorsByWord(int problem) {
        //unknown, no-problem, problem
        //[0.01, 0.01, 0.01], [0.01, 2, 0.001] and [0.01, 0.001, 2]
        double[] lambda = new double[R];
        for (int i=0; i<R; i++)
            lambda[i] = beta;
        if (problem == 1) {
            lambda[0] = 1.0;
            //lambda_L[1] = 2;
            lambda[R-1] = 0.001;
        } else if (problem == -1) {
            lambda[0] = 0.001;
            //lambda_L[1] = 0.01;
            lambda[R-1] = 1.0;
        }
        return lambda;
    }
    public int getProbNorm(double[] p, int K){
        for(int k = 1; k < K; k++){
            p[k] += p[k - 1];
        }
        double u = Math.random() * p[K - 1]; //p[] is unnormalised
        int newTopic;
        for(newTopic = 0; newTopic < K; newTopic++){
            if(u < p[newTopic]){
                break;
            }
        }
        return newTopic;
    }
    public int[] getProbNorm(double[][] p, int K, int L) {
        // accumulate multinomial parameters
        // accumulate multinomial parameters
        for (int i = 0; i < K; i++) {
            for (int s = 0; s < L; s++) {
                if (s == 0) {
                    if (i == 0) continue;
                    else p[i][s] += p[i - 1][L - 1]; // accumulate the sum of the previous array
                } else p[i][s] += p[i][s - 1];
            }
        }
        double u = Math.random() * p[K - 1][L - 1];
        boolean loopBreak = false;
        int newTopic = 0;
        int newLabel = 0;
        for (newTopic = 0; newTopic < K; newTopic++) {
            for (newLabel = 0; newLabel < L; newLabel++) {
                if (p[newTopic][newLabel] > u) {
                    loopBreak = true;
                    break;
                }
            }
            if (loopBreak == true) {
                break;
            }
        }
        if (newTopic == K) newTopic = K - 1;
        if (newLabel == L) newLabel = L - 1; // to avoid over array boundary

        int[] res = {newTopic, newLabel};
        return res;
    }

    public int[] getProbNorm(double[][][] p, int K, int L, int R) {

        // accumulate multinomial parameters
        double sumProb = 0;
        for (int k = 0; k < K; k++)
            for (int l = 0; l < L; l++)
                for (int r = 0; r < R; r++)
                    sumProb += p[k][l][r];

        int newTopic = K;
        int newLabel = L;
        int newRLabel = R;

        double randNo = Math.random() * sumProb;
        double tmpSumProb = 0;
        boolean found = false;
        for (int k = 0; k < K; k++) {
            for (int l = 0; l < L; l++) {
                for (int r = 0; r < R; r++){
                    tmpSumProb += p[k][l][r];
                    if (randNo <= tmpSumProb) {
                        newTopic = k;
                        newLabel = l;
                        newRLabel = r;
                        found = true;
                    }
                    if (found) break;
                }
                if (found) break;
            }
            if (found) break;
        }

        if (newRLabel == R) newRLabel = R - 1;
        if (newTopic == K) newTopic = K - 1;
        if (newLabel == L) newLabel = L - 1; // to avoid over array boundary

        int[] res = {newLabel, newTopic, newRLabel};
        return res;
    }

    public void saveIteratedModel(int iters, Documents docSet) throws IOException {
        // TODO Auto-generated method stub
        //lda.params lda.phi lda.theta lda.tassign lda.twords
        //lda.params
        String resPath = PathConfig.LdaResultsPath;
        String modelName = "lda_" + iters;
        ArrayList<String> lines = new ArrayList<String>();
        lines.add("alpha = " + alpha);
        lines.add("beta = " + beta);
        lines.add("topicNum = " + K);
        lines.add("problemNum = " + R);
        lines.add("docNum = " + M);
        lines.add("termNum = " + V);
        lines.add("iterations = " + iterations);
        lines.add("saveStep = " + saveStep);
        lines.add("beginSaveIters = " + beginSaveIters);
        FileUtil.writeLines(resPath + modelName + ".params", lines);
    }

    public class TwordsComparable implements Comparator<Integer> {
        public double[] sortProb; // Store probability of each word in topic k

        public TwordsComparable(double[] sortProb) {
            this.sortProb = sortProb;
        }


        public int compare(Integer o1, Integer o2) {
            // TODO Auto-generated method stub
            //Sort topic word index according to the probability of each word in topic k
            if (sortProb[o1] > sortProb[o2]) return -1;
            else if (sortProb[o1] < sortProb[o2]) return 1;
            else return 0;
        }
    }



}
