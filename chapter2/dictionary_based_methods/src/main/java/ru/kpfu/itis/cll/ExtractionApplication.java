package ru.kpfu.itis.cll;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.json.JSONObject;

public class ExtractionApplication {
	String scriptName = null;
	String PPH_TYPE = null;
	String NONPPH_TYPE = null;
	String POSS_PPH_TYPE = null;
	String IMP_TYPE = null;
	String jsonparam=null;

	Set<JSONObject> dataList;	
	public static POSDetector detector;

	public ExtractionApplication(Set<JSONObject> dataList) {
		this.dataList = dataList;
		detector = new POSDetector();
	}

	public void setScriptName(int system) {
		switch (system) {
		case 0:
			this.scriptName = "ru.kpfu.itis.cll.BasicProblemExtractor";
			jsonparam="l_dba";
			break;
		case 1:
			this.scriptName = "ru.kpfu.itis.cll.ClauseProblemExtractor";
			jsonparam="l_cba";
			break;
		case 2:
			this.scriptName = "ru.kpfu.itis.cll.BasicEnProblemExtractor";
			jsonparam="l_dba";
			break;
		case 3:
			this.scriptName = "ru.kpfu.itis.cll.ClauseEnProblemExtractor";
			jsonparam="l_cba";
			break;
		default:
			this.scriptName = "ru.kpfu.itis.cll.ClauseProblemExtractor";
			jsonparam="l_cba";
			break;
		}
		PPH_TYPE = scriptName + ".ProblemSentence";
		NONPPH_TYPE = scriptName + ".NotProblemSentence";
		POSS_PPH_TYPE = scriptName + ".DeficiencyPhrase";
		IMP_TYPE = scriptName + ".ImperativeTrigger";
	}
	/*
	 *  This function is used to classify objects. Then all json objects are saved into output array.
	 */
	public void classifyData(int i) throws Exception {
		setScriptName(i);

		final AnalysisEngine engine = AnalysisEngineFactory
				.createEngine(scriptName + "Engine");
		final CAS cas = engine.newCAS();
		ArrayList<String> res = new ArrayList<String>();
		for (JSONObject obj : dataList) {
			JSONObject obj_new = run(engine, cas, obj);
			res.add(obj_new.toString());
		}
	}
	/*
	 * This function is similar to classifyData. Item object is created due to an evaluation step;
	 */
	public void classifyAndEvalData(int i) throws Exception {
		setScriptName(i);
		final AnalysisEngine engine = AnalysisEngineFactory
				.createEngine(scriptName + "Engine");
		final CAS cas = engine.newCAS();
		ArrayList<Item> objects = new ArrayList<Item>();
		for (JSONObject obj : dataList) {
			JSONObject obj_new = run(engine, cas, obj);
			Item item = new Item("","",obj_new.getString("problemscore"));
			item.setClassByKey(obj_new.getString(jsonparam));
			objects.add(item);
		}	
		Evaluation.runEval(objects);  
	}

	public JSONObject run(AnalysisEngine engine, CAS cas, JSONObject obj)
			throws Exception {
		Item object = new Item(obj.getString("text"),
				obj.getString("stemmedText"), "problem");
		String text = object.getStemmedText();

		cas.reset();
		cas.setDocumentText(text + " .");
		engine.process(cas);
		Collection<AnnotationFS> pphrases = CasUtil.select(cas, cas
				.getTypeSystem().getType(PPH_TYPE));
		Collection<AnnotationFS> non_pphrases = CasUtil.select(cas, cas
				.getTypeSystem().getType(NONPPH_TYPE));

		ArrayList<AnnotationFS> prphrases = new ArrayList<AnnotationFS>();

		if (pphrases.size() == 0 && non_pphrases.size() == 0) {
			Collection<AnnotationFS> poss_pphrases = CasUtil.select(cas, cas
					.getTypeSystem().getType(POSS_PPH_TYPE)); // DeficiencyPhrase
			for (AnnotationFS item : poss_pphrases) {
				// check phrases' pos tags
				LinkedList<String> tags = detector.getTags(item.getCoveredText(), "pos");
				if (tags.size() == 0) {
					prphrases.add(new Annotation(1, null));
					break;
				}
				for (String tag : tags) {
					if (tag.equals("substantive")) {
						prphrases.add(new Annotation(1, null));
						break;
					}
				}
				prphrases.add(new Annotation(1, null));
			}
		}
		boolean isProblemDetected = false;

		if (prphrases.size() > 0 || pphrases.size() > 0) {
			isProblemDetected = true;
		} else {
			text = object.getText();
			cas.reset();
			cas.setDocumentText(text + " .");
			engine.process(cas);
			pphrases = CasUtil.select(cas, cas.getTypeSystem()
					.getType(IMP_TYPE));
			if (pphrases.size() > 0)
				isProblemDetected = true;
		}
		if (isProblemDetected)
			obj.put(jsonparam, "problem");
		else
			obj.put(jsonparam, "no-problem");
		return obj;
	}

}
