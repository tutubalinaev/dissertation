package ru.kpfu.itis.cll;

enum classLabel {problem, noproblem}

public class Item {
	String text;
	String stemmedText;
	classLabel classID;
	classLabel predictedID;

	public Item(String text, String className) {
		this.text=text;
		this.classID=classLabel.valueOf(className.replace("-", ""));
	}
	public Item(String text, String sttext, String className) {
		this.text=text;
		this.stemmedText=sttext;
		this.classID=classLabel.valueOf(className.replace("-", ""));
	}

	public String getText() {		
		return text;
	}
	public String getStemmedText() {
		return getFilteredText(stemmedText);
	}
	public String getFilteredText(String str){
		String res=str.toLowerCase();
    	//text="нарекание пока нет.";
		res = res.replaceAll("quot;","");
		res = getFilteredRUText(res);
		res = getFilteredEnText(res);

		res = res.replaceAll("[?:!;(),.],", ",");
		
		res = res+" .";
		res = res.replaceAll("[?:!;(),]\\.", "\\.");
		//res = res.replaceAll("[?:!;(),.]+?,", ",");
    	return res;
	}
	
	private String getFilteredRUText(String res){
		res = res.replaceAll(",[\\s]?но", " но");
		res = res.replaceAll(",[\\s]?хотя", " хотя");
		res = res.replaceAll(",[\\s]?что", " что");
		res = res.replaceAll(",[\\s]?а", " а");
		res = res.replaceAll(",[\\s]?пока", " пока");
		res = res.replaceAll(",[\\s]?если", " если");		
		res = res.replaceAll("[?:!;(),]", ";");
		res = res.replaceAll("[;]+", ";");
		res = res.replaceAll("[.]+", ".");		
		res = res.replaceAll(" но ", ", но ");
		res = res.replaceAll(" а ", ", а ");
		res = res.replaceAll(" хотя ", ", хотя ");
		res = res.replaceAll(" что ", ", что ");
		res = res.replaceAll(" пока ", ", пока ");
		res = res.replaceAll(" если ", ", если ");
		res = res.replaceAll(" то ", ", то ");
		res = res.replaceAll(" правда ", ", правда ");
		return res;
	}
	private String getFilteredEnText(String res){
		res = res.replaceAll("won\'t","will not");
		res = res.replaceAll("can\'t", "cannot");
		res = res.replaceAll("i\'m", "i am");
		res = res.replaceAll("ain\'t", "is not");
		res = res.replaceAll("(\\w+)\'ll", "$1 will");
		res = res.replaceAll("(\\w+)n\'t", "$1 not");
		res = res.replaceAll("(\\w+)\'ve", "$1 have");
		res = res.replaceAll("(\\w+)\'s", "$1 is");
		res = res.replaceAll("(\\w+)\'re", "$1 are");
		res = res.replaceAll("(\\w+)\'d", "$1 would");
		
		res = res.replaceAll(",[\\s]?but", " but");
		res = res.replaceAll(" but ", ", but ");
		res = res.replaceAll(",[\\s]?because", " because");
		res = res.replaceAll(" because ", ", because ");
		
		return res;
	}
	
	
	public void setProblemClass(){
		predictedID=classLabel.problem;
	}
	public void setNoProblemClass(){
		predictedID=classLabel.noproblem;
	}
	
	public void setClassByKey(String key){
		if (key.equals("problem"))
			predictedID=classLabel.problem;
		else 
			predictedID=classLabel.noproblem;
	}
	
	
	
	@Override
	public String toString() {		
		return text+" "+stemmedText+" "+classID+" "+predictedID;
	}
}
