package ru.kpfu.itis.cll;
import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ru.itbrains.gate.morph.MorphInfo;
import ru.itbrains.gate.morph.MorphParser;

public class POSDetector {
	static MorphParser mp=null;
	
	public POSDetector() {
		mp=new MorphParser();
	}
	
    public LinkedList<String> getTags(String text, String key) throws IOException{    	
		List<MorphInfo> list=mp.runParser(text, "UTF-8");
		LinkedList<String> tags=new LinkedList<String>();
		for (MorphInfo inf:list){
			//System.out.println(inf.getHomonymGrammems());
			for (Map<String, String>map:inf.getHomonymGrammems()){
				//String word=map.get("baseForm");
				String word=inf.getOriginalWord();
				//System.out.println(word+"\\"+MorphDictionary.getPOS(map.get("pos")));
				if (map.get(key)!=null){
					tags.add(map.get(key));
					if (key.equals("mood") && map.get(key).equals("imperative"))
						System.out.println(word);
					
				}
					
				break;
			}
		}
		return tags;
    }
    	
	// morph dictionary
	static class MorphDictionary {
		public static String getPOS(String line) {
			Hashtable<String, String> table = new Hashtable<String, String>();
			//System.out.println(line);
			table.put("adjective", "A");
			table.put("adverb", "ADV");
			table.put("interjection", "INJ");
			table.put("numeral", "NUM");
			table.put("substantive", "S");
			table.put("verb", "V");
			table.put("preposition", "PR");
			table.put("particle", "PART");
			table.put("conjunction", "CONJ");
			table.put("s-pronoun", "SPRO");
			table.put("adv-pronoun", "ADVPRO");
			table.put("a-pronoun", "APRO");
			table.put("a-numeral", "ANUM");
			table.put("composite", "COM");
			return (line!=null&&table.containsKey(line))?table.get(line):"unkn";
		}
		
		public static String getMult(String line) {
			Hashtable<String, String> table = new Hashtable<String, String>();
			table.put("singular", "sgn");
			table.put("plural", "pl");
			return table.get(line);
		}
		
		public static String getCase(String line){
			Hashtable<String, String> table = new Hashtable<String, String>();
			table.put("nominative","im");
			table.put("genitive","rod");
			table.put("dative","dat");
			table.put("accusative","vin");
			table.put("instrumental","tvor");
			table.put("ablative","pr");
			table.put("partitive","part");
			table.put("locative","mest");
			table.put("vocative","zvat");
			return (line!=null&&table.containsKey(line))?table.get(line):"u";
		}
		
	}
	
}
