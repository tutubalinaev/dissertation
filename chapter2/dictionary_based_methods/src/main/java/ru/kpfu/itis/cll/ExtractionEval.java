package ru.kpfu.itis.cll;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
/*
 * The main class to run the dictionary-based methods for problem phrase extraction.
 * Variables: classify FOLDERNAME -on INPUTFILE -tm MODEL
 * where FOLDERNAME is a folder' path with an input file INPUTFILE, 
 * MODEL is an int value (0 - DbA for Russian texts, 1 - CbA for Russian texts, 2 - DbA for English texts, 3 - CbA for English texts)
 */
public class ExtractionEval {

	static String PATH = "";
	private static long startTime = System.currentTimeMillis();

	public static void main(String[] args) throws Exception {
		int trainmodelmode = 0;
		Options options = new Options();

		options.addOption("on", true, "Name of the input file to process");
		options.addOption("tm", true, "Train Modelmode");

		CommandLineParser parser = new GnuParser();
		try {
			String name = "";
			CommandLine line = parser.parse(options, args);
			if (line.hasOption("on")) {
				name = line.getOptionValue("on");
			}
			if (line.hasOption("tm")) {
				trainmodelmode = Integer.parseInt(line.getOptionValue("tm"));
			}

			String[] argList = line.getArgs();
			PATH = argList[1];

			ExtractionSystem sentimentanalysis = new ExtractionSystem(PATH,name);

			switch (argList[0]) {
			case "classify":
				sentimentanalysis.classifyData(trainmodelmode);
				break;
			case "eval":
				sentimentanalysis.classifyAndEvalData(trainmodelmode);
				break;
			default:
				throw new IllegalArgumentException("Invalid mode: "
						+ argList[0]);
			}
		} catch (ParseException exp) {
			System.err.println("The program failed.  Reason: "
					+ exp.getMessage());
		}
		long endTime = System.currentTimeMillis();
		System.out.println("It took " + ((endTime - startTime) / 1000)
				+ " seconds");
	}
}
