package ru.kpfu.itis.cll;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.tcas.Annotation;
import org.json.JSONObject;

import au.com.bytecode.opencsv.CSVReader;

/*
 * This main class for methods' evaluation (not for production use).
 */

public class App {
    /**
     * UIMA type holding dates.
     */
    //public static final String scriptName="ru.kpfu.itis.cll.BasicEnProblemExtractor";
	public static final String scriptName="ru.kpfu.itis.cll.ClauseProblemExtractor";
	
	public static final String PPH_TYPE = scriptName+".ProblemSentence";
	public static final String NONPPH_TYPE = scriptName+".NotProblemSentence";
	public static final String POSS_PPH_TYPE = scriptName+".DeficiencyPhrase";
	public static final String IMP_TYPE = scriptName+".ImperativeTrigger";
	
	public static POSDetector detector = new POSDetector();

	public static void run(AnalysisEngine engine, CAS cas, Item object) throws Exception {
    	String text=object.getStemmedText();

    	cas.reset();   
    	cas.setDocumentText(text+" .");
        engine.process(cas);
        Collection<AnnotationFS> pphrases =CasUtil.select(cas, cas.getTypeSystem().getType(PPH_TYPE));
        Collection<AnnotationFS> non_pphrases =CasUtil.select(cas, cas.getTypeSystem().getType(NONPPH_TYPE));       
        
        ArrayList<AnnotationFS> prphrases = new  ArrayList<AnnotationFS>();
                
        if (pphrases.size()==0 && non_pphrases.size()==0){
        	Collection<AnnotationFS> poss_pphrases =CasUtil.select(cas, cas.getTypeSystem().getType(POSS_PPH_TYPE)); //DeficiencyPhrase
	        for (AnnotationFS item:poss_pphrases){
	        	//check phrases' pos tags
	            LinkedList<String> tags = detector.getTags(item.getCoveredText(),"pos");
	        	if (tags.size()==0){
	        		prphrases.add(new Annotation(1, null));
	        		break;
	        	}
	        	for (String tag:tags){
	        		if (tag.equals("substantive")){
	        			prphrases.add(new Annotation(1, null));
	        			break;
	        		}
	        	}
	        	prphrases.add(new Annotation(1, null));
	        }      
        }
               
	    if (prphrases.size()>0 || pphrases.size()>0) {
	        	object.setProblemClass();        	
	    } else {      
	      text=object.getText();
	      cas.reset();   
	      cas.setDocumentText(text+" .");
	      engine.process(cas);
	      pphrases=CasUtil.select(cas, cas.getTypeSystem().getType(IMP_TYPE));
		  if (pphrases.size()>0)
		     object.setProblemClass();        	
		  else 
		     object.setNoProblemClass(); 
	    }    
    }
    

    public static void main(String[] args) throws Exception {
        String mainPath="/Users/telena/Dropbox/ProblemExtraction/final_corpus/ru/";   
        ArrayList<Item> objects=runForRussianTexts(mainPath,0);
        for (Item obj:objects){
        	JSONObject o = new JSONObject();
        	o.put("text", obj.getText());
        	o.put("stemmedText", obj.getStemmedText());
        	o.put("problemscore", obj.classID);
        	System.out.println(o.toString());
        }
        runForFullDataset(objects);   
        Evaluation.runEval(objects);       
    }
    
    public static ArrayList<Item> runForRussianTexts(String mainPath, int datasetID){
    	String fileName=getFileName(datasetID);  //0+1
        ArrayList<Item> objects=new ArrayList<Item>();
        FileUtil.readCSVLines(mainPath+fileName, objects, 2, 3, 4);
        return objects;
    }
    public static ArrayList<Item> runForEnglishTexts(String mainPath, int datasetID){
    	String fileName=getFileName(datasetID);  //2--5
        ArrayList<Item> objects=new ArrayList<Item>();
        FileUtil.readCSVLines(mainPath+fileName, objects, 2, 0, 0);
        return objects;
    }
    
    public static String getFileName(int i){
    	String result="";
    	switch (i) {
		case 0:
			result="mobile_app_ru_rating_only2.txt_stemmed.txt";
			break;
		case 1:
			result="otzovik_cars_rating_full.csv_stemmed.txt";
			break;
		case 2:
			result="auto_en_rating.txt";
			break;
		case 3:
			result="hp_en_rating.txt";
			break;
		case 4:
			result="baby_en_rating.txt";
			break;
		case 5:
			result="tools_en_rating.txt";
			break;
		default:
			break;
		}
    	return result;
    }
    
    public static void runForFullDataset(ArrayList<Item> objects)  throws Exception {
    	 final AnalysisEngine engine =
                 AnalysisEngineFactory.createEngine(scriptName+"Engine");
         final CAS cas = engine.newCAS();
         
         for (Item obj:objects){
        		run(engine, cas, obj);
     	 }
     	
    }
    public static boolean isEnConjExists(Item obj){
    	return (obj.text.toLowerCase().contains(" but ")
    			|| obj.text.toLowerCase().contains(" because ")
    			|| obj.text.toLowerCase().contains(" despite ")
    			|| obj.text.toLowerCase().startsWith("because ")
    			|| obj.text.toLowerCase().startsWith("despite ")
    			|| obj.text.toLowerCase().startsWith("but "));			
   }
    public static boolean isRuConjExists(Item obj){
    	return (obj.text.toLowerCase().contains(" но ")
    			|| obj.text.toLowerCase().contains(" а ")
    			|| obj.text.toLowerCase().contains(" хотя ")
    			|| obj.text.toLowerCase().contains(" пока ")
    			|| obj.text.toLowerCase().contains(" если ")
    			|| obj.text.toLowerCase().contains(" поэтому ")
    			|| obj.text.toLowerCase().contains(" теперь ")
    			|| obj.text.toLowerCase().contains(" правда ")
    			|| obj.text.toLowerCase().startsWith("но ")
    			|| obj.text.toLowerCase().startsWith("а ")
    			|| obj.text.toLowerCase().startsWith("хотя ")
    			|| obj.text.toLowerCase().startsWith("пока ")
    			|| obj.text.toLowerCase().startsWith("если ")
    			|| obj.text.toLowerCase().startsWith("поэтому ")
    			|| obj.text.toLowerCase().startsWith("теперь ")
    			|| obj.text.toLowerCase().startsWith("правда "));			
    }
    
}
