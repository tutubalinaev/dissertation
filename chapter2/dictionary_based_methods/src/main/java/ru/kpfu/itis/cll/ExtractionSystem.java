package ru.kpfu.itis.cll;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONObject;

public class ExtractionSystem {
	private Set<JSONObject> dataList = new HashSet<JSONObject>();
	private String PATH =  "";
	private String FILE =  "";

	public ExtractionSystem(String path, String file)  throws FileNotFoundException, UnsupportedEncodingException {
		this.PATH = path;
		this.FILE = file;
		loadData(path,file);
	}

	public void classifyData(int system) throws Exception {
		ExtractionApplication knSystem = new ExtractionApplication(dataList);
		switch (system){
			case 0:
			case 1:
				knSystem.classifyData(system);
				break;
			default:
				throw new IllegalArgumentException("Invalid system: " + system);
		}
		//print json objects into an output file
		ArrayList<?> listToPrint = new ArrayList(knSystem.dataList);		
		FileUtil.writeLines(PATH+FILE+"-ed.txt",listToPrint);		
	}
	public void classifyAndEvalData(int system) throws Exception {
		ExtractionApplication knSystem = new ExtractionApplication(dataList);
		switch (system){
			case 0:
			case 1:
				knSystem.classifyAndEvalData(system);
				break;
			default:
				throw new IllegalArgumentException("Invalid system: " + system);
		}
	}
	
	private void loadData(String path, String file) throws FileNotFoundException, UnsupportedEncodingException{
		ArrayList<String> lines = new  ArrayList<String>();
		FileUtil.readLines(path+file,lines);
		for (String line:lines){
			JSONObject obj = new JSONObject(line);
			dataList.add(obj);
		}
		System.out.println("Data was added: " + dataList.size() + " objects");
	}
}
